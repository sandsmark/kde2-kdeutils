/***********************************************************************
 *
 *  Kfwin.h
 *
 ***********************************************************************/

#ifndef KFWIN_H
#define KFWIN_H

#include <klistview.h>
#include <kfileitem.h>

class KfArchiver;
class QPixmap;
class QFileInfo;

class KfFileLVI : public QListViewItem
{
 public:
  KfFileLVI(KListView* lv, const KFileItem &item);
  ~KfFileLVI();

  QString key(int column, bool) const;

  QFileInfo *fileInfo;
  KFileItem fileitem;
};

class KfindWindow: public   KListView
{
  Q_OBJECT
public:
  KfindWindow( QWidget * parent = 0, const char * name = 0 );

  void beginSearch();
  void endSearch();

  void insertItem(const KFileItem &item);

public slots:
  void copySelection();
  void slotContextMenu(KListView *,QListViewItem *item,const QPoint&p);

private slots:
  void deleteFiles();
  void fileProperties();
  void openFolder();
  void saveResults();
  void openBinding();
  void selectionHasChanged();

protected:
  virtual void resizeEvent(QResizeEvent *e);

  virtual QDragObject *dragObject() const;

signals:
  void resultSelected(bool);

private:
  bool haveSelection;
  bool m_pressed;
  void resetColumns(bool init);
};

#endif
