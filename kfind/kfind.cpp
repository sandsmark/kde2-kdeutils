/***********************************************************************
 *
 *  Kfind.cpp
 *
 **********************************************************************/

#include <qlayout.h>

#include <klocale.h>
#include <kglobal.h>
#include <kstatusbar.h>
#include <kmessagebox.h>
#include <kdebug.h>

#include "kftabdlg.h"
#include "kfwin.h"
#include "kquery.h"

#include "kfind.h"
#include "kfind.moc"

Kfind::Kfind(const KURL & url, QWidget *parent, const char *name)
  : KDialogBase( Plain, QString::null, 
	User1 | User2 | User3 | Close | Help, User1, 
        parent, name, true, true,
	i18n("&Find"), i18n("Stop"), i18n("Save...") )
{
  QWidget::setCaption( i18n("Find Files" ) );
  setButtonBoxOrientation(Vertical);

  enableButton(User1, true); // Enable "Search"
  enableButton(User2, false); // Disable "Stop"
  enableButton(User3, false); // Disable "Save..."

  setEscapeButton(User2);
  
  isResultReported = false;

  QFrame *frame = plainPage();

  // create tabwidget
  tabWidget = new KfindTabWidget(url, frame, "dialog");

  // prepare window for find results
  win = new KfindWindow(frame,"window");

  mStatusBar = new KStatusBar(frame);
  mStatusBar->insertFixedItem(i18n("SearchingXXX"), 0, true);
  setStatusMsg(i18n("Ready."));
  mStatusBar->setItemAlignment(0, AlignLeft | AlignVCenter);
  mStatusBar->insertItem(QString::null, 1, 1, true);
  mStatusBar->setItemAlignment(1, AlignLeft | AlignVCenter);

  QVBoxLayout *vBox = new QVBoxLayout(frame);
  vBox->addWidget(tabWidget, 0);
  vBox->addWidget(win, 1);
  vBox->addWidget(mStatusBar, 0);

  connect(this, SIGNAL(user1Clicked()),
	  this, SLOT(startSearch()));
  connect(this, SIGNAL(user2Clicked()),
	  this, SLOT(stopSearch()));
  connect(this, SIGNAL(user3Clicked()),
	  win, SLOT(saveResults()));

  connect(win ,SIGNAL(resultSelected(bool)),
	  this,SIGNAL(resultSelected(bool)));

  query = new KQuery(frame);
  connect(query, SIGNAL(addFile(const KFileItem&)),
	  SLOT(addFile(const KFileItem&)));
  connect(query, SIGNAL(result(int)), SLOT(slotResult(int)));
}

Kfind::~Kfind()
{
}

void Kfind::closeEvent(QCloseEvent *)
{
   slotClose();
}

void Kfind::setProgressMsg(const QString &msg)
{
   mStatusBar->changeItem(msg, 1);
}

void Kfind::setStatusMsg(const QString &msg)
{
   mStatusBar->changeItem(msg, 0);
}

void Kfind::startSearch()
{
  tabWidget->setQuery(query);

  isResultReported = false;

  // Reset count
  setProgressMsg(i18n("%1 files found").arg(0));
  setStatusMsg(i18n("Searching..."));

  emit resultSelected(false);
  emit haveResults(false);

  enableButton(User1, false); // Disable "Search"
  enableButton(User2, true); // Enable "Stop"
  enableButton(User3, false); // Disable "Save..."

  win->beginSearch();
  tabWidget->beginSearch();

  query->start();
}

void Kfind::stopSearch()
{
  query->kill();
}

void Kfind::newSearch()
{
  // WABA: Not used any longer?
  stopSearch();

  tabWidget->setDefaults();

  emit haveResults(false);
  emit resultSelected(false);

  setFocus();
}

void Kfind::slotResult(int errorCode)
{
  if (errorCode == 0)
    setStatusMsg(i18n("Ready."));
  else if (errorCode == KIO::ERR_USER_CANCELED)
    setStatusMsg(i18n("Aborted."));
  else if (errorCode == KIO::ERR_DOES_NOT_EXIST)
  {
     setStatusMsg(i18n("Error."));
     KMessageBox::sorry( this, i18n("Could not find the specified directory."));
  }
  else
  {
     kdDebug()<<"KIO error code: "<<errorCode<<endl;
     setStatusMsg(i18n("Error."));
  };

  enableButton(User1, true); // Enable "Search"
  enableButton(User2, false); // Disable "Stop"
  enableButton(User3, true); // Enable "Save..."

  win->endSearch();
  tabWidget->endSearch();
  setFocus();
}

void Kfind::addFile(const KFileItem &item)
{
  win->insertItem(item);

  if (!isResultReported)
  {
    emit haveResults(true);
    isResultReported = true;
  }

  int count = win->childCount();
  QString str;
  if (count == 1)
    str = i18n("1 file found");
  else
    str = i18n("%1 files found")
      .arg(KGlobal::locale()->formatNumber(count, 0));
  setProgressMsg(str);
}

void Kfind::setFocus()
{
  tabWidget->setFocus();
}

void Kfind::copySelection()
{
  win->copySelection();
}
