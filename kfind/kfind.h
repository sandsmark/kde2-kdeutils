/***********************************************************************
 *
 *  Kfind.h
 *
 ***********************************************************************/

#ifndef KFIND_H
#define KFIND_H

#include <kdialogbase.h>

class QString;

class KQuery;
class KURL;
class KFileItem;
class KfindTabWidget;
class KfindWindow;
class KStatusBar;

class Kfind: public KDialogBase
{
Q_OBJECT

public:
  Kfind(const KURL & url, QWidget * parent = 0, const char * name = 0);
  ~Kfind();
  void copySelection();

  void setStatusMsg(const QString &);
  void setProgressMsg(const QString &);

public slots:
  void startSearch();
  void stopSearch();
  void newSearch();
  void addFile(const KFileItem & item);
  void setFocus();
  void slotResult(int);
//  void slotSearchDone();

signals:
  void haveResults(bool);
  void resultSelected(bool);

private:
  void closeEvent(QCloseEvent *);
  KfindTabWidget *tabWidget;
  KfindWindow * win;

  bool isResultReported;
  KQuery *query;
  KStatusBar *mStatusBar;
};

#endif


