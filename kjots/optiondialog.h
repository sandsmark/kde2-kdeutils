/*
 *   kjots
 *   This file only: Copyright (C) 1999  Espen Sand, espensa@online.no
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

#ifndef _OPTION_DIALOG_H_
#define _OPTION_DIALOG_H_ 


class QLineEdit; 
class KFontChooser;

#include <kdialogbase.h>

class ConfigureDialog : public KDialogBase
{
  Q_OBJECT
  
  public:
    ConfigureDialog( QWidget *parent=0, char *name=0, bool modal=true );
    ~ConfigureDialog( void );

  protected slots:
    virtual void slotOk( void );  
    virtual void slotApply( void );

  private:
    void setupURLPage( void );
    void setupFontPage( void );

  private slots:
    void readConfig( void );
    void writeConfig( void );

  private:
    QLineEdit    *mHttpInput;
    QLineEdit    *mFtpInput;
    KFontChooser *mFontChooser;

  signals:
    void valueChanged( void );
};

#endif
