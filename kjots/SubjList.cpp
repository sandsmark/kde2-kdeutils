//
//  kjots
//
//  Copyright (C) 1997 Christoph Neerfeld
//  email:  Christoph.Neerfeld@home.ivm.de chris@kde.org
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//


//
// 1999-11-07-Espen Sand:
// Converted to KDialogBase
//

#include <qlayout.h>

#include <kapp.h>
#include <klistbox.h>
#include <klocale.h>

#include "SubjList.h"
#include "KJotsMain.h"


SubjList::SubjList( QWidget *parent, const char *name, bool modal )
  :KDialogBase( i18n("Subjects"), Cancel, Cancel, Cancel, parent, name, modal )
{
  //
  // 1999-11-07-Espen Sand:
  // I am using KDialogBase in message box mode to center the action
  // button. Because of this I have to revert some default behavior.
  //
  setButtonText( Cancel, i18n("&Close") );

  lb_subj = new KListBox(this);
  setMainWidget( lb_subj );
  lb_subj->setFocus();
  lb_subj->setMinimumSize(200, 120);
  connect( lb_subj, SIGNAL(highlighted(int)), this, SLOT(highlighted(int)) );
  highlightflag = true;
  connect( lb_subj, SIGNAL(selected(int)), this, SLOT(hide()) );
}


void SubjList::rebuildList( QList<TextEntry> * new_list )
{
  lb_subj->clear();
  lb_subj->setUpdatesEnabled(FALSE);
  entrylist = new_list;
  QListIterator<TextEntry> it(*entrylist);
  it.toFirst();
  TextEntry *item;
  for ( ; (item = it.current()); ++it )
    {
      lb_subj->insertItem(item->subject);
    }
  lb_subj->setUpdatesEnabled(TRUE);
  lb_subj->repaint();
}

void SubjList::select( int index )
{
  current = index;
  lb_subj->setCurrentItem(index);
}

void SubjList::entryChanged( const QString &new_subj)
{
  QString temp = lb_subj->text(current);
  if( temp == (QString) new_subj )
    return;
  /*
    lb_subj->removeItem(current);            // this looks ridiculous but it's necessary
    lb_subj->insertItem(new_subj, current ); // changeItem does not work properly
  */

  highlightflag = false; // prevents flicker when "highlighted" signal occurs while updating
  lb_subj->changeItem(new_subj, current );
  lb_subj->setCurrentItem(current);
  highlightflag = true;
}
#include "SubjList.moc"
