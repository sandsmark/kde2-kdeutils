// -*- C++ -*-

//
//  kjots
//
//  Copyright (C) 1997 Christoph Neerfeld
//  email:  Christoph.Neerfeld@home.ivm.de or chris@kde.org
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//

#ifndef SubjList_included
#define SubjList_included

#include <qlist.h>
#include <kdialogbase.h>

class KListBox;

#include "KJotsMain.h"

class SubjList : public KDialogBase
{
  Q_OBJECT
public:
  SubjList( QWidget *parent=0, const char *name=0, bool modal=true );
  virtual ~SubjList() {}

signals:
  void entryMoved(int);

public slots:
  void rebuildList( QList<TextEntry> *);
  void entryChanged(const QString &);
  void select( int );

protected slots:
  void highlighted( int index ) { 
	  if (!highlightflag) return; // don't refresh whilst updating
      current = index; 
      if ( current >= 0 )
	  emit entryMoved(index); 
  }

protected:
  KListBox         *lb_subj;
  QList<TextEntry> *entrylist;
  int               current;
  bool              highlightflag;
};

#endif // SubjList_included



