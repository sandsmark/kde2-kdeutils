/*
 *   kjots
 *   This file only: Copyright (C) 1999  Espen Sand, espensa@online.no
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

#include <qlabel.h>
#include <qlayout.h>
#include <qlineedit.h>

#include <kapp.h>
#include <kconfig.h>
#include <kfontdialog.h>
#include <klocale.h>


#include "optiondialog.h"


ConfigureDialog::ConfigureDialog( QWidget *parent, char *name, bool modal )
  :KDialogBase( Tabbed, i18n("Configure"), Help|Apply|Ok|Cancel,
		Ok, parent, name, modal )
{
  setHelp( "kjots/index.html", QString::null );
 
  setupURLPage();
  setupFontPage();

  readConfig();
  connect( this, SIGNAL(cancelClicked()), this, SLOT(readConfig()) );
}

ConfigureDialog::~ConfigureDialog( void )
{
}


void ConfigureDialog::setupURLPage( void )
{
  QFrame *page = addPage( i18n("&URL") );
  if( page == 0 ) { return; }

  QGridLayout *grid = new QGridLayout( page, 5, 2, 0, spacingHint() );
  
  QLabel *label;
  QString text;

  text = i18n("Specify which command to execute");
  label = new QLabel( text, page );
  grid->addMultiCellWidget( label, 0, 0, 0, 1 );

  text = i18n("For HTTP");
  label = new QLabel( text, page );
  grid->addWidget( label, 1, 0, AlignRight );
  
  text = i18n("For FTP");
  label = new QLabel( text, page );
  grid->addWidget( label, 2, 0, AlignRight );

  mHttpInput = new QLineEdit( page );
  mHttpInput->setMinimumWidth( fontMetrics().maxWidth()*20 );
  grid->addWidget( mHttpInput, 1, 1 );

  mFtpInput = new QLineEdit( page );
  mFtpInput->setMinimumWidth( fontMetrics().maxWidth()*20 );
  grid->addWidget( mFtpInput, 2, 1 );

  text = i18n("(%u is replaced with the selected URL)");
  label = new QLabel( text, page );
  grid->addMultiCellWidget( label, 3, 3, 1, 1 );

  grid->setRowStretch( 4, 10 );  
}



void ConfigureDialog::setupFontPage( void )
{
  QFrame *page = addPage( i18n("Editor &Font") );
  if( page == 0 ) { return; }

  QVBoxLayout *topLayout = new QVBoxLayout( page, 0, spacingHint() );
  if( topLayout == 0 ) { return; }

  mFontChooser = new KFontChooser(page,"fonts",false,QStringList(),false,4);
  topLayout->addWidget( mFontChooser );
}




void ConfigureDialog::readConfig( void )
{
  KConfig &config = *kapp->config();

  config.setGroup("kjots");
  mHttpInput->setText( config.readEntry("execHttp") );
  mFtpInput->setText( config.readEntry("execFtp") );

  QFont font( config.readEntry("EFontFamily"), 
	      config.readNumEntry("EFontSize"),
	      config.readNumEntry("EFontWeight"), 
	      config.readNumEntry("EFontItalic"),
	      (QFont::CharSet) config.readNumEntry("EFontCharset") );
  mFontChooser->setFont(font);
}

void ConfigureDialog::writeConfig( void )
{
  KConfig &config = *kapp->config();

  config.setGroup("kjots");
  config.writeEntry( "execHttp", mHttpInput->text() );
  config.writeEntry( "execFtp", mFtpInput->text() );
  QFont font = mFontChooser->font();
  config.writeEntry("EFontFamily", font.family());
  config.writeEntry("EFontSize", font.pointSize());
  config.writeEntry("EFontWeight", font.weight());
  config.writeEntry("EFontItalic", font.italic());
  config.writeEntry("EFontCharset", (int)font.charSet());
}


void ConfigureDialog::slotOk( void )
{
  slotApply();
  accept();
}


void ConfigureDialog::slotApply( void )
{
  writeConfig();
  emit valueChanged();
}


#include "optiondialog.moc"
