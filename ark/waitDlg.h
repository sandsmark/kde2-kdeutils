/*

 $Id: waitDlg.h 84093 2001-02-23 18:46:10Z mjarrett $

 ark -- archiver for the KDE project

 Copyright (C)

 1997-1999: Rob Palmbos palm9744@kettering.edu
 1999: Francois-Xavier Duranceau duranceau@kde.org

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 2
 of the License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

*/

#ifndef WAIT_DLG_H
#define WAIT_DLG_H

class QDialog;

class WaitDlg : public QDialog {
	Q_OBJECT
public:
	WaitDlg( QWidget *_parent=0, const char *_name=0, bool _modal=false, WFlags _f=0 );
	void close();
	
private slots:
	void onCancel();
	
signals:	
	void dialogClosed();
};

#endif /* WAIT_DLG_H */

