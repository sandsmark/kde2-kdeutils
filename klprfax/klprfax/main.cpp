/***************************************************************************
                          main.cpp  -  description
                             -------------------
    begin                : Thu May 24 23:17:55 'Europe/Amsterdam' 2001
    copyright            : (C) 2001 by Hans Dijkema
    email                : hdijkema@hum.org
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include <kcmdlineargs.h>
#include <kaboutdata.h>
#include <klocale.h>

#include "klprfax.h"
#include "klprfax_config.h"

#include <ksimpleconfig.h>
#include <kmessagebox.h>

static const char *description =
	I18N_NOOP("K Send a Fax\n"
            "\n"
            "With this program you can make a printer that\n"
            "acts as a fax. The program uses efax package of\n"
            "Ed Casas (GPL2)"
           );
// INSERT A DESCRIPTION FOR YOUR APPLICATION HERE


static KCmdLineOptions options[] =
{
  { "fax <file>", I18N_NOOP("The file to fax"), 0 },
  { 0,0,0 }
  // INSERT YOUR COMMANDLINE OPTIONS HERE
};

int main(int argc, char *argv[])
{

  KAboutData aboutData( "klprfax", I18N_NOOP("K Send a Fax"),
    VERSION, description, KAboutData::License_GPL,
    "(c) 2001, Hans Dijkema", 0, 0, "h.dijkema@hum.org");
  aboutData.addAuthor("Hans Dijkema",0, "h.dijkema@hum.org");

  KCmdLineArgs::init( argc, argv, &aboutData );
  KCmdLineArgs::addCmdLineOptions( options ); // Add our own options.

  KApplication a;
  KSimpleConfig conf("klprfax.rc");
  KCmdLineArgs *args=KCmdLineArgs::parsedArgs();

  if (!args->isSet("fax")) {
    klprfax_config *config = new klprfax_config(&a,&conf);
    //a.setMainWidget(config);
    a.setTopWidget(config);
    a.setMainWidget(config);
    config->show();
  }
  else {
    Klprfax *klprfax = new Klprfax(&a,&conf,args->getOption("fax"));
    if (klprfax->fatal) { return 1; }
    a.setTopWidget(klprfax);
    a.setMainWidget(klprfax);
    klprfax->show();
  }

  return a.exec();
}
