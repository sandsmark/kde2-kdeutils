#include "klprfax_widget.h"

//
// Make this table we are using for displaying fax numbers non editable.
//

klprfax_table::klprfax_table(int rows,int cols,QWidget *parent,const char *name)
     : QTable(rows,cols,parent,name)
{}

QWidget *klprfax_table::createEditor(int,int,bool) const
{
  return 0;
}

//
// There are two possible ways to display a fax number:
//   If there is one number: a klprfax_label is used, which is a QLabel.
//   If there are more: a klprfax_box is used, which is a QComboBox.
//
// We're using these classes to get the the right cell selected when
// we click on the widget, wich is used as a table cell.
//


klprfax_widget::klprfax_label::klprfax_label(QWidget *parent,klprfax_widget *_w) : QLabel(parent)
{
  w=_w;
  setFocusPolicy(QWidget::StrongFocus);
}

void klprfax_widget::klprfax_label::focusInEvent(QFocusEvent *e)
{
  w->setRowCol();
  QLabel::focusInEvent(e);
}

klprfax_widget::klprfax_box::klprfax_box(QWidget *parent,klprfax_widget *_w) : QComboBox(parent)
{
  w=_w;
  setFocusPolicy(QWidget::StrongFocus);
}

void klprfax_widget::klprfax_box::focusInEvent(QFocusEvent *e)
{
  w->setRowCol();
  QComboBox::focusInEvent(e);
}

//
// This is where we implement the behaviour we want.
// We want a widget that can be a label or a combobox.
//

klprfax_widget::klprfax_widget(int r,int c,QTable *_parent) : QWidget(_parent)
{
  parent=_parent;
  labels=0;
  label=NULL;
  box=NULL;
  row=r;col=c;
}

klprfax_widget::~klprfax_widget()
{
  if (label) delete label;
  if (box)   delete box;
}

QWidget *klprfax_widget::widget(void)
{
  return this;
/*  if (labels==0) { return NULL; }
  else if (labels==1) { return label;}
  else { return box; }*/
}

void klprfax_widget::add(QString & txt)
{
  if (txt=="" || txt==QString::null) { return; }

  labels+=1;
  switch (labels) {
    case 1 :
             label=new klprfax_label(this,this);
             label->setText(txt);
    break;

    case 2 :
             box=new klprfax_box(this,this);
             box->insertItem(label->text());
             box->setCurrentItem(0);
             delete label;
             add(txt);
    break;
    default: box->insertItem(txt);
    break;
  }
}

void klprfax_widget::commit(void)
{
  parent->setCellWidget(row,col,this);
}

QString klprfax_widget::text(void)
{
  switch (labels) {
    case 0 : return QString::null;
    break;
    case 1 : return label->text();
    break;
    default: return box->currentText();
    break;
  }
}

void klprfax_widget::setRowCol(void)
{
  parent->setCurrentCell(row,col);
}

void klprfax_widget::adjustSize(void)
{
  if (labels==1) { label->adjustSize(); }
  else if (labels>1) { box->adjustSize(); }
  QWidget::adjustSize();
}

#define D(a) QWidget::a;if (labels==1) { label->a; } else if (labels>1) { box->a; }

void klprfax_widget::setMinimumSize(int w,int h)
{
  D(setMinimumSize(w,h))
}

void klprfax_widget::setMaximumSize(int w,int h)
{
  D(setMaximumSize(w,h))
}

void klprfax_widget::setGeometry(int x,int y,int w,int h)
{
  QWidget::setGeometry(x,y,w,h);
  if (labels==1) { label->setGeometry(0,0,w,h); }
  else if (labels>1) { box->setGeometry(0,0,w,h); }
}

void klprfax_widget::setGeometry(QRect & r)
{
  setGeometry(r.x(),r.y(),r.width(),r.height());
}

void klprfax_widget::resize(int w,int h)
{
  D(resize(w,h))
}

QSize klprfax_widget::sizeHint(void) const
{
  if (labels==0) { return QWidget::sizeHint(); }
  else if (labels==1) { return label->sizeHint(); }
  else { return box->sizeHint(); }
}
