#ifndef __KLPRFAX_WIDGET_H
#define __KLPRFAX_WIDGET_H

#include <qtable.h>
#include <qlabel.h>
#include <qcombobox.h>
#include <qpushbutton.h>

class klprfax_table : public QTable
{
  public:
    klprfax_table(int rows,int cols,QWidget *parent=0,const char *name=0);
  public:
    virtual QWidget *createEditor(int row,int col,bool init) const;
};


class klprfax_widget : public QWidget
{
  class klprfax_label : public QLabel
  {
    private:
      klprfax_widget *w;
    public:
      klprfax_label(QWidget *parent,klprfax_widget *w);
    protected:
      void focusInEvent(QFocusEvent *);
  };

  class klprfax_box : public QComboBox
  {
    private:
      klprfax_widget *w;
    public:
      klprfax_box(QWidget *parent,klprfax_widget *w);
    protected:
      void focusInEvent(QFocusEvent *);
  };

  public:
    klprfax_widget(int row,int col,QTable *parent=0);
    virtual ~klprfax_widget();
  public:
    void     add(QString & txt);
    void     commit(void);
    QString  text(void);
    QWidget *widget(void);
  private:
    klprfax_box      *box;
    klprfax_label    *label;
    int               labels;
    int               row,col;
    QTable           *parent;
  public:
    void setRowCol(void);
  public:
    virtual void adjustSize(void);
    virtual void setMinimumSize(int w,int h);
    virtual void setMaximumSize(int w,int h);
    virtual void setGeometry(int x,int y,int w,int h);
    virtual void setGeometry(QRect & r);
    virtual void resize(int w,int h);
    virtual QSize sizeHint(void) const;
};

#endif
