/***************************************************************************
                          klprfax.h  -  description
                             -------------------
    begin                : Thu May 24 23:17:55 'Europe/Amsterdam' 2001
    copyright            : (C) 2001 by Hans Dijkema
    email                : hdijkema@hum.org
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef KLPRFAX_H
#define KLPRFAX_H

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <kapp.h>
#include <qwidget.h>

#include <kconfig.h>

#include <qlistview.h>
#include <qlistbox.h>
#include <qpushbutton.h>
#include <addressbook.h>
#include <kabapi.h>
#include <qlineedit.h>
#include <qprogressbar.h>
#include <qlabel.h>
#include <qgrid.h>
#include <qtable.h>

#include "klprfax_widget.h"

#include <kprocess.h>

/** Klprfax is the base class of the project */

class Klprfax : public QWidget
{
  Q_OBJECT
  public:
    /** construtor */
    Klprfax(KApplication *app,KConfigBase *cnf,QCString fax,QWidget* parent=0, const char *name=0);
    /** destructor */
    ~Klprfax();

private: // Private attributes

  KApplication  * app;
  KConfigBase   * conf;

  QGrid         * dlg;
  QGrid         * buttons;
  QGrid         * edits;
  QGrid         * progr;


  klprfax_table * names;
  QPushButton   * goFax;
  QPushButton   * noFax;
  QPushButton   * gogoFax;
  QPushButton   * configure;
  QPushButton   * about;
  QLineEdit     * from;
  QLineEdit     * fromFax;
  QLabel        * lfrom;
  QLabel        * lfromFax;
  QLabel        * lprefix;
  QLineEdit     * prefix;

  QListBox      * log;

  QProgressBar  * progress;
  QLabel        * doing;

  QString         efax;
  QString         faxFile;

  bool            dialPrefix;

  bool            canCancel;
  bool            canceled;

  KabAPI        * kab;

  KShellProcess   efax_helper;
  QString         efax_diagnostic;

  QString         script,dir;

public: // Public attributes
  /**  */
  bool fatal;

private: // Private methods
  void doLog(QString s);

  bool NameFax(AddressBook::Entry & e,QString & name, QString & fax0,QString & fax1,QString & fax2,QString & fax3);
  void findEFax();

  void enableAll(bool yes=true);

protected slots: // Protected slots
  /** No descriptions */
  void goFaxing();
  void gogoFaxing();
  void Fax();
  void noFaxing();

  /** No descriptions */
  void setConfig(void);

private slots: // Private slots
  /** No descriptions */
  void doAbout();
  void efaxOutput(KProcess *proc,char *buf,int len);
  void efaxExited(KProcess *proc);
};

#endif

