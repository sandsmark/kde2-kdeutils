/***************************************************************************
                          klprfax_config.h  -  description
                             -------------------
    begin                : Sun May 27 2001
    copyright            : (C) 2001 by Hans Dijkema
    email                : hdijkema@hum.org
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef KLPRFAX_CONFIG_H
#define KLPRFAX_CONFIG_H

#include <kapp.h>
#include <qwidget.h>

#include <kconfig.h>
#include <ksimpleconfig.h>
#include <qlineedit.h>
#include <qpushbutton.h>
#include <qlabel.h>
#include <qgrid.h>
#include <qgroupbox.h>


/**
  *@author Hans Dijkema
  */

class klprfax_config : public QWidget  {
   Q_OBJECT
public: 
	klprfax_config(KApplication *app,KConfigBase *cnf,QWidget *parent=0, const char *name=0);
	~klprfax_config();
private:
  KConfigBase  * config;
  KConfigBase  * rootConf;
  KApplication * app;
//
  QPushButton *ok;
  QPushButton *cancel;
  QPushButton *about;
// The canvas:
  QGrid       *canvas;
  QGrid       *rootCanvas;
  QGrid       * buttons;
  QGrid       *dlg;
// by root
  QLabel      *explainPtrName;
  QLineEdit   *printerName;
  QPushButton *editPrintCap;
  QLabel      *explainModem;
  QLineEdit   *modemDevice;
  QPushButton *setModemDevice;
  QLabel      *rootGroup;
// by users
  QLabel      *userGroup;
  QLabel      *lxDisplay;
  QLineEdit   *xDisplay;
  QLabel      *lfrom;
  QLineEdit   *from;
  QLabel      *lfromFax;
  QLineEdit   *fromFax; 
  QLabel      *lprefix;
  QLineEdit   *prefix;
private: // Private methods
  /** No descriptions */
  /** No descriptions */
  bool isRoot(void);
  /** No descriptions */
  void createLPD();
private slots: // Public slots
  /** No descriptions */
  void EditPrintCap();
  /** No descriptions */
  void setDevice();
  /** No descriptions */
  void set();
  /** No descriptions */
  void doAbout();
};

#endif
/** No descriptions */
