#ifndef DAEMONDOCK
#define DAEMONDOCK 1
/*
 * daemondock.h
 * Copyright (C) 1999 Paul Campbell <paul@taniwha.com>
 *
 * This file contains the docked widget for the laptop battery monitor
 *
 * $Id: daemondock.h 88701 2001-03-26 03:22:31Z staikos $
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */


#include "laptop_daemon.h"

class laptop_daemon;
class KPCMCIA;
class KPCMCIACard;
class QPopupMenu;

#include <qmap.h>

class laptop_dock : public KDockWindow {

  Q_OBJECT

public:
  laptop_dock(laptop_daemon* parent);
  ~laptop_dock();
  void displayPixmap();

  void mousePressEvent( QMouseEvent * ); 
  void mouseReleaseEvent( QMouseEvent * ); 
  void showEvent( QShowEvent * );  

  inline void setPCMCIA(KPCMCIA *p) { _pcmcia = p; }

private slots:
  void invokeStandby();
  void invokeSuspend();
  void invokeLockSuspend();
  void invokeHibernation();
  void invokeLockHibernation();
  void invokeSetup();
  void noop();

  void slotEjectAction(int id);
  void slotResumeAction(int id);
  void slotSuspendAction(int id);
  void slotInsertAction(int id);
  void slotResetAction(int id);
  void slotDisplayAction(int id);

private:
  laptop_daemon *p;
  QPixmap	pm;
  int	 	current_code;
  KPCMCIA       *_pcmcia;
  QMap<int,KPCMCIACard *> _ejectActions,
                          _suspendActions,
                          _resumeActions,
                          _resetActions,
                          _displayActions,
                          _insertActions;

}; 
#endif 

