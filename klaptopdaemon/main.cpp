/*
 * main.cpp
 *
 * Requires the Qt widget libraries, available at no cost at
 * http://www.troll.no/
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */


#include <klocale.h>
#include <kglobal.h>
#include <kprocess.h>
#include <kconfig.h>

#include "warning.h"
#include "power.h"
#include "battery.h"
#include "pcmcia.h"


extern "C"
{

  KCModule *create_pcmcia(QWidget *parent, const char *name) 
  { 
    KGlobal::locale()->insertCatalogue("kcmlaptop");
    return new PcmciaConfig(parent, name);
  }

  KCModule *create_bwarning(QWidget *parent, const char *name) 
  { 
    KGlobal::locale()->insertCatalogue("kcmlaptop");
    return new WarningConfig(0, parent, name);
  }
  KCModule *create_cwarning(QWidget *parent, const char *name) 
  { 
    KGlobal::locale()->insertCatalogue("kcmlaptop");
    return new WarningConfig(1, parent, name);
  }
  KCModule *create_battery(QWidget *parent, const char *name) 
  { 
    KGlobal::locale()->insertCatalogue("kcmlaptop");
    return new BatteryConfig(parent, name);
  }
  KCModule *create_power(QWidget *parent, const char *name) 
  { 
    KGlobal::locale()->insertCatalogue("kcmlaptop");
    return new PowerConfig(parent, name);
  }

  void init_battery()
  {
    KConfig *config = new KConfig("kcmlaptoprc");  
    config->setGroup("BatteryDefault");
    bool enable = config->readBoolEntry("Enable", false);
    if (!enable)
      return;
    KProcess process;
    process << "klaptopdaemon";
    process.start(KProcess::DontCare);
  }
}


