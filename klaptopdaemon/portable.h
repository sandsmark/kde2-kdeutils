#include <qlabel.h>

#ifndef PORTABLE_H
#define PORTABLE_H

struct power_result {
	int	powered	;	// true if we're plugged into the wall
	int	percentage;	// value 0-100 percentage of battery left
	int	time;		// time in minutes left - -1 if this is not supported by the BIOS
};

class laptop_portable {
public:
	static int has_power_management();	// returns 1 if this computer has power management
	static int has_battery_time();		// returns 1 if this give BIOS battery time info
	static int has_suspend();		// returns 1 if this computer can perform a suspend
	static int has_standby();		// returns 1 if this computer can perform a standby
	static int has_hibernation();		// returns 1 if this computer can perform a hibernate
	static QLabel *no_power_management_explanation(QWidget *parent);
	static QLabel *how_to_do_suspend_resume(QWidget *parent);
	static QLabel *pcmcia_info(int x, QWidget *parent);
	static void invoke_standby();
	static void invoke_suspend();
	static void invoke_hibernation();
	static struct power_result poll_battery_state();
	static int poll_activity();
};

#endif
