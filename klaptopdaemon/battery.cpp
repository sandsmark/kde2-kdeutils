/*
 * battery.cpp
 *
 * Copyright (c) 1999 Paul Campbell <paul@taniwha.com>
 *
 * Requires the Qt widget libraries, available at no cost at
 * http://www.troll.no/
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <iostream>

#include <sys/types.h>
#include <unistd.h>
#include <sys/stat.h>
#include <stdlib.h>

#include <qfileinfo.h>
#include <qstring.h>
#include <qhbuttongroup.h>
#include <qlayout.h>

#include <klocale.h>
#include <kconfig.h>
#include <knumvalidator.h>
#include <stdio.h>

#include "battery.h"
#include "version.h"
#include "portable.h"

BatteryConfig::BatteryConfig (QWidget * parent, const char *name)
  : KCModule(parent, name)
{
    apm = laptop_portable::has_power_management();

    if (!apm) {
      QVBoxLayout *top_layout = new QVBoxLayout(this, 12, 5);

      QLabel* explain = laptop_portable::no_power_management_explanation(this);
      top_layout->addWidget(explain, 0);

      top_layout->addStretch(1);

      top_layout->activate();

    } else {
        QVBoxLayout *top_layout = new QVBoxLayout(this, 12, 5);
        QGridLayout *top_grid = new QGridLayout(3, 3);
        top_layout->addLayout(top_grid);

	iconloader = new KIconLoader("klaptopdaemon");
	

        // do we show the monitor
        runMonitor = new QCheckBox(i18n("Show Battery Monitor"), this);
        runMonitor->setFixedSize(200, 24);
  	connect(runMonitor, SIGNAL(clicked()), this, SLOT(configChanged()));
        top_grid->addWidget(runMonitor, 0, 0);

	top_grid->setColStretch(2, 1);

        // the poll time (in seconds)
        QLabel* poll_label = new QLabel(i18n("Poll (sec):"), this);
        poll_label->setFixedSize(200, 24);
        top_grid->addWidget(poll_label, 2, 0);


        editPoll = new QLineEdit(this);
        editPoll->setValidator( new KIntValidator( 1,3600,editPoll ) );
	editPoll->setFixedSize(60, 24);
  	connect(editPoll, SIGNAL(textChanged(const QString&)), this, SLOT(configChanged()));
	top_grid->addWidget(editPoll, 2, 1);

        QHBoxLayout *xx = new QHBoxLayout;
        top_layout->addLayout(xx);

	// group box to hold the icons together
        QGroupBox* icons_groupbox = new QGroupBox(i18n("Icons:"), this);
        xx->addWidget(icons_groupbox, 0);
        xx->addStretch(1);

        // layout to hold the icons inside the groupbox
        QVBoxLayout *icon_layout = new QVBoxLayout(icons_groupbox, 8);
        icon_layout->addSpacing(8);

        QGridLayout *icon_grid = new QGridLayout(2, 3);

        icon_layout->addLayout(icon_grid);

        QHBoxLayout *a00 = new QHBoxLayout;
        QLabel* nobattery_label = new QLabel(i18n("No Battery:"), icons_groupbox);
        nobattery_label->setFixedSize(100, 24);
        icon_grid->addLayout(a00,0,0);
        a00->addStretch(1);
        a00->addWidget(nobattery_label,0);
        a00->addStretch(1);

        QHBoxLayout *a01 = new QHBoxLayout;
        QLabel* nocharge_label = new QLabel(i18n("Not Charging:"), icons_groupbox);
        nocharge_label->setFixedSize(100, 24);
        icon_grid->addLayout(a01, 0, 1);
        a01->addStretch(1);
        a01->addWidget(nocharge_label,0);
        a01->addStretch(1);

        QHBoxLayout *a02 = new QHBoxLayout;
        QLabel* charging_label = new QLabel(i18n("Charging:"), icons_groupbox);
        charging_label->setFixedSize(100, 24);
        icon_grid->addLayout(a02, 0, 2);
        a02->addStretch(1);
        a02->addWidget(charging_label,0);
        a02->addStretch(1);

        QHBoxLayout *a10 = new QHBoxLayout;
        buttonNoBattery = new KIconButton(iconloader, icons_groupbox);
        buttonNoBattery->setFixedSize(50, 50);
	buttonNoBattery->setIconType(KIcon::NoGroup, KIcon::Any, 0);
  	connect(buttonNoBattery, SIGNAL(iconChanged(QString)), this, SLOT(configChanged()));
        //icon_grid->addLayout(a10, 1, 0);    
       	//buttonNoBattery->setFixedSize(50, 50);
        icon_grid->addLayout(a10, 1, 0);
        a10->addStretch(1);
	a10->addWidget(buttonNoBattery,0);
	a10->addStretch(1);

        QHBoxLayout *a11 = new QHBoxLayout;
        buttonNoCharge = new KIconButton(iconloader, icons_groupbox);
        buttonNoCharge->setFixedSize(50, 50);
	buttonNoCharge->setIconType(KIcon::NoGroup, KIcon::Any, 0);
  	connect(buttonNoCharge, SIGNAL(iconChanged(QString)), this, SLOT(configChanged()));
        icon_grid->addLayout(a11, 1, 1);
        a11->addStretch(1);
        a11->addWidget(buttonNoCharge,0);
        a11->addStretch(1);

        QHBoxLayout *a12 = new QHBoxLayout;
        buttonCharge = new KIconButton(iconloader, icons_groupbox);
	buttonCharge->setFixedSize(50, 50);
	buttonCharge->setIconType(KIcon::NoGroup, KIcon::Any, 0);
  	connect(buttonCharge, SIGNAL(iconChanged(QString)), this, SLOT(configChanged()));
        icon_grid->addLayout(a12, 1, 2);
        a12->addStretch(1);
        a12->addWidget(buttonCharge,0);
        a12->addStretch(1);

        QLabel* explain = new QLabel(i18n("This panel controls whether the battery status\nmonitor appears in the dock and what it looks like."), this);
        explain->setMinimumSize(explain->sizeHint());
        top_layout->addWidget(explain, 0);

        top_layout->addStretch(1);


        QHBoxLayout *v1 = new QHBoxLayout;
        top_layout->addLayout(v1, 0);
        v1->addStretch(1);
        QString s1 = LAPTOP_VERSION;
        QString s2 = i18n("Version: ")+s1;
        QLabel* vers = new QLabel(s2, this);
        vers->setMinimumSize(vers->sizeHint());
        v1->addWidget(vers, 0);


        top_layout->activate();                  
     }
     config =  new KConfig("kcmlaptoprc");

     load();      
}

BatteryConfig::~BatteryConfig()
{
}


void BatteryConfig::save()
{
        if (apm) {
                poll_time = editPoll->text();
                enablemonitor = runMonitor->isChecked();
                nobattery =  buttonNoBattery->icon();
                chargebattery =  buttonCharge->icon();
                nochargebattery =  buttonNoCharge->icon();
        }
        config->setGroup("BatteryDefault");

        config->writeEntry("Enable", enablemonitor);
        config->writeEntry("Poll", poll_time);
        config->writeEntry("NoBatteryPixmap", nobattery);
        config->writeEntry("ChargePixmap", chargebattery);
        config->writeEntry("NoChargePixmap", nochargebattery);
	config->sync();      
        changed(false);
	::system("klaptopdaemon&");
}

void BatteryConfig::load()
{
       	config->setGroup("BatteryDefault");

        poll_time = config->readEntry("Poll", "20");
        enablemonitor = config->readBoolEntry("Enable", false);

        nobattery = config->readEntry("NoBatteryPixmap", "laptop_nobattery");
        nochargebattery = config->readEntry("NoChargePixmap", "laptop_nocharge");
        chargebattery = config->readEntry("ChargePixmap", "laptop_charge");

        if (apm) {
                editPoll->setText(poll_time);
                buttonNoCharge->setIcon(nochargebattery);
                //buttonNoCharge->setPixmap(kapp->iconLoader()->loadIcon(nochargebattery));
                buttonCharge->setIcon(chargebattery);
		//buttonCharge->setPixmap(kapp->iconLoader()->loadIcon(chargebattery));
	        buttonNoBattery->setIcon(nobattery);
                //buttonNoBattery->setPixmap(kapp->iconLoader()->loadIcon(nobattery));
                runMonitor->setChecked(enablemonitor);
        }                                                 
        changed(false);
}

void BatteryConfig::defaults()
{
       poll_time = "20";
        enablemonitor = false;

        nobattery = "laptop_nobattery";
        nochargebattery = "laptop_nocharge";
        chargebattery = "laptop_charge";

        if (apm) {
                editPoll->setText(poll_time);
                buttonNoCharge->setIcon(nochargebattery);
                //buttonNoCharge->setPixmap(kapp->iconLoader()->loadIcon(nochargebattery));
                buttonCharge->setIcon(chargebattery);
                //buttonCharge->setPixmap(kapp->iconLoader()->loadIcon(chargebattery));
                buttonNoBattery->setIcon(nobattery);
                //buttonNoBattery->setPixmap(kapp->iconLoader()->loadIcon(nobattery));
                runMonitor->setChecked(enablemonitor);
        }          
        changed(false);
}


void BatteryConfig::configChanged()
{
  emit changed(true);
}


QString BatteryConfig::quickHelp() const
{
  return i18n("<h1>Battery Monitor</h1>This module allows you to monitor "
	"your batteries. To make use of this module, you must have APM "
	"installed. (And, of course, you should have batteries in your "
	"machine.)");
}


#include "battery.moc"


