/*
 * pcmcia.h
 *
 * Copyright (c) 1999 Paul Campbell <paul@taniwha.com>
 *
 * Requires the Qt widget libraries, available at no cost at
 * http://www.troll.no/
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */


#ifndef __WARNINGCONFIG_H__
#define __WARNINGCONFIG_H__

#include <qdialog.h>
#include <qpushbutton.h>
#include <qlabel.h>
#include <qlcdnumber.h>
#include <qradiobutton.h>
#include <qbuttongroup.h>
#include <qcheckbox.h>

#include <kapp.h>
#include <knuminput.h>

#include <kcmodule.h>

class WarningConfig : public KCModule
{
  Q_OBJECT
public:
  WarningConfig(int x, QWidget *parent=0, const char* name=0);

  void save( void );
  void load();
  void defaults();

  virtual QString quickHelp() const;

public slots:
  void enableRunCommand(bool);
  void enablePlaySound(bool);
  void browseRunCommand();
  void browsePlaySound();         
private slots:
  void configChanged();
    
private:
  void my_load(int x);

  KConfig *config;
 
  QLineEdit *editRunCommand;
  QLineEdit *editPlaySound;
  QLineEdit* editLow;
 
  QCheckBox *checkRunCommand;
  QCheckBox *checkPlaySound;
  QCheckBox *checkBeep;
  QCheckBox *checkNotify;
  QCheckBox *checkSuspend;
  QCheckBox *checkStandby;
  QCheckBox *checkHibernate;
 
  QPushButton *buttonBrowsePlaySound;
  QPushButton *buttonBrowseRunCommand;
 
  bool    apm, runcommand, playsound, beep, notify, do_suspend, do_standby, do_hibernate;
  QString runcommand_val, low_val, crit_val, sound_val;
  int             have_time, type;        

};

#endif

