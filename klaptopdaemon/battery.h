/*
 * pcmcia.h
 *
 * Copyright (c) 1999 Paul Campbell <paul@taniwha.com>
 *
 * Requires the Qt widget libraries, available at no cost at
 * http://www.troll.no/
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */


#ifndef __BATTERYCONFIG_H__
#define __BATTERYCONFIG_H__

#include <qdialog.h>
#include <qpushbutton.h>
#include <qlabel.h>
#include <qlcdnumber.h>
#include <qradiobutton.h>
#include <qbuttongroup.h>
#include <qcheckbox.h>
#include <qlineedit.h>

#include <kapp.h>
#include <knuminput.h>
#include <kiconloader.h>
#include <kicondialog.h>
#include <klineeditdlg.h>

#include <kcmodule.h>

class BatteryConfig : public KCModule
{
  Q_OBJECT
public:
  BatteryConfig( QWidget *parent=0, const char* name=0);
  ~BatteryConfig( );     

  void save( void );
  void load();
  void defaults();

  virtual QString quickHelp() const;

private slots:

  void configChanged();

private:
	KConfig *config;

        QLineEdit* editPoll;
        QCheckBox* runMonitor;
        bool            enablemonitor;

	KIconLoader *iconloader;

        KIconButton *buttonNoBattery;
        KIconButton *buttonNoCharge;
        KIconButton *buttonCharge;
        QString nobattery, nochargebattery, chargebattery;
        bool    apm;
        QString poll_time;       

};

#endif

