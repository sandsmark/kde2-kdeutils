/*
 * warning.cpp
 *
 * Copyright (c) 1999 Paul Campbell <paul@taniwha.com>
 *
 * Requires the Qt widget libraries, available at no cost at
 * http://www.troll.no/
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <iostream> 

#include <sys/types.h>
#include <unistd.h>
#include <sys/stat.h>
#include <stdlib.h>

#include <qfileinfo.h> 
#include <qstring.h>
#include <qhbuttongroup.h>
#include <qlayout.h>
#include <qlineedit.h>

#include <klocale.h>
#include <kconfig.h>
#include <kfiledialog.h>
#include <stdio.h>
#include <kmessagebox.h>
#include <knumvalidator.h>

#include "warning.h"
#include "portable.h"
#include "version.h"

WarningConfig::WarningConfig (int t, QWidget * parent, const char *name)
  : KCModule(parent, name)
{
  type = t;
  apm = laptop_portable::has_power_management();

  config =  new KConfig("kcmlaptoprc");
  my_load(0);
  if (!apm) {
      	QVBoxLayout *top_layout = new QVBoxLayout(this, 12, 5);
	
        QLabel* explain = laptop_portable::no_power_management_explanation(this);
      	top_layout->addWidget(explain, 0);

      	top_layout->addStretch(1);

      	top_layout->activate();

    } else {
	QVBoxLayout *top_layout = new QVBoxLayout(this, 10, 5);

	QGridLayout *grid = new QGridLayout(6, 6, 5);
	top_layout->addLayout(grid);

	grid->addColSpacing(2, 5);
	grid->addColSpacing(5, 5);

	QLabel* low_label = 0;
	if (type) {
		if (have_time == 1) {
			low_label = new QLabel(i18n("Critical trigger (minutes):"), this);
		} else {
			low_label = new QLabel(i18n("Critical trigger (percent):"), this);
		}
	} else {
		if (have_time == 1) {
			low_label = new QLabel(i18n("Low trigger (minutes):"), this);
		} else {
			low_label = new QLabel(i18n("Low trigger (percent):"), this);
		}
	}
	low_label->setMinimumSize(low_label->sizeHint());
	grid->addWidget(low_label, 0, 0);

        editLow = new QLineEdit(this);
	editLow->setValidator( new KIntValidator( 1,3600,editLow ) );
        editLow->setMinimumSize(editLow->sizeHint());
  	connect(editLow, SIGNAL(textChanged(const QString&)), this, SLOT(configChanged()));
        grid->addWidget(editLow, 0, 1);   


	// setup the Run Command stuff
	checkRunCommand = new QCheckBox(i18n("Run Command"), this);
	checkRunCommand->setMinimumSize(checkRunCommand->sizeHint());
	grid->addWidget(checkRunCommand, 1, 0);

	editRunCommand = new QLineEdit(this);
	editRunCommand->setMinimumSize(editRunCommand->sizeHint());
	grid->addWidget(editRunCommand, 2, 0);
	connect(editRunCommand,SIGNAL(textChanged ( const QString & )),this, SLOT(configChanged()));

	buttonBrowseRunCommand = new QPushButton(i18n("Browse"), this);
	buttonBrowseRunCommand->setMinimumSize(75, 25);
	buttonBrowseRunCommand->setMaximumSize(buttonBrowseRunCommand->sizeHint());
	grid->addWidget(buttonBrowseRunCommand, 2, 1);

	// setup the Play Sound stuff
	checkPlaySound = new QCheckBox(i18n("Play Sound"), this);
	checkPlaySound->setMinimumSize(checkPlaySound->sizeHint());
	grid->addWidget(checkPlaySound, 3, 0);

	editPlaySound = new QLineEdit(this);
	editPlaySound->setMinimumSize(editPlaySound->sizeHint());
	grid->addWidget(editPlaySound, 4, 0);
	connect(editPlaySound,SIGNAL(textChanged ( const QString & )),this, SLOT(configChanged()));

	buttonBrowsePlaySound = new QPushButton(i18n("Browse"), this);
	buttonBrowsePlaySound->setMinimumSize(75, 25);
	buttonBrowsePlaySound->setMaximumSize(buttonBrowsePlaySound->sizeHint());
	grid->addWidget(buttonBrowsePlaySound, 4, 1);

	// setup the System Sound stuff
	checkBeep = new QCheckBox(i18n("System Beep"), this);
	checkBeep->setMinimumSize(checkBeep->sizeHint());
	top_layout->addWidget(checkBeep);
	connect(checkBeep, SIGNAL(clicked()), this, SLOT(configChanged()));  

	// setup the System Sound stuff
	checkNotify = new QCheckBox(i18n("Notify"), this);
	checkNotify->setMinimumSize(checkNotify->sizeHint());
	top_layout->addWidget(checkNotify);
	connect(checkNotify, SIGNAL(clicked()), this, SLOT(configChanged()));  

	int can_suspend = laptop_portable::has_suspend();
	int can_standby = laptop_portable::has_standby();
	int can_hibernate = laptop_portable::has_hibernation();

	if (can_suspend) {
		checkSuspend = NULL;
	} else {
		checkSuspend = new QCheckBox(i18n("Suspend"), this);
		checkSuspend->setMinimumSize(checkSuspend->sizeHint());
		top_layout->addWidget(checkSuspend);
		connect(checkSuspend, SIGNAL(clicked()), this, SLOT(configChanged()));  
	}
	if (can_standby ) {
		checkStandby = new QCheckBox(i18n("Standby"), this);
		checkStandby->setMinimumSize(checkStandby->sizeHint());
		top_layout->addWidget(checkStandby);
		connect(checkStandby, SIGNAL(clicked()), this, SLOT(configChanged()));  
	} else {
		checkStandby = NULL;
	}
	if (can_hibernate ) {
		checkHibernate = new QCheckBox(i18n("Hibernate"), this);
		checkHibernate->setMinimumSize(checkHibernate->sizeHint());
		top_layout->addWidget(checkHibernate);
		connect(checkHibernate, SIGNAL(clicked()), this, SLOT(configChanged()));  
	} else {
		checkHibernate = NULL;
	}
        QLabel* explain = 0;

	if (type) {
		explain = new QLabel(i18n("This panel controls how and when you receive warnings\nthat your battery power is going to run out VERY VERY soon."), this);
	} else {
		explain = new QLabel(i18n("This panel controls how and when you receive warnings\nthat your battery power is about to run out"), this);
	}
        explain->setMinimumSize(explain->sizeHint());
        top_layout->addWidget(explain, 0);

	if (!can_suspend || !can_standby || !can_hibernate) {
        	QLabel* note = laptop_portable::how_to_do_suspend_resume(this);
        	top_layout->addWidget(note, 0);
		
	}

	top_layout->addStretch(1);


	QHBoxLayout *v1 = new QHBoxLayout;
        top_layout->addLayout(v1, 0);
	v1->addStretch(1);
	QString s1 = LAPTOP_VERSION;
	QString s2 = i18n("Version: ")+s1;
        QLabel* vers = new QLabel(s2, this);
	vers->setMinimumSize(vers->sizeHint());
        v1->addWidget(vers, 0);
	
	// connect some slots and signals
	connect(buttonBrowsePlaySound, SIGNAL(clicked()),
	                                 SLOT(browsePlaySound()));
	connect(buttonBrowseRunCommand, SIGNAL(clicked()),
	                                  SLOT(browseRunCommand()));
	connect(checkPlaySound, SIGNAL(toggled(bool)),
	                          SLOT(enablePlaySound(bool)));
	connect(checkRunCommand, SIGNAL(toggled(bool)),
	                           SLOT(enableRunCommand(bool)));

	top_layout->activate();


    }
    my_load(1);

}


void WarningConfig::save()
{
    if (apm) {
    	runcommand = checkRunCommand->isChecked();
    	playsound = checkPlaySound->isChecked();
    	beep = checkBeep->isChecked();
    	notify = checkNotify->isChecked();
    	do_suspend = (checkSuspend? checkSuspend->isChecked() : false);
    	do_standby = (checkStandby? checkStandby->isChecked() : false);
    	do_hibernate = (checkHibernate? checkHibernate->isChecked() : false);
    	runcommand_val = editRunCommand->text();
    	low_val = editLow->text();
    	sound_val = editPlaySound->text();
		
    }
    config->setGroup((type?"BatteryCritical":"BatteryLow"));
       
    config->writeEntry("LowVal", low_val);
    config->writeEntry("RunCommand", runcommand);
    config->writeEntry("PlaySound", playsound);
    config->writeEntry("SystemBeep", beep);
    config->writeEntry("Notify", notify);
    config->writeEntry("Suspend", do_suspend);
    config->writeEntry("Standby", do_standby);
    config->writeEntry("Hibernate", do_hibernate);
    config->writeEntry("RunCommandPath", runcommand_val);
    config->writeEntry("PlaySoundPath", sound_val);
    config->sync();
    ::system("klaptopdaemon&");
}

void WarningConfig::load()
{
	if (config == NULL)
		return;
	my_load(0);
	my_load(1);
}

void WarningConfig::my_load(int x)
{

	// open the config file
	if (!x) {
		config->setGroup((type?"BatteryCritical":"BatteryLow"));

		low_val = config->readEntry("LowVal", (type?"5":"15"));
		runcommand = config->readBoolEntry("RunCommand", false);
		playsound = config->readBoolEntry("PlaySound", false);
		beep = config->readBoolEntry("SystemBeep", true);
		notify = config->readBoolEntry("Notify", (type && checkSuspend ? false : true));
		do_suspend = config->readBoolEntry("Suspend", (type && checkSuspend ? true :false));
		do_standby = config->readBoolEntry("Standby", false);
		do_hibernate = config->readBoolEntry("Hibernate", false);
		runcommand_val = config->readEntry("RunCommandPath");
		sound_val = config->readEntry("PlaySoundPath");
		have_time = config->readNumEntry("HaveTime", 2);
		if (laptop_portable::has_power_management())
			have_time = 1;
	
	} else
	if (apm) {
		checkRunCommand->setChecked(runcommand);
		checkPlaySound->setChecked(playsound);
		checkBeep->setChecked(beep);
		checkNotify->setChecked(notify);
		if (checkHibernate)
			checkHibernate->setChecked(do_hibernate);
		if (checkStandby)
			checkStandby->setChecked(do_standby);
		if (checkSuspend)
			checkSuspend->setChecked(do_suspend);
		editRunCommand->setText(runcommand_val);
		editLow->setText(low_val);
		editPlaySound->setText(sound_val);
	
		enableRunCommand(checkRunCommand->isChecked());
		enablePlaySound(checkPlaySound->isChecked());
	}
	changed(false);

}

void WarningConfig::defaults()
{
	// open the config file
	runcommand = 0;
	playsound = 0;
	beep = 1;
	notify = (type && checkSuspend ? 0:1);
	do_standby = 0;
	do_hibernate = 0;
	do_suspend = (type && checkSuspend ? 1:0);
	runcommand_val = "";
	low_val = (type?"5":"15");
	sound_val = "";
	

	if (apm) {
		checkRunCommand->setChecked(runcommand);
		checkPlaySound->setChecked(playsound);
		checkBeep->setChecked(beep);
		checkNotify->setChecked(notify);
		if (checkHibernate)
			checkHibernate->setChecked(do_hibernate);
		if (checkStandby)
			checkStandby->setChecked(do_standby);
		if (checkSuspend)
			checkSuspend->setChecked(do_suspend);
		editRunCommand->setText(runcommand_val);
		editLow->setText(low_val);
		editPlaySound->setText(sound_val);
	
		enableRunCommand(checkRunCommand->isChecked());
		enablePlaySound(checkPlaySound->isChecked());
	}
	changed(false);
}


void WarningConfig::configChanged()
{
  emit changed(true);
}


void WarningConfig::enableRunCommand(bool enable)
{
	editRunCommand->setEnabled(enable);
	buttonBrowseRunCommand->setEnabled(enable);
	configChanged();
}

void WarningConfig::enablePlaySound(bool enable)
{
	editPlaySound->setEnabled(enable);
	buttonBrowsePlaySound->setEnabled(enable);
	configChanged();
}

void WarningConfig::browseRunCommand()
{
  KURL url = KFileDialog::getOpenURL(QString::null, QString::null, this  );
  
  if( url.isEmpty() )
    return;
  
  if( !url.isLocalFile() )
  {
    KMessageBox::sorry( 0L, i18n( "Only local files are currently supported." ) );
    return;
  }
  
  editRunCommand->setText( url.path() );
  configChanged();
}

void WarningConfig::browsePlaySound()
{
  KURL url = KFileDialog::getOpenURL(QString::null, QString::null, this  );
  
  if( url.isEmpty() )
    return;
  
  if( !url.isLocalFile() )
  {
    KMessageBox::sorry( 0L, i18n( "Only local files are currently supported." ) );
    return;
  }
  
  editPlaySound->setText( url.path() );
  configChanged();
}


QString WarningConfig::quickHelp() const
{
  return i18n("<h1>Low battery Warning</h1>This module allows you to "
	"set an alarm in case your battery's capacity is exceeded.");
}

#include "warning.moc"
