#ifndef LAPTOPDAEMON
#define LAPTOPDAEMON 1
/*
 * laptop_daemon.h
 * Copyright (C) 1999 Paul Campbell <paul@taniwha.com>
 *
 * This file contains the implementation of the main laptop battery monitoring daemon
 *
 * $Id: laptop_daemon.h 91398 2001-04-10 22:14:08Z antlarr $
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */


#include <qdir.h>
#include <unistd.h>
#include <time.h>
#include <qmovie.h>
#include <qlist.h>
#include <qfileinfo.h>
#include <qimage.h>

#include <kiconloader.h>
#include <kprocess.h>
//#include <kaudio.h>
#include <qtooltip.h>

#include <kdockwindow.h>
#include <kuniqueapp.h>

#include <kdebug.h>

#include "kpcmcia.h"

#include "daemondock.h"

class laptop_dock;
class laptop_daemon: public KUniqueApplication
{
	Q_OBJECT
public:
    	laptop_daemon();
    	virtual ~laptop_daemon();     
	virtual int newInstance();     
        void 	setPollInterval(const int poll=60);
signals:
        void 	signal_checkBattery();
protected:
	 void 	timerEvent(QTimerEvent *); 
protected slots:         
	void 	checkBatteryNow();
	void 	timerDone();
	void	dock_quit();
        void    updatePCMCIA(int num);
private:
	void	restart();
	void 	haveBatteryLow(int t, const int num, const int type);
	int	calcBatteryTime(int percent, long time, bool restart);
	void 	start_monitor();
	void 	invokeStandby();
	void 	invokeSuspend();
	void	displayPixmap();
	
	laptop_dock *dock_widget;

	// Capability
	bool    hasAudio;
	//KAudio  audioServer;
	
	// General settings
public:
	int	val;
	int	exists;
	int	powered;
	int	left;
	QString noBatteryIcon;
	QString chargeIcon;
	QString noChargeIcon;
protected:
	int	triggered[2];

	int	oldval, oldexists, oldpowered, oldleft;

	int	changed;

	//
	//	power out actions
	//

	int	power_wait[2];		// how close to the end when we trigger the action
	int	power_action[2];	// what to do when this action is triggered

	//
	//	power out warnings
	//

	bool    systemBeep[2];		
	bool    runCommand[2];
	QString runCommandPath[2];
	bool    playSound[2];
	QString playSoundPath[2];
	bool    notify[2];
	bool    do_suspend[2];
	bool    do_standby[2];
	int	low[2];
	int	poll;			// how often to pol


	int     oldTimer;
	QTimer  *timer;

	bool    	enabled, backoff;
	unsigned long	power_time;
	unsigned long	last_time;


	int	have_time;

        // PCMCIA related
        KPCMCIA *_pcmcia;
};
#endif
