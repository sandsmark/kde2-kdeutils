/*
 * pcmcia.h
 *
 * Copyright (c) 1999 Paul Campbell <paul@taniwha.com>
 *
 * Requires the Qt widget libraries, available at no cost at
 * http://www.troll.no/
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */


#ifndef __POWERCONFIG_H__
#define __POWERCONFIG_H__

#include <qdialog.h>
#include <qpushbutton.h>
#include <qlabel.h>
#include <qlcdnumber.h>
#include <qradiobutton.h>
#include <qbuttongroup.h>
#include <qlineedit.h>

#include <kapp.h>
#include <knuminput.h>

#include <kcmodule.h>

class PowerConfig : public KCModule
{
  Q_OBJECT
public:
  PowerConfig( QWidget *parent=0, const char* name=0);

  void save( void );
  void load();
  void defaults();

  virtual QString quickHelp() const;

private slots:

  void configChanged();

    
private:
       
  int  getPower();
  int  getNoPower();
  void setPower( int, int );

  QButtonGroup *nopowerBox;
  QRadioButton *nopowerStandby, *nopowerSuspend, *nopowerOff, *nopowerHibernate;
  QButtonGroup *powerBox;
  QRadioButton *powerStandby, *powerSuspend, *powerOff, *powerHibernate;
  QLineEdit *noeditwait;
  QLineEdit *editwait;
  QString edit_wait;
  QString noedit_wait;

  KConfig *config;
  int power, nopower, apm;
};

#endif

