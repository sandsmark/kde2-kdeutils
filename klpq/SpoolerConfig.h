/**********************************************************************

	--- Qt Architect generated file ---

	File: SpoolerConfig.h
	Last generated: Sat Feb 7 11:59:44 1998

 *********************************************************************/

#ifndef SpoolerConfig_included
#define SpoolerConfig_included

#include <qstring.h>
#include <kdialogbase.h>

class QRadioButton;
class QLineEdit;
class KURLRequester;

class SpoolerConfig : public KDialogBase
{
  Q_OBJECT

  private:
    struct SCmdStrings
    {
      QString lpq;
      QString lpc;
      QString lprm;
    };

  public:
    SpoolerConfig( QWidget *parent=0, const char *name=0, bool modal=true );
    virtual ~SpoolerConfig( void );

    void    initConfig( const QString &name );
    QString saveConfig( void );

  private slots:
    void spoolerChanged( int button );

  private:
    QRadioButton *mRadio[3];
    KURLRequester    *mLineEdit[3];
    int mCurrentButton;
    SCmdStrings mBsd;
    SCmdStrings mPpr;
    SCmdStrings mLprng;
};

#endif // SpoolerConfig_included
