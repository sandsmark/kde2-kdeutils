// -*- C++ -*-

//
//  klpq
//
//  Copyright (C) 1997 Christoph Neerfeld
//  email:  Christoph.Neerfeld@home.ivm.de or chris@kde.org
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//

#include <stdlib.h>

#include <qstring.h>
#include <qfile.h>
#include <qtextstream.h>

#include <kapp.h>
#include <kdebug.h>
#include <klocale.h>
#include <kconfig.h>
#include <kcmdlineargs.h>
#include <kaboutdata.h>
#include <kmessagebox.h>

#include "klpq.h"

static const char *description =
	I18N_NOOP("KDE Print Queue utility.");

static KCmdLineOptions option[] =
{
   { "n", 0, 0 },
   { "P <printer>", I18N_NOOP("Print to 'printer'"), 0},
   { "+[file(s)]", I18N_NOOP("Files to print"), 0 },
   { 0, 0, 0 }
};

int main( int argc, char **argv )
{
  KAboutData aboutData("klpq", I18N_NOOP("Print Queue"),
    "1.01", description, KAboutData::License_GPL,
    "(c) 1997, Christoph Neerfeld");
  aboutData.addAuthor("Christoph Neerfeld",0, "chris@kde.org");
  aboutData.addAuthor("Hans Petter Bieker", 0, "bieker@kde.org");
  KCmdLineArgs::init( argc, argv, &aboutData );
  KCmdLineArgs::addCmdLineOptions( option );

  KApplication a;

  KConfig *config = KGlobal::config();
  config->setGroup( QString::fromLatin1("klpq") );

  QString choice = QString::fromLatin1(getenv("PRINTER"));
  QString last_printer = config->readEntry( QString::fromLatin1("lastQueue"),
					    QString::fromLatin1("lp"));

  KCmdLineArgs *args = KCmdLineArgs::parsedArgs();

  if (args->isSet("P"))
     choice = QString::fromLatin1(args->getOption("P"));

  bool err = false;
  for( int i = 0; i < args->count(); i++ )
  {
    if (!Klpq::printURL(args->url(i)))
      err = true;
  }
  if (err) Klpq::messageNotFound();
  if (args->count() != 0)
    exit(0); // we don't want the window if we drop some files

  args->clear();

  Klpq *main_widget = new Klpq;

  bool empty_printcap = TRUE;

  bool no_printcap = TRUE;
  int iprintcap=0,nprintcap=1;
  char pcpath[3][80]={"/etc/printers.conf", "/etc/printcap",
                      "/usr/local/etc/printcap"};
  while (iprintcap <= nprintcap)
    {
      QFile printcap( QString::fromLatin1(pcpath[iprintcap]) );
      if( !printcap.open(IO_ReadOnly) )
	{
	  iprintcap++;
	  continue;
	}
      no_printcap = FALSE;
      QTextStream st( (QIODevice *) &printcap);
      while( !st.eof() )
	{
	  QString temp = st.readLine();
	  temp = temp.stripWhiteSpace();
	  if( temp[0] == '#' || temp.isEmpty() )
	    continue;
	  QString name = temp.left( temp.find(':') );
	  if( name.isEmpty() )
	    continue;
	  //debug("name = %s", (const char *) name);
	  if( name.contains('|') )
	    name = name.left( name.find('|') );
	  name = name.stripWhiteSpace();
	  if( name.isEmpty() )
	    continue;
	  else
	    empty_printcap = FALSE;
	  /*
	  do {
	    temp = st.readLine();
	    temp = temp.stripWhiteSpace();
	    if( temp[0] == '#')
	      temp = ":";
	  } while( temp.simplifyWhiteSpace().at(0) == ':');
	  */
          if(name.right(1) == QString::fromLatin1("\\") )
            name = name.left(name.length() - 1);
	  main_widget->addPrintQueue(name);
	  if( choice.isEmpty() && name == last_printer )
	    main_widget->setLastPrinterCurrent();
	  if( name == choice)
	    main_widget->setLastPrinterCurrent();
	}
      printcap.close();
      iprintcap++;
    }
  if( no_printcap )
    {
      KMessageBox::sorry(0, i18n("Sorry, can't open your printcap file."));
      exit(1);
    }
  if( empty_printcap )
    {
      KMessageBox::sorry(0, i18n("Unable to parse your printcap - probably you haven't configured any printers yet."));
      exit(1);
    }

  a.setMainWidget( main_widget );
  main_widget->callUpdate();
  main_widget->show();
  return a.exec();
}
