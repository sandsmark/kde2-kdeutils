// 1999-12-14 Espen Sand
// Changed to KDialogBase. The translators have had problems
// to work with the previous fixed size version.

#include "ConfAutoUpdate.h"
#include "ConfAutoUpdate.moc"

#include <qlabel.h>
#include <qlcdnumber.h>
#include <qlayout.h>
#include <qslider.h>

#include <klocale.h>

ConfAutoUpdate::ConfAutoUpdate(	QWidget *parent, const char* name, bool modal )
  : KDialogBase( parent, name, modal, i18n("Update Frequency"), Ok|Cancel, Ok )
{
  QWidget *page = new QWidget( this ); 
  setMainWidget(page);

  QVBoxLayout *topLayout = new QVBoxLayout( page, 0, spacingHint() );
  topLayout->addSpacing( spacingHint() );
  topLayout->addStretch( 5 );
  QHBoxLayout *hlay = new QHBoxLayout( topLayout );
  QVBoxLayout *vlay = new QVBoxLayout( hlay, 10 );
  topLayout->addSpacing( spacingHint() );
  topLayout->addStretch( 5 );

  QLabel *label = new QLabel( page, "Label_1" );
  label->setText( i18n("Update frequency in seconds:") );
  vlay->addWidget( label );

  mFrequencySlider = new QSlider( page, "Slider_1" );
  mFrequencySlider->setOrientation( Horizontal );
  mFrequencySlider->setFocusPolicy( WheelFocus ); 
  vlay->addWidget( mFrequencySlider );

  mFrequencyLCD = new QLCDNumber( page, "LCDNumber_1" );
  mFrequencyLCD->setFrameStyle( QFrame::WinPanel|QFrame::Sunken );
  mFrequencyLCD->setSmallDecimalPoint( false );
  mFrequencyLCD->setNumDigits( 3 );
  mFrequencyLCD->setMode( QLCDNumber::DEC );
  hlay->addWidget( mFrequencyLCD, 0 );

  connect( mFrequencySlider, SIGNAL(valueChanged(int)), 
	   mFrequencyLCD, SLOT(display(int)) );
}


ConfAutoUpdate::~ConfAutoUpdate( void )
{

}


void ConfAutoUpdate::setFrequency( int sec ) 
{ 
  mFrequencySlider->setValue( sec ); 
}

int ConfAutoUpdate::getFrequency( void ) 
{ 
  return( mFrequencySlider->value() ); 
}

