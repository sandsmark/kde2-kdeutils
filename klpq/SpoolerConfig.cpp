/*
  This file is part of klpq (C) 1998 Christoph Neerfeld
*/
/**********************************************************************

	--- Qt Architect generated file ---

	File: SpoolerConfig.cpp
	Last generated: Sat Feb 7 11:59:44 1998

 *********************************************************************/

// 1999-12-14 Espen Sand
// Changed to KDialogBase. The translators have had problems
// to work with the previous fixed size version.


#include <qbuttongroup.h>
#include <qlabel.h>
#include <qlayout.h>
#include <qlineedit.h>
#include <qradiobutton.h>

#include <kglobal.h>
#include <kconfig.h>
#include <klocale.h>
#include <kseparator.h>
#include <kurlrequester.h>
#include <klineedit.h>
#include "SpoolerConfig.h"
#include "SpoolerConfig.moc"


SpoolerConfig::SpoolerConfig( QWidget *parent, const char *name, bool modal )
  : KDialogBase( parent, name, modal, i18n("Spooler"), Ok|Cancel, Ok )
{
  
  QWidget *page = new QWidget( this ); 
  setMainWidget(page);
  QVBoxLayout *topLayout = new QVBoxLayout( page, 0, spacingHint() );

  QLabel *label = new QLabel( page, "label" );
  label->setText( i18n("Select your spooling system") );
  topLayout->addWidget( label );

  topLayout->addSpacing( spacingHint() );


  QHBoxLayout *hlay = new QHBoxLayout( topLayout );
  
  QButtonGroup *group = new QButtonGroup( page, "buttongroup" );
  group->setFrameStyle( QFrame::NoFrame );
  connect( group, SIGNAL(clicked(int)), this, SLOT(spoolerChanged(int)) );
  hlay->addWidget( group ); 

  QVBoxLayout *vlay = new QVBoxLayout( group, spacingHint(), spacingHint() );

  QString radioText[3] = 
  {
    i18n("BSD"),
    i18n("PPR"),
    i18n("LPRNG")
  };
  for( int i=0; i<3; i++ )
  {
    mRadio[i] = new QRadioButton( group, "radio" );
    mRadio[i]->setText( radioText[i] );
    vlay->addWidget( mRadio[i] );
  }

  KSeparator *sep = new KSeparator( page, "sep" );
  sep->setOrientation( QFrame::VLine );
  hlay->addWidget( sep );

  QGridLayout *gridLayout = new QGridLayout( hlay, 4, 2, spacingHint() );
  gridLayout->setColStretch( 1, 10 );

  label = new QLabel( page, "label" );
  label->setText( i18n("Paths of spooling commands:") );
  gridLayout->addMultiCellWidget( label, 0, 0, 0, 1 );
  
  const QString cmdText[3] = 
  {
    i18n("lpq:"),
    i18n("lpc:"),
    i18n("lprm:")
  };
  for( int i=0; i<3; i++ )
  {
    label = new QLabel( page, "label" );
    label->setText( cmdText[i] );
    gridLayout->addWidget( label, i+1, 0 );
    
    mLineEdit[i] = new KURLRequester( page, "lineedit" );
    mLineEdit[i]->setMinimumWidth( fontMetrics().maxWidth()*15 );
    gridLayout->addWidget( mLineEdit[i], i+1, 1 );
  }

  topLayout->addSpacing( spacingHint() );
  topLayout->addStretch( 10 );
}

SpoolerConfig::~SpoolerConfig()
{
}


void SpoolerConfig::initConfig( const QString &spoolerName )
{
  KConfig &config = *KGlobal::config();

  config.setGroup(QString::fromLatin1("klpqBsd"));
  mBsd.lpq  = config.readEntry(QString::fromLatin1("lpqCommand"), QString::fromLatin1("lpq"));
  mBsd.lpc  = config.readEntry(QString::fromLatin1("lpcCommand"), QString::fromLatin1("/usr/sbin/lpc"));
  mBsd.lprm = config.readEntry(QString::fromLatin1("lprmCommand"), QString::fromLatin1("lprm"));

  config.setGroup(QString::fromLatin1("klpqPpr"));
  mPpr.lpq  = config.readEntry(QString::fromLatin1("lpqCommand"), QString::fromLatin1("ppop"));
  mPpr.lpc  = config.readEntry(QString::fromLatin1("lpcCommand"), QString::fromLatin1("ppop"));
  mPpr.lprm = config.readEntry(QString::fromLatin1("lprmCommand"), QString::fromLatin1("ppop"));

  config.setGroup(QString::fromLatin1("klpqLprng"));
  mLprng.lpq  = config.readEntry(QString::fromLatin1("lpqCommand"), QString::fromLatin1("lpq"));
  mLprng.lpc  = config.readEntry(QString::fromLatin1("lpcCommand"), QString::fromLatin1("/usr/sbin/lpc"));
  mLprng.lprm = config.readEntry(QString::fromLatin1("lprmCommand"), QString::fromLatin1("lprm"));

  if( spoolerName == QString::fromLatin1("PPR") )
  {
    mLineEdit[0]->lineEdit()->setText( mPpr.lpq );
    mLineEdit[1]->lineEdit()->setText( mPpr.lpc );
    mLineEdit[2]->lineEdit()->setText( mPpr.lprm );
    mRadio[1]->setChecked( true );
    mCurrentButton = 1;
  }
  else if( spoolerName == QString::fromLatin1("LPRNG") )
  {
    mLineEdit[0]->lineEdit()->setText( mLprng.lpq );
    mLineEdit[1]->lineEdit()->setText( mLprng.lpc );
    mLineEdit[2]->lineEdit()->setText( mLprng.lprm );
    mRadio[2]->setChecked( true );
    mCurrentButton = 2;
  }
  else
  {
    mLineEdit[0]->lineEdit()->setText( mBsd.lpq );
    mLineEdit[1]->lineEdit()->setText( mBsd.lpc );
    mLineEdit[2]->lineEdit()->setText( mBsd.lprm );
    mRadio[0]->setChecked( true );
    mCurrentButton = 0;
  }
}


QString SpoolerConfig::saveConfig( void )
{
  KConfig &config = *KGlobal::config();

  QString spoolerName;  
  if( mRadio[1]->isChecked() )
  {
    mPpr.lpq  = mLineEdit[0]->lineEdit()->text();
    mPpr.lpc  = mLineEdit[1]->lineEdit()->text();
    mPpr.lprm = mLineEdit[2]->lineEdit()->text();
    spoolerName = QString::fromLatin1("PPR");
  }
  else if( mRadio[2]->isChecked() )
  {
    mLprng.lpq  = mLineEdit[0]->lineEdit()->text();
    mLprng.lpc  = mLineEdit[1]->lineEdit()->text();
    mLprng.lprm = mLineEdit[2]->lineEdit()->text();
    spoolerName = QString::fromLatin1("LPRNG");
  }
  else
  {
    mBsd.lpq  = mLineEdit[0]->lineEdit()->text();
    mBsd.lpc  = mLineEdit[1]->lineEdit()->text();
    mBsd.lprm = mLineEdit[2]->lineEdit()->text();
    spoolerName = QString::fromLatin1("BSD");
  }
  config.setGroup(QString::fromLatin1("klpq"));
  config.writeEntry(QString::fromLatin1("Spooler"), spoolerName );

  config.setGroup(QString::fromLatin1("klpqPpr"));
  config.writeEntry(QString::fromLatin1("lpqCommand"), mPpr.lpq );
  config.writeEntry(QString::fromLatin1("lpcCommand"), mPpr.lpc );
  config.writeEntry(QString::fromLatin1("lprmCommand"), mPpr.lprm );
  config.setGroup(QString::fromLatin1("klpqLprng"));
  config.writeEntry(QString::fromLatin1("lpqCommand"), mLprng.lpq );
  config.writeEntry(QString::fromLatin1("lpcCommand"), mLprng.lpc );
  config.writeEntry(QString::fromLatin1("lprmCommand"), mLprng.lprm );
  config.setGroup(QString::fromLatin1("klpqBsd")); 
  config.writeEntry(QString::fromLatin1("lpqCommand"), mBsd.lpq );
  config.writeEntry(QString::fromLatin1("lpcCommand"), mBsd.lpc );
  config.writeEntry(QString::fromLatin1("lprmCommand"), mBsd.lprm );

  config.sync();

  return( spoolerName );
}



void SpoolerConfig::spoolerChanged( int button )
{
  //
  // Save current state.
  //
  if( mCurrentButton == 1 )
  {
    mPpr.lpq  = mLineEdit[0]->lineEdit()->text();
    mPpr.lpc  = mLineEdit[1]->lineEdit()->text();
    mPpr.lprm = mLineEdit[2]->lineEdit()->text();
  }
  else if( mCurrentButton == 2 )
  {
    mLprng.lpq  = mLineEdit[0]->lineEdit()->text();
    mLprng.lpc  = mLineEdit[1]->lineEdit()->text();
    mLprng.lprm = mLineEdit[2]->lineEdit()->text();
  }
  else
  {
    mBsd.lpq  = mLineEdit[0]->lineEdit()->text();
    mBsd.lpc  = mLineEdit[1]->lineEdit()->text();
    mBsd.lprm = mLineEdit[2]->lineEdit()->text();
    mCurrentButton = 0;
  }

  mCurrentButton = button;

  //
  // Print new state
  //
  if( button == 1 )
  {
    mLineEdit[0]->lineEdit()->setText( mPpr.lpq );
    mLineEdit[1]->lineEdit()->setText( mPpr.lpc );
    mLineEdit[2]->lineEdit()->setText( mPpr.lprm );
  }
  else if( button == 2 )
  {
    mLineEdit[0]->lineEdit()->setText( mLprng.lpq );
    mLineEdit[1]->lineEdit()->setText( mLprng.lpc );
    mLineEdit[2]->lineEdit()->setText( mLprng.lprm );
  }
  else
  {
    mLineEdit[0]->lineEdit()->setText( mBsd.lpq );
    mLineEdit[1]->lineEdit()->setText( mBsd.lpc );
    mLineEdit[2]->lineEdit()->setText( mBsd.lprm );
  }
}
