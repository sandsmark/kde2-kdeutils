// -*- C++ -*-

//
//  klpq
//
//  Copyright (C) 1997 Christoph Neerfeld
//  email:  Christoph.Neerfeld@home.ivm.de or chris@kde.org
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//

#ifndef KLPQ_H
#define KLPQ_H

#include <kmainwindow.h>
#include <kurl.h>

#include "ConfAutoUpdate.h"
#include "SpoolerConfig.h"

class QCheckBox;
class QComboBox;
class QFrame;
class QLabel;
class QListBox;
class QPushButton;
class QTimer;
class QAccel;
class QStringList;
class QPopupMenu;
class QDragEnterEvent;
class QDropEvent;

class KListView;
class KProcess;

class MyRowTable;
class Spooler;

class Klpq : public KMainWindow
{
  Q_OBJECT;
public:
  Klpq(const char* name=NULL);
  ~Klpq();

  void addPrintQueue( QString name ) { cb_printer->insertItem(name); }
  void setLastPrinterCurrent();
  void callUpdate() { update(); }
  static bool printURL(const KURL &url, const QString &printer = QString::null);
  static void messageNotFound();

protected slots:
  void update();
  void setAuto();
  void printerSelect(int) { update(); }
  void queuing();
  void printing();
  void remove();
  void makeTop();
  void prevPrinter();
  void nextPrinter();
  void configureAuto();
  void configureSpooler();
  void recvLpq( KProcess *, char *buffer, int buflen );
  void exitedLpq(KProcess *) { lpq_running = FALSE; updateList(); enable(); }
  void recvLpc( KProcess *, char *buffer, int buflen );
  void exitedLpc(KProcess *);
  void popupMenu();

protected:
  enum LpcCommand { NotRunning = 0, Status, Queuing, Printing, MakeTop };
  void enable();
  void disable();
  void updateList();

  // DnD
  void dragEnterEvent(QDragEnterEvent* event);
  void dropEvent(QDropEvent* event);

  QFrame      *f_main;
  QFrame      *f_top;
  QComboBox   *cb_printer;
  QLabel      *l_printer;
  QCheckBox   *c_queuing;
  QCheckBox   *c_printing;
  QPushButton *b_update;
  QPushButton *b_quit;
  KListView   *lb_list;
  QPushButton *b_remove;
  QPushButton *b_make_top;
  QListBox    *lb_status;
  QPushButton *b_auto;
  QPopupMenu  *popup_menu;

  QBoxLayout *top2bottom;

  QAccel      *accel;

  KProcess    *lpc;
  KProcess    *lpq;
  Spooler     *spooler;
  LpcCommand   lpc_com;
  bool         lpq_running;
  QString      lpq_in_buffer;

  ConfAutoUpdate *conf_auto;
  SpoolerConfig  *conf_spooler;
  int          update_delay;

  QStringList     remote_files;
};

#endif /* klpq_included */
