// -*- C++ -*-

//
//  klpq
//
//  Copyright (C) 1997 Christoph Neerfeld
//  email:  Christoph.Neerfeld@home.ivm.de or chris@kde.org
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//

#ifndef queueview_included
#define queueview_included

#include <klistview.h>

class MyLVI : public QListViewItem
{
public:
  MyLVI(QListView *parent, int _range, const QString& ow, int _job, 
	const QString& fi, int _filesize);
  virtual ~MyLVI() {}

  int jobId() { return job; }

  virtual QString key( int, bool ) const;
protected:
  int range;
  int job;
  int filesize;
};

#endif /* queueview_included */


