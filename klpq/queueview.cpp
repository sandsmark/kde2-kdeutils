// -*- C++ -*-

//
//  klpq
//
//  Copyright (C) 1997 Christoph Neerfeld
//  email:  Christoph.Neerfeld@home.imv.de or chris@kde.org
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//

#include <kglobal.h>
#include <klocale.h>

#include "queueview.h"


MyLVI::MyLVI(QListView *parent, int _range, const QString& owner,
	     int _job, const QString& filename, int _filesize)
  : QListViewItem(parent),
    range(_range), job(_job), filesize(_filesize)
{
  setText(0, KGlobal::locale()->formatNumber(range, 0));
  setText(1, owner);
  setText(2, KGlobal::locale()->formatNumber(job, 0));
  setText(3, filename);
  setText(4, KGlobal::locale()->formatNumber(filesize, 0));
}

QString MyLVI::key( int column, bool ) const
{
  if (column == 0)
    return QString().sprintf("%10d", range);
  else if (column == 2)
    return QString().sprintf("%10d", job);
  else if (column == 4)
    return QString().sprintf("%10d", filesize);
  else
    return text( column );
}
