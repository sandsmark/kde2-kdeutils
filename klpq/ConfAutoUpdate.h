// -*- C++ -*-

#ifndef ConfAutoUpdate_included
#define ConfAutoUpdate_included


#include <kdialogbase.h>
class QSlider;
class QLCDNumber;

class ConfAutoUpdate : public KDialogBase
{
  Q_OBJECT

  public:
    ConfAutoUpdate( QWidget *parent=0, const char *name=0, bool modal=true );
    virtual ~ConfAutoUpdate( void );

    void setFrequency( int sec );
    int  getFrequency( void );

  private:
    QSlider *mFrequencySlider;
    QLCDNumber *mFrequencyLCD;
};

#endif // ConfAutoUpdate_included




