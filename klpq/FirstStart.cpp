#include <qradiobutton.h>
#include <qlabel.h>
#include <qvbuttongroup.h>
#include <qvbox.h>

#include <klocale.h>

#include "FirstStart.h"
#include "FirstStart.moc"

FirstStart::FirstStart(QWidget* parent, const char* name)
 : KDialogBase( parent, name, TRUE, i18n("Select your spooling system"), Ok )
{
  QVBox *page = makeVBoxMainWidget();

  new QLabel( i18n("<qt>Please select the spooling system you use. Most "
		   "systems use the <b>BSD</b> spooler. So if you don't "
		   "know which spooler is installed on your system, BSD "
		   "is probably a good choice.</qt>"), page );

  QVButtonGroup * grp = new QVButtonGroup( i18n("Spooler Type"), page );

  rb_bsd = new QRadioButton( i18n("BSD"), grp );
  rb_bsd->setChecked( TRUE );
  rb_ppr = new QRadioButton( i18n("PPR"), grp );
  rb_lprng = new QRadioButton( i18n("LPRNG"), grp );
}


FirstStart::~FirstStart()
{
}

QString FirstStart::getSpooler() const
{
  if (rb_ppr->isChecked() )
    return QString::fromLatin1("PPR");
  else if (rb_lprng->isChecked() )
    return QString::fromLatin1("LPRNG");
  else
    return QString::fromLatin1("BSD");
}
