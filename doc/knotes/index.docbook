<?xml version="1.0" ?>
<!DOCTYPE book PUBLIC "-//KDE//DTD DocBook XML V4.1-Based Variant V1.0//EN" "dtd/kdex.dtd" [
  <!ENTITY kappname "&knotes;">
  <!ENTITY % addindex "IGNORE">
  <!ENTITY % English "INCLUDE" > <!-- change language only here -->
]>

<book lang="&language;">

<bookinfo>
<title>The &knotes; Handbook</title>

<authorgroup>
<author>
<firstname>Fabian</firstname>
<surname>Dal Santo</surname>
<affiliation>
<address><email>linuxgnu@yahoo.com.au</email></address>
</affiliation>
</author>

<author>
<firstname>Greg</firstname>
<othername>M.</othername>
<surname>Holmes</surname>
</author>

<othercredit role="reviewer">
<firstname>Lauri</firstname>
<surname>Watts</surname>
<contrib>Reviewer</contrib>
</othercredit>
<!-- TRANS:ROLES_OF_TRANSLATORS -->

</authorgroup>

<copyright>
<year>2000</year>
<holder>Greg M. Holmes</holder>
</copyright>
<copyright>
<year>2001</year>
<holder>Fabian Del Santo</holder>
</copyright>

<legalnotice>&FDLNotice;</legalnotice>

<date>2001-01-27</date>
<releaseinfo>1.01.00</releaseinfo>

<abstract><para>&knotes; is a sticky notes application for the
desktop.</para></abstract>

<keywordset>
<keyword>KDE</keyword>
<keyword>Knotes</keyword>
<keyword>kdeutils</keyword>
<keyword>notes</keyword>
<keyword>popup</keyword>
<keyword>pop-up</keyword>
<keyword>knotes</keyword>

</keywordset>

</bookinfo>

<chapter id="introduction">
<title>Introduction</title>

<para>&knotes; is a program that lets you write the computer equivalent
of sticky notes.  The notes are saved automatically when you exit the
program, and they display when you open the program. </para>

<para>You may print and mail your notes if you configure &knotes; to use helper
applications. </para>

<para>Display features of notes such as color and font may be customized for
each note.  You may also customize the defaults. </para>

</chapter>

<chapter id="configuration">
<title>Configuration</title>

<sect1 id="ConfiguringDisplay">
<title>Configuring the Display in &knotes;</title>

<para>To configure the display:</para>

<procedure>
<step><para><mousebutton>Right</mousebutton> click on the panel icon.</para></step>
<step><para>Select <guimenuitem>Preferences...</guimenuitem></para>
<para>The &knotes; <guilabel>Defaults - KNotes</guilabel> dialog will
open.</para> </step>
<step><para>Choose the <guiicon>Display</guiicon> icon from the three icons at
the left.</para></step>
</procedure>

<para>The following choices are available:</para>

<variablelist>
<varlistentry>
<term><guilabel>Text Color</guilabel></term>
<listitem><para>The color square shows the current text color.  By clicking this
color square you open the standard &kde; color selection
dialog.</para></listitem> 
</varlistentry>
<varlistentry>
<term><guilabel>Background Color</guilabel></term>
<listitem><para>The color square shows the current background color.  By
clicking this color square you open the standard &kde; color selection
dialog.</para></listitem>
</varlistentry>

<varlistentry>
<term><guilabel>Note Width</guilabel></term>
<listitem><para>The width of the note in pixels.  Edit this number as
desired.</para></listitem> 
</varlistentry>

<varlistentry>
<term><guilabel>Note Height</guilabel></term>
<listitem><para>The height of the note in pixels.  Edit this number as
desired.</para></listitem>
</varlistentry>
</variablelist>

</sect1>

<sect1 id="configuring-editor">
<title>Configuring the Editor in &knotes;</title>
<para>To customize the editor:</para>
<procedure>
<step><para><mousebutton>Right</mousebutton> click on the panel
icon.</para></step>
<step><para>Select <guimenuitem>Preferences...</guimenuitem></para>
<para>The &knotes; <guilabel>Defaults - KNotes</guilabel> dialog will
open.</para></step> 
<step><para>Choose the <guiicon>Editor</guiicon> icon from the three
icons at the left.</para></step>
</procedure>

<para>The following choices are available:</para>

<variablelist>
<varlistentry>
<term><guilabel>Tab Size</guilabel></term>
<listitem><para>This is the size of the tab in spaces.  Edit this number as
desired.</para></listitem>
</varlistentry>
<varlistentry>
<term><guilabel>Auto Indent</guilabel></term>
<listitem><para>This is a check box.  If selected, auto-indenting is
on.</para></listitem>
</varlistentry>
<varlistentry>
<term><guilabel>Current Font: Click to Change</guilabel></term>
<listitem><para>Click this button to open the standard &kde; font selection
dialog.</para></listitem>
</varlistentry>
</variablelist>

</sect1>

<sect1 id="configuring-actions">
<title>Configuring Actions in &knotes;</title>

<para>To configure actions in &knotes;:</para>
<procedure><step><para>Right-click on the &knotes; panel
icon.</para></step><step>
<para>Select <guimenuitem>Preferences...</guimenuitem></para>
<para>The &knotes; <guilabel>Defaults - KNotes</guilabel> dialog will
open.</para></step>
<step><para>Choose the <guiicon>Actions</guiicon> icon from the three icons at the
left.</para></step>
</procedure>

<para>The following choices are available:</para>

<variablelist>
<varlistentry>
<term><guilabel>Mail Action</guilabel></term>
<listitem><para>Type a mail command and any required command line switches in
this box.</para>
<para>By using <token>%f</token> in the command line you can pass
the filename of the note body to the mail command.</para>
</listitem>
</varlistentry>

<varlistentry>
<term><guilabel>Print</guilabel></term>
<listitem><para>Type a printing command and any required command line switches
in this box.</para>
<para>Use the following variables in the command line:</para>
<para><token>%p</token> is the printer name.</para>
<para><token>%t</token> is the title.</para>
<para><token>%f</token> is the filename of the note body.</para>
</listitem>
</varlistentry>
</variablelist>

</sect1>

</chapter>

<chapter id="using-knotes">
<title>Using &knotes;</title>

<sect1 id="create-new-note">
<title>Creating a New Note</title>

<para>To create a new note:</para>
<procedure>
<step><para><mousebutton>Right</mousebutton> click on the &knotes; panel
icon.</para></step>
<step><para>Select New Note.</para></step>
</procedure>

</sect1>

<sect1 id="typing-the-note">
<title>Writing Your Note</title>

<para>To write your note, simply type the note in the space provided.  Normal
keyboard and mouse editing functions are supported.</para> 

<para><mousebutton>Right</mousebutton> clicking in the editing space provides the
following menu options:</para>

<itemizedlist>
<listitem><para><guimenuitem>Undo</guimenuitem></para></listitem>
<listitem><para><guimenuitem>Redo</guimenuitem></para></listitem>
<listitem><para><guimenuitem>Cut</guimenuitem></para></listitem>
<listitem><para><guimenuitem>Copy</guimenuitem></para></listitem>
<listitem><para><guimenuitem>Paste</guimenuitem></para></listitem>
<listitem><para><guimenuitem>Paste special...</guimenuitem></para></listitem>
<listitem><para><guimenuitem>Clear</guimenuitem></para></listitem>
<listitem><para><guimenuitem>Select All</guimenuitem></para></listitem>
</itemizedlist>

<para>Text may be selected by holding down the <mousebutton>left</mousebutton>
mouse button and moving the mouse, or by holding down the <keycap>Shift</keycap>
key and using the <keycap>arrow</keycap> keys.</para>

</sect1>

<sect1 id="inserting-the-date">
<title>Inserting the Date</title>

<para>To insert the current date in the Note:</para>
<procedure>
<step><para><mousebutton>Right</mousebutton> click on the title bar of the note.</para></step> 
<step><para>Select <guimenuitem>Insert Date</guimenuitem>.</para></step>
</procedure>

<para>The current date and time will be inserted at the cursor position in the
text of the note.</para>

</sect1>

<sect1 id="renaming-a-note">
<title>Renaming a Note</title>

<para>To rename a note:</para>
<procedure>
<step><para><mousebutton>Right</mousebutton> click on the note title
bar.</para></step>
<step><para>Select <guimenuitem>Rename</guimenuitem>.</para></step>
</procedure>

<para>Type the new name of the note in the dialog that appears.  To accept the
new name, press the <guibutton>OK</guibutton> button.  To exit the dialog
without renaming the note, press the <guibutton>Cancel</guibutton> button.  To
clear what you have typed and start over, click the <guibutton>Clear</guibutton>
button.</para>

</sect1>
<sect1 id="mailing-a-note">
<title>Mailing a Note</title>

<para>To mail a note:</para>
<procedure>
<step><para><mousebutton>Right</mousebutton> click on the note title
bar.</para></step>
<step><para>Select <guimenuitem>Mail</guimenuitem>.</para></step>
</procedure>

<para>What happens next depends on how you configured the Mail action in the
<guilabel>Preferences...</guilabel> dialog.</para>

</sect1>

<sect1 id="printing-a-note">
<title>Printing a Note</title>

<para>To print a note:</para>

<procedure>
<step><para><mousebutton>Right</mousebutton> click on the note title
bar.</para></step>
<step><para>Select <guimenuitem>Print</guimenuitem>.</para></step>
</procedure>

<para>What happens next depends on how you configured the Print action in the
<guilabel>Preferences...</guilabel> dialog.</para>

</sect1>


<sect1 id="deleting-a-note">
<title>Deleting a Note</title>
<para>To delete a note:</para>
<procedure>
<step><para><mousebutton>Right</mousebutton> click on the note title
bar.</para></step>
<step><para>Select <guimenuitem>Delete Note</guimenuitem>.</para></step>
</procedure>
</sect1>

<sect1 id="hiding-a-note">
<title>Hiding a Note</title>

<para>To hide a note, click the <guiicon>X</guiicon> in the upper right corner
of the title bar of the note.  The note will no longer be displayed on the
screen.  The note itself will not be deleted.</para>

</sect1>

<sect1 id="displaying-notes">
<title>Displaying Notes</title>

<para>When you start &knotes;, all notes will display on the screen.  If you
hide a note and later want to display it:</para>

<procedure>
<step><para>Right-click on the &knotes; panel icon.</para></step>
<step><para>Select <guimenuitem>Notes</guimenuitem>.</para></step>
<step><para>Choose the note you want to display.</para></step>
</procedure>

</sect1>

<sect1 id="desktop-functions">
<title>Desktop Functions</title>

<para>To send a note to a specific desktop:</para>

<procedure>
<step><para><mousebutton>Right</mousebutton> click on the title bar of the
note.</para></step>
<step><para>Select <guimenuitem>To Desktop</guimenuitem></para></step>
<step><para>Choose the desktop desired, or alternatively,
<guimenuitem>All Desktops</guimenuitem></para></step> </procedure>

<para>To make the note remain on top of other windows:</para>
<procedure>
<step><para><mousebutton>Right</mousebutton> click on the title bar of the
note.</para></step>
<step><para>Select <guimenuitem>Always on Top</guimenuitem>.</para></step>
</procedure>

<para>To return the note to more normal window behavior, simply repeat this
process.</para> 

</sect1>

<sect1 id="customizing-display">
<title>Customizing a Note Display</title>

<para>To customize a note display:</para>

<procedure>
<step><para><guimenuitem>Right</guimenuitem> click on the note
title bar.</para></step>
<step><para>Select <guimenuitem>Note preferences...</guimenuitem>.</para>
<para>The <guilabel>Local Settings - Knotes</guilabel> dialog will
open.</para></step>
<step><para>Choose the <guiicon>Display</guiicon> icon from the three icons at
the left.</para></step>
</procedure>

<para>The following choices are available:</para>

<variablelist>
<varlistentry>
<term><guilabel>Text Color</guilabel></term>
<listitem><para>The color square shows the current text color.  By
clicking this color square you open the standard &kde; color selection
dialog.</para></listitem>
</varlistentry>
<varlistentry>
<term><guilabel>Background Color</guilabel></term>
<listitem><para>The color square shows the current text color.</para></listitem>
</varlistentry>
<varlistentry>
<term><guilabel>Note Width</guilabel></term>
<listitem><para>The width of the note in pixels.  Edit this number as
desired.</para></listitem>
</varlistentry>
<varlistentry>
<term><guilabel>Note Height</guilabel></term>
<listitem><para>The height of the note in pixels.  Edit this number as
desired.</para></listitem>
</varlistentry>
</variablelist>

</sect1>

<sect1 id="customizing-editor">
<title>Customizing a Note Editor</title>

<para>To customize a note editor:</para>
<procedure>
<step><para><mousebutton>Right</mousebutton> click on the title bar of the
note.</para></step>
<step><para>Select <guimenuitem>Note preferences...</guimenuitem></para>
<para>The <guilabel>Local Settings - Knotes</guilabel> dialog will
open.</para></step>
<step><para>Choose the <guiicon>Editor</guiicon> icon from the three icons at
the left.</para></step>
</procedure>

<para>The following choices are available:</para>

<variablelist>
<varlistentry>
<term><guilabel>Tab Size</guilabel></term>
<listitem><para>This is the size of the tab in spaces.  Edit this number as
desired.</para></listitem>
</varlistentry>
<varlistentry>
<term><guilabel>Auto Indent</guilabel></term>
<listitem><para>This is a check box.  If selected, auto-indenting is
on.</para></listitem>
</varlistentry>
<varlistentry>
<term><guilabel>Current Font: Click to Change</guilabel></term>
<listitem><para>Click this button to open the standard &kde; font selection
dialog.</para></listitem>
</varlistentry>
</variablelist>

</sect1>

<sect1 id="customizing-actions">
<title>Customizing a Note Actions</title>
<para>To customize a note actions:</para>

<procedure>
<step><para><mousebutton>Right</mousebutton> click on the title bar of the
note.</para></step>
<step><para>Select <guimenuitem>Note preferences...</guimenuitem></para>
<para>The <guilabel>Local Settings - Knotes</guilabel> dialog will
open.</para></step>
<step><para>Choose the <guiicon>Action</guiicon> icon from the three icons at
the left.</para></step>
</procedure>

<para>The following choices are available</para>

<variablelist>
<varlistentry>
<term><guilabel>Mail Action</guilabel></term>
<listitem><para>Type a mail command and any required command line switches in
this box.</para> <para>By using <token>%f</token> in the command
line you can pass the filename of the note body to the mail
command.</para>
</listitem>
</varlistentry>
<varlistentry>
<term><guilabel>Print Action</guilabel></term>
<listitem><para>Type a printing command and any required command line switches
in this box.</para>
<para>Use the following variables in the command line:</para>
<para><token>%p</token> is the printer name.</para>
<para><token>%t</token> is the title.</para>
<para><token>%f</token> is the filename of the note body.</para>
</listitem>
</varlistentry>
</variablelist>

</sect1>

<sect1 id="quit-knotes">
<title>Quitting &knotes;</title>
<para>To quit &knotes;:</para>
<procedure>
<step><para><mousebutton>Right</mousebutton> click on the panel
icon.</para></step>
<step><para>Select <guimenuitem>Quit</guimenuitem>.</para></step>
</procedure>
</sect1>

</chapter>

<chapter id="credits">

<title>Credits and License</title>

<para>&knotes;</para>

<para>Program copyright 1997 Bernd Wuebben <email>wuebben@kde.org</email></para>

<para>Contributors:</para>
<itemizedlist>
<listitem><para>Wynn Wilkes<email>wynnw@calderasystems.com</email></para>
</listitem>
</itemizedlist>

<para>Documentation copyright 2000 Greg M. Holmes
<email>holmegm@earthlink.net</email></para>

<para>Documentation updated 2001 by Fabian Del Santo
<email>linuxgnu@yahoo.com.au</email>.</para>

<!-- TRANS:CREDIT_FOR_TRANSLATORS -->

&underFDL;
&underGPL;

</chapter>

<appendix id="installation">
<title>Installation</title>

<sect1 id="getting-knotes">
<title>How to obtain &knotes;</title>

<para>&knotes; is part of the &kde; project <ulink
 url="http://www.kde.org">http://www.kde.org</ulink>.  &knotes; can be found in
 the kdeutils package on <ulink
 url="ftp://ftp.kde.org/pub/kde/">ftp://ftp.kde.org/pub/kde/</ulink>, the main
 ftp site of the &kde; project. </para>

</sect1>

<sect1 id="requirements">
<title>Requirements</title>

<para>In order to print or mail notes, you need to have helper
applications installed, such as <command>a2ps</command> and &kmail;.
You will need to configure &knotes; to use these helper applications.
</para>

</sect1>

<sect1 id="compilation">
<title>Compilation and Installation</title>

<para>In order to compile and install &knotes; on your system, type the
following in the base directory of the kdeutils distribution:</para>

<screen width="40">
<prompt>%</prompt><userinput><command>./configure</command></userinput>
<prompt>%</prompt><userinput><command>make</command></userinput>
<prompt>%</prompt><userinput><command>make</command> <option>install</option></userinput>
</screen>

<para>Since &knotes; uses <command>autoconf</command> and
<command>automake</command> you should have no trouble compiling
it. Should you run into problems please report them to the &kde; mailing
lists.</para>

</sect1>
</appendix>

&documentation.index; 
</book>
<!--
Local Variables:
mode: sgml
sgml-minimize-attributes:nil
sgml-general-insert-case:lower
sgml-indent-step:0
sgml-indent-data:nil
End:

// vim:ts=0:sw=2:tw=78:noet
-->


