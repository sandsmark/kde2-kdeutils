
/*

 $Id: mail.h 137205 2002-02-18 16:11:55Z mlaurent $

 Copyright (C) Bernd Johannes Wuebben
               wuebben@math.cornell.edu
	       wuebben@kde.org

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 2
 of the License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

 */



#ifndef _MAIL_DLG_H_
#define _MAIL_DLG_H_

#include <qlineedit.h>
#include <kdialogbase.h>

class QLabel;
class TopLevel;

class Mail : public KDialogBase
{
  Q_OBJECT

  public:
    Mail( TopLevel *parent = 0, const char *name=0, bool modal=true );

    QString getRecipient( void ) {return recipient->text();};
    QString getSubject( void ) {return subject->text();};

  public:
    QLineEdit 	*recipient;
    QLineEdit 	*subject;
    QLabel 	*subjectlabel;

  protected:
    void focusInEvent( QFocusEvent *);

  public slots:
    virtual void slotUser1( void );
  void recipientTextChanged(const QString &text );
};

#endif
