/*
 *   kedit - Small KDE editor
 *   This file only: Copyright (C) 1999  Espen Sand, espen@kde.org
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

#ifndef _OPTION_STATE_H_
#define _OPTION_STATE_H_

#include <kapp.h>
#include <kglobalsettings.h>
#include <qfont.h> 
#include <kspell.h>

struct SFontState
{
  SFontState( void )
  {
    font = KGlobalSettings::fixedFont();
  }

  QFont font;
};

struct SColorState
{
  SColorState( void )
  {
    custom = false;
    textFg = KGlobalSettings::textColor();
    textBg = KGlobalSettings::baseColor();
  }

  bool custom;
  QColor textFg;
  QColor textBg;
};

struct SSpellState
{
  SSpellState( void )
  {
    config = KSpellConfig();
  }

  KSpellConfig config;
};

struct SMiscState
{
  enum WrapModes
  {
    noWrap = 0,
    dynamicWrap = 1,
    fixedColumnWrap = 2
  }; 

  SMiscState( void )
  {
    wrapMode    = noWrap;
    wrapColumn  = 79;
    backupCheck = true;
    mailCommand = "mail -s \"%s\" \"%s\"";
  }

  int  wrapMode;
  int  wrapColumn;
  bool backupCheck;
  QString mailCommand;
};


struct SOptionState
{
  SFontState  font;
  SColorState color;
  SSpellState spell;
  SMiscState  misc;
};






#endif
