/*
 *   kedit - Small KDE editor
 *   This file only: Copyright (C) 1999  Espen Sand, espen@kde.org
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

#ifndef _OPTION_DIALOG_H_
#define _OPTION_DIALOG_H_

class QCheckBox;
class QLabel;
class QLineEdit;
class KColorButton;
class KFontChooser;
class KSpellConfig;

#include <kdialogbase.h>

#include "optionstate.h"


class COptionDialog : public KDialogBase
{
  Q_OBJECT

  public:
    enum Page
    {
      page_font = 0,
      page_color,
      page_spell,
      page_misc,
      page_max
    };

    COptionDialog( QWidget *parent = 0, char *name = 0, bool modal = false );
    ~COptionDialog( void );

    void setFont( const SFontState &font );
    void setColor( const SColorState &color );
    void setSpell( const SSpellState &spell );
    void setMisc( const SMiscState &misc );
    void setState( const SOptionState &state );

  protected slots:
    virtual void slotDefault( void );
    virtual void slotOk( void );
    virtual void slotApply( void );
    virtual void slotHelp( void );
    void slotCustomColorSelectionChanged( void );

  private:
    struct SFontWidgets
    {
      KFontChooser *chooser;
    };

    struct SColorWidgets
    {
      QCheckBox *custom;
      KColorButton *fgColor;
      KColorButton *bgColor;
    };

    struct SSpellWidgets
    {
      KSpellConfig *config;
    };

    struct SMiscWidgets
    {
      QComboBox *wrapCombo;
      QLabel    *wrapLabel;
      QLineEdit *wrapInput;
      QCheckBox *backupCheck;
      QLineEdit *mailInput;
    };

  private slots:
    void wrapMode( int mode );

  private:
    void setupFontPage( void );
    void setupColorPage( void );
    void setupSpellPage( void );
    void setupMiscPage( void );

  signals:
    void fontChoice( const SFontState &font );
    void colorChoice( const SColorState &color );
    void spellChoice( const SSpellState &spell );
    void miscChoice( const SMiscState &misc );
    void save();

  private:
    SOptionState   mState;
    SColorWidgets  mColor;
    SFontWidgets   mFont;
    SSpellWidgets  mSpell;
    SMiscWidgets   mMisc;
};


#endif







