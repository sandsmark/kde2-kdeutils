
/*

 $Id: mail.cpp 137205 2002-02-18 16:11:55Z mlaurent $

 Copyright (C) Bernd Johannes Wuebben
               wuebben@math.cornell.edu
	       wuebben@kde.org

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 2
 of the License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

 */


#include <qframe.h>
#include <qlabel.h>
#include <qlayout.h>

#include <klocale.h>
#include <kmessagebox.h>

#include "kedit.h"
#include "mail.h"


Mail::Mail( TopLevel *parent, const char *name,  bool modal)
  : KDialogBase( parent, name, modal, i18n("Mail Document"), User1|Cancel, 
		 User1, true, i18n("&Mail") ) 
{
  QWidget *page = new QWidget( this ); 
  setMainWidget(page);
  QVBoxLayout *topLayout = new QVBoxLayout( page, 0, spacingHint() );
  
  QString text;

  text = i18n("Mail Document to:");
  QLabel *label = new QLabel( text, page, "mailto" );
  topLayout->addWidget( label );
  
  recipient = new QLineEdit( page, "mailtoedit");
  connect(recipient,SIGNAL(textChanged ( const QString & )),this,SLOT(recipientTextChanged(const QString & )));
  recipient->setFocus();
  recipient->setMinimumWidth( fontMetrics().maxWidth()*20 );
  topLayout->addWidget( recipient );
  
  text = i18n("Subject:");
  label = new QLabel( text, page, "subject" );
  topLayout->addWidget( label );

  subject = new QLineEdit( page, "subjectedit");
  subject->setMinimumWidth( fontMetrics().maxWidth()*20 );
  topLayout->addWidget( subject );
  
  //
  // Default subject string
  //
  QString subjectstr = parent->name();    
  int index = subjectstr.findRev('/');
  if( index != -1)
  {
    subjectstr = subjectstr.right(subjectstr.length() - index - 1 );
  }
  subject->setText(subjectstr);

  topLayout->addStretch(10);
  enableButton(KDialogBase::User1,!recipient->text().isEmpty());
}

void Mail::recipientTextChanged(const QString &text )
{
  enableButton(KDialogBase::User1,!text.isEmpty());
}

void Mail::focusInEvent( QFocusEvent *)
{
  recipient->setFocus();
}


void Mail::slotUser1( void )
{
  QString str = getRecipient();
  if( str.isEmpty() == true )
  {
    QString msg = i18n("You must specify a recipient");
    KMessageBox::sorry( this, msg );
    return;
  }
  accept();
}

#include "mail.moc"


