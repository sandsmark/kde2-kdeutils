/*
 *   kedit - Small KDE editor
 *   This file only: Copyright (C) 1999  Espen Sand, espen@kde.org
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

#include <qcheckbox.h>
#include <qcombobox.h>
#include <qfont.h>
#include <qframe.h>
#include <qlayout.h>
#include <qlabel.h>
#include <qlineedit.h> 
#include <qvbox.h>
#include <knumvalidator.h>

#include <kapp.h>
#include <kcolorbtn.h>
#include <kfontdialog.h> // For KFontChooser
#include <kiconloader.h>
#include <klocale.h>
#include <kspell.h>

#include "optiondialog.h"
#include "optiondialog.moc"



COptionDialog::COptionDialog( QWidget *parent, char *name, bool modal )
  :KDialogBase( IconList, i18n("Configure"), Help|Default|Apply|Ok|Cancel,
		Ok, parent, name, modal, true )
{
  setHelp( "kedit/index.html", QString::null );

  setupFontPage();
  setupColorPage();
  setupSpellPage();
  setupMiscPage();
}

COptionDialog::~COptionDialog( void )
{
}


void COptionDialog::setupFontPage( void )
{
  //
  // 2000-02-19 Espen Sand.
  // It is ok to use a QVBox page here because the font dialog will use
  // all vertical space correctly.
  //
  QVBox *page = addVBoxPage( i18n("Font"), i18n("Editor font" ), 
			     BarIcon("fonts", KIcon::SizeMedium ) );
  mFont.chooser = new KFontChooser( page,"font",false,QStringList(),false,6 );
  mFont.chooser->setSampleText( i18n("KEdit editor font") );
}


void COptionDialog::setupColorPage( void )
{
  QFrame *page = addPage( i18n("Color"), i18n("Text color in editor area"),
			  BarIcon("colorize", KIcon::SizeMedium ) );
  QVBoxLayout *topLayout = new QVBoxLayout( page, 0, spacingHint() );

  QGridLayout *gbox = new QGridLayout( 3, 2 );
  topLayout->addLayout(gbox);

  mColor.custom = new QCheckBox( i18n("Use custom colors"), page );
  connect( mColor.custom, SIGNAL(clicked() ),
           this, SLOT(slotCustomColorSelectionChanged()) );
  mColor.fgColor = new KColorButton( page );
  mColor.bgColor = new KColorButton( page );
  QLabel *label1 = new QLabel( mColor.fgColor, i18n("Foreground color:"), page );
  QLabel *label2 = new QLabel( mColor.bgColor, i18n("Background color:"), page );

  gbox->addMultiCellWidget( mColor.custom, 0, 0, 0, 1 );
  gbox->addWidget( label1, 1, 0 );
  gbox->addWidget( label2, 2, 0 );
  gbox->addWidget( mColor.fgColor, 1, 1 );
  gbox->addWidget( mColor.bgColor, 2, 1 );

  topLayout->addStretch(10);
}


void COptionDialog::setupSpellPage( void )
{
  QFrame *page = addPage( i18n("Spelling"), i18n("Spell checker behavior"),
			  BarIcon("spellcheck", KIcon::SizeMedium ) );
  //SmallIcon("spellcheck") );
  QVBoxLayout *topLayout = new QVBoxLayout( page, 0, spacingHint() );

  mSpell.config = new KSpellConfig( page, "spell", 0, false );
  topLayout->addWidget( mSpell.config );

  topLayout->addStretch(10);
}


void COptionDialog::setupMiscPage( void )
{
  QFrame *page = addPage( i18n("Miscellaneous"), i18n("Various properties"),
			  BarIcon("misc", KIcon::SizeMedium ) );
  QVBoxLayout *topLayout = new QVBoxLayout( page, 0, spacingHint() );

  QGridLayout *gbox = new QGridLayout( 5, 2 );
  topLayout->addLayout( gbox );

  QString text;

  text = i18n("Word Wrap:");
  QLabel *label = new QLabel( text, page, "wraplabel" );
  gbox->addWidget( label, 0, 0 );
  QStringList wrapList;
  wrapList.append( i18n("Disable wrapping") );
  wrapList.append( i18n("Let editor width decide") );
  wrapList.append( i18n("At specified column") );
  mMisc.wrapCombo = new QComboBox( false, page );
  connect(mMisc.wrapCombo,SIGNAL(activated(int)),this,SLOT(wrapMode(int)));
  mMisc.wrapCombo->insertStringList( wrapList );
  gbox->addWidget( mMisc.wrapCombo, 0, 1 );

  text = i18n("Wrap Column:");
  mMisc.wrapLabel = new QLabel( text, page, "wrapcolumn" );
  gbox->addWidget( mMisc.wrapLabel, 1, 0 );
  mMisc.wrapInput = new QLineEdit( page, "values" );
  mMisc.wrapInput->setValidator( new KIntValidator( 0,9999,mMisc.wrapInput ) );
  mMisc.wrapInput->setMinimumWidth( fontMetrics().maxWidth()*10 );
  gbox->addWidget( mMisc.wrapInput, 1, 1 );

  gbox->addRowSpacing( 2, spacingHint()*2 );  

  text = i18n("Make backup when saving a file");
  mMisc.backupCheck = new QCheckBox( text, page, "backup" );
  gbox->addMultiCellWidget( mMisc.backupCheck, 3, 3, 0, 1 );
  
  mMisc.mailInput = new QLineEdit( page, "mailcmd" );
  mMisc.mailInput->setMinimumWidth(fontMetrics().maxWidth()*10);
  text = i18n("Mail Command:");
  label = new QLabel( text, page,"mailcmdlabel" );
  gbox->addWidget( label, 4, 0 );
  gbox->addWidget( mMisc.mailInput, 4, 1 );

  topLayout->addStretch(10);
}


void COptionDialog::wrapMode( int mode )
{
  bool state = mode == 2 ? true : false;
  mMisc.wrapInput->setEnabled( state );
  mMisc.wrapLabel->setEnabled( state );
}


void COptionDialog::slotOk( void )
{
  slotApply();
  accept();
}


void COptionDialog::slotApply( void )
{
      mState.font.font = mFont.chooser->font();
      emit fontChoice( mState.font );

      mState.color.custom = mColor.custom->isChecked();
      mState.color.textFg = mColor.fgColor->color();
      mState.color.textBg = mColor.bgColor->color();
      emit colorChoice( mState.color );

      mState.spell.config = *mSpell.config;
      mState.spell.config.writeGlobalSettings();
      emit spellChoice( mState.spell );

      mState.misc.wrapMode    = mMisc.wrapCombo->currentItem();
      mState.misc.backupCheck = mMisc.backupCheck->isChecked();
      mState.misc.wrapColumn  = mMisc.wrapInput->text().toInt();
      mState.misc.mailCommand = mMisc.mailInput->text();
      emit miscChoice( mState.misc );
      emit save();
}


void COptionDialog::slotDefault( void )
{
  //
  // The constructors store the default settings.
  //
  switch( activePageIndex() )
  {
    case page_font:
      setFont( SFontState() );
    break;

    case page_color:
      setColor( SColorState() );
    break;

    case page_spell:
      setSpell( SSpellState() );
    break;

    case page_misc:
      setMisc( SMiscState() );
    break;
  }
}


void COptionDialog::slotHelp( void )
{
  if( activePageIndex() == page_spell )
  {
    mSpell.config->activateHelp();
  }
  else
  {
    KDialogBase::slotHelp();
  }
}

void COptionDialog::slotCustomColorSelectionChanged()
{
  bool custom = mColor.custom->isChecked();
  mColor.fgColor->setEnabled(custom);
  mColor.bgColor->setEnabled(custom);
}

void COptionDialog::setFont( const SFontState &font )
{
  mState.font = font;
  mFont.chooser->setFont( font.font, false );
}


void COptionDialog::setColor( const SColorState &color )
{
  mState.color = color;
  mColor.custom->setChecked(color.custom);
  mColor.fgColor->setEnabled(color.custom);
  mColor.bgColor->setEnabled(color.custom);
  mColor.fgColor->setColor( color.textFg );
  mColor.bgColor->setColor( color.textBg );
}


void COptionDialog::setSpell( const SSpellState &spell )
{
  *mSpell.config = spell.config;
}


void COptionDialog::setMisc( const SMiscState &misc )
{
  mState.misc = misc;
  mMisc.wrapCombo->setCurrentItem( misc.wrapMode ); 
  mMisc.wrapInput->setText( QString().setNum(misc.wrapColumn) );
  mMisc.backupCheck->setChecked( misc.backupCheck );
  mMisc.mailInput->setText( misc.mailCommand );
  wrapMode( mMisc.wrapCombo->currentItem() );
}


void COptionDialog::setState( const SOptionState &state )
{
  setFont( state.font );
  setColor( state.color );
  setSpell( state.spell );
  setMisc( state.misc );
}
