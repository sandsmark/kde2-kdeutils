/*
    Requires the Qt widget libraries, available at no cost at 
    http://www.troll.no
       
    Copyright (C) 1996 Bernd Johannes Wuebben   
                       wuebben@math.cornell.edu

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
  
    $Log$
    Revision 1.14  1999/08/22 14:32:52  espensa
    Once again: Fix in kedit.cpp so that the "Recent" list works.
    Cleaned up various dialogs (better layout - Qt2), Accelerators
    for action buttons etc. (Normal lables have no acelleratos yet)
    (Old code is still present, but ifdef'ed away)

    Revision 1.13  1999/03/15 15:17:41  dfaure
    Merged with 1.1 branch. (layouts for most dialogs).
    Just learned that there is a keditcl widget in libkdeui. I'm not going to
    update it as well !
    Bernd says :
     * NOTE THIS CLASS IS NOT THE SAME AS THE ONE IN THE KDEUI LIB
     * The difference is that this one uses KFileDialog instead of
     * QFileDialog. So don't remove this class with the idea in mind to
     * link against kdeui.
    But I say :
     * What should be done would be to inherit from the full widget in libkdeui
     * and reimplement its method that uses QFileDialog so that it uses KFileDialog
     * here. Any volunteer ?

    Revision 1.12  1999/03/02 15:56:50  kulow
    CVS_SILENT replacing klocale->translate with i18n

    Revision 1.11  1999/02/17 17:12:42  bieker
    i18n() -> i18n()

    Revision 1.10  1998/09/14 20:45:05  kulow
    I know, Ok is ok too, but OK is more OK some GUI guides say :)

    Revision 1.9  1997/10/24 19:25:19  wuebben
    Lot's of fixes and improvements. Kedit was pretty screwed up.

    Revision 1.8  1997/10/24 05:52:43  wuebben
    Bernd: Some major fix ups. Hopefully I didn't break too much. Qt 1.31
    is nice, but it also uglified kedit, so I had to fix that.

    Revision 1.7  1997/08/31 21:19:58  kdecvs
    Kalle: Even more changes for the new KLocale and the new KConfig


*/

#include <qbuttongroup.h>
#include <qlayout.h>
#include <qlineedit.h>
#include <qradiobutton.h>

#include <klocale.h>
#include <kapp.h>

#include "print.h"
#include "print.moc"

PrintDialog::PrintDialog( QWidget *parent, const char *name, bool modal )
  :KDialogBase( parent, name, modal, i18n("Print Document"), Ok|Cancel,
		Ok, false )
{
  QWidget *page = new QWidget( this );
  setMainWidget( page );  
  QString text;

  QVBoxLayout *topLayout = new QVBoxLayout( page, 0, spacingHint() );

  QButtonGroup *bg = new QButtonGroup( page, "buttongroup" );
  topLayout->addWidget(bg);

  QVBoxLayout *vbox = new QVBoxLayout( bg, marginHint()*2 );
  text = i18n("Print directly using lpr");
  rawbutton = new QRadioButton( text, bg, "rawbutton" );
  rawbutton->setChecked( true );
  vbox->addWidget( rawbutton );

  QHBoxLayout *hbox = new QHBoxLayout();
  vbox->addLayout(hbox);
  text = i18n("Print using Command:");
  commandbutton = new QRadioButton( text, bg, "commandbutton" );
  hbox->addWidget(commandbutton);

  commandbox = new QLineEdit( bg, "command" );
  commandbox->setMinimumWidth(fontMetrics().maxWidth()*15);
  hbox->addWidget(commandbox);

  bg = new QButtonGroup( page, "buttongroup" );
  topLayout->addWidget( bg );
  vbox = new QVBoxLayout( bg, marginHint()*2 );
  
  text = i18n("Entire Document");
  allbutton = new QRadioButton(text, bg, "documentbutton");
  vbox->addWidget( allbutton );
  allbutton->setChecked( true );

  text = i18n("Selection");
  selectionbutton = new QRadioButton(text,bg,"selectionbutton");
  vbox->addWidget( selectionbutton );

  vbox->addStretch( 10 );
}



printinfo PrintDialog::getCommand( void ) const
{
  printinfo pi;

  pi.command   = commandbox->text();
  pi.raw       = rawbutton->isChecked();
  pi.selection = selectionbutton->isChecked();

  return(pi);
}

void PrintDialog::setWidgets( const printinfo &pi )
{
  if( pi.raw == 1 )
  {
    rawbutton->setChecked( true );
    commandbutton->setChecked( false );
  }
  else
  {
    rawbutton->setChecked( false );
    commandbutton->setChecked( true );
  }  
  commandbox->setText(pi.command);
}

