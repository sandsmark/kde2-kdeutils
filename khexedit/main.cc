/*
 *   khexedit - Versatile hex editor
 *   Copyright (C) 1999-2000 Espen Sand, espensa@online.no
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */



#include <iostream.h>
#include <unistd.h>
#include <stdlib.h>

#include <klocale.h>
#include <kcmdlineargs.h>
#include <kaboutdata.h>

#include "toplevel.h"
#include "version.h" // Contains khexedit name string and version string


static const char *description = 
	I18N_NOOP("KDE Hex editor");

static const char *version = APP_VERSION_STRING;

static KCmdLineOptions option[] =
{
   { "offset <offset>", I18N_NOOP("Jump to 'offset'"), 0 },
   { "+[file(s)]", I18N_NOOP("File(s) to open"), 0 },
   { 0, 0, 0 }
};

static uint parseDecimalOrHexadecimal( char *buf );

int main( int argc, char **argv )
{
  KAboutData aboutData("khexedit", I18N_NOOP("KHexEdit"),
    version, description, KAboutData::License_GPL,
    "(c) 1999-2000, Espen Sand",
    "This is a work in progress. If you\n\n"
    "1. have a suggestion for improvement\n"
    "2. have found a bug\n"
    "3. want to contribute with something\n"
    "4. just want to tell me how nice or useful khexedit is\n\n"
    "then feel free to send me a mail." );
  aboutData.addAuthor("Espen Sand",0, "espensa@online.no", 
		      "http://home.sol.no/~espensa/khexedit/" );
  KCmdLineArgs::init( argc, argv, &aboutData );
  KCmdLineArgs::addCmdLineOptions( option );

  KApplication app;

  if( app.isRestored() != 0 )
  {
    RESTORE( KHexEdit );
  }
  else
  {
    KHexEdit *hexEdit = new KHexEdit;
    if( hexEdit == 0 )
    {
      cerr << "Unable to start - Memory exhausted" << endl;
      return( 1 );
    }

    hexEdit->show();

    KCmdLineArgs *args = KCmdLineArgs::parsedArgs();

    if (args->isSet("offset"))
    {
        QCString offsetStr = args->getOption("offset");
        uint _offset = parseDecimalOrHexadecimal(offsetStr.data() );
        hexEdit->setStartupOffset( _offset );
    }
    
    for(int i = 0; i < args->count(); i++)
    {
       hexEdit->addStartupFile( QFile::decodeName(args->arg(i)) );
    }

    args->clear();
  }

  int result = app.exec();
  return( result );
}



static uint parseDecimalOrHexadecimal( char *buf )
{
  if( buf == 0 ) { return( 0 ); }
  
  long int value;
  char *end = 0;
	    
  value = strtol( buf, &end, 10 );
  if( *end != 0 )
  {
    value = strtol( buf, &end, 16 );
    if( *end != 0 )
    {
      value = 0;
    }
  }
  if( value < 0 ) { value = 0; }
  return( value );
}

