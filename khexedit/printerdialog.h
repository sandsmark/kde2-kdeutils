/*
 *   khexedit - Versatile hex editor
 *   Copyright (C) 1999  Espen Sand, espensa@online.no
 *   This file is based on the work by F. Zigterman, fzr@dds.nl
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

#ifndef _PRINTER_DIALOG_H_
#define _PRINTER_DIALOG_H_

class QButtonGroup;
class QCheckBox;
class QComboBox;
class QFrame; 
class QLabel;
class QLayout;
class QLineEdit;
class QPushButton;
class QRadioButton;
class QSpinBox;

#include <kdialogbase.h>
#include <ksimpleconfig.h>

class CListView;

#include "hexprinter.h"


class CPrinterDialog : public KDialogBase
{
  Q_OBJECT

  public:
    enum EPage
    {
      page_destination = 0,
      page_option,
      page_layout,
      page_max
    };

    struct SPageSize
    {
      double width;
      double height;
      bool isInch;
    };

    CPrinterDialog( QWidget *parent = 0, char *name = 0, bool modal = false );
    ~CPrinterDialog( void );
    void writeConfiguration( void );

  signals:
    void printPostscript( CHexPrinter & );
    void printText( CHexPrinter & );

  protected slots:
    virtual void slotUser1( void );
    virtual void slotUser2( void );
 
  protected:
    virtual void timerEvent( QTimerEvent *e );
    virtual void resizeEvent( QResizeEvent *e );
    virtual void showEvent( QShowEvent *e );


  private:
    struct SDestinationWidgets
    {
      QLabel       *printerLabel;
      CListView    *printerList;
      QCheckBox    *fileCheck;
      QLineEdit    *fileInput;
      QPushButton  *browseButton;
      QRadioButton *allRadio;
      QRadioButton *selectRadio;
      QRadioButton *rangeRadio;
      QLabel       *toLabel;
      QLabel       *fromLabel;
      QLineEdit    *toInput;
      QLineEdit    *fromInput;
      QRadioButton *postscriptRadio;
      QRadioButton *textRadio;
      QLabel       *helpLabel;
    };
    struct SOptionWidgets 
    {
      QLabel       *paperSizeLabel;
      QButtonGroup *oriGroup;
      QRadioButton *portraitRadio;
      QRadioButton *landscapeRadio;
      QButtonGroup *colorGroup;
      QRadioButton *colorRadio;
      QRadioButton *grayRadio;
      QRadioButton *bwRadio;
      QComboBox    *paperSelector;
      QSpinBox     *pageSpin;
      QRadioButton *firstRadio;
      QRadioButton *lastRadio;
      QCheckBox    *scaleCheck;
      QLineEdit    *previewInput;
      QPushButton  *browseButton;

    };
    struct SLayoutWidgets
    {
      QSpinBox     *marginSpin[4];
      QCheckBox    *headerCheck;
      QCheckBox    *footerCheck;
      QLabel       *headerLabel[4];
      QComboBox    *headerCombo[4];
      QLabel       *footerLabel[4];
      QComboBox    *footerCombo[4];
    };

  private slots:
    void printFileCheck( void );
    void browserClicked( void );
    void printRangeClicked( int id );
    void textFormatClicked( int id );
    void paperTypeChanged( int id );
    void appBrowserClicked( void );
    void slotDrawHeader( bool state );
    void slotDrawFooter( bool state );

  private:
    void setupDestinationPage( void );
    void setupOptionPage( void );
    void setupLayoutPage( void );
    void setColumnWidth( void );

    void readConfiguration( void );
    QString headerText( uint index );
    QString headerLine( uint index );
    int headerTextIndex( const QString & headerText );
    int headerLineIndex( const QString & headerLine );

    bool parsePrintcap( void );
    void addPrinterName( const QString &printer );
    void setSelectedPrinter( const QString &name );
    void paperType( QStringList &list );
    const SPageSize paperSize( KPrinter::PageSize pageSize );

  private:
    QFrame *mFrame[ page_max ];
    SDestinationWidgets  mDestination;
    SOptionWidgets mOption;
    SLayoutWidgets mLayout;
    const int mPrinterColumnWidth;
    KSimpleConfig *mConfig;

    QString mWorkDir;
    QString mAppDir;

    CHexPrinter mPrinter;
};





#endif




