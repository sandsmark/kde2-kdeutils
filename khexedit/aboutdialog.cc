/*
 *   khexedit - Versatile hex editor
 *   Copyright (C) 1999  Espen Sand, espensa@online.no
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

#include <kapp.h>
#include <klocale.h>
#include <qlabel.h>

#include "aboutdialog.h"
#include "version.h" // Contains khexedit name string and version string


CAboutKHexEdit::CAboutKHexEdit( QWidget *parent, const char *name, bool modal )
  :KAboutDialog( AbtAppStandard, "khexedit", Close, Close, parent, name, modal)
{
  setTitle( i18n("KDE binary file editor") );
  setProduct( QString(APP_PACKAGE_STRING), QString(APP_VERSION_STRING),
	      QString("Espen Sand"), QString("1999-2000") );

  static const QString text1 = i18n("\n"
    "This program uses modified code and techniques from other KDE programs,\n"
    "specifically kwrite, kiconedit and ksysv. Credit goes to authors\n"
    "and maintainers.\n"
    "\n"
    "Leon Lessing, leon@lrlabs.com, has made parts of the bit swapping\n"
    "functionality.\n"
    "\n"
    "Craig Graham, c_graham@hinge.mistral.co.uk, has made parts of\n"
    "the bit stream functionality of the conversion field.\n"
    "\n"
    "Dima Rogozin, dima@mercury.co.il, has extended the string dialog\n"
    "list capabilities.\n"
    "\n"
    "Edward Livingston-Blade, sbcs@bigfoot.com, has given me very good\n"
    "reports which removed some nasty bugs.\n");

  static const QString text2 = i18n("\n"
    "This program is free software; you can redistribute it and/or modify\n"
    "it under the terms of the GNU General Public License as published by\n"
    "the Free Software Foundation; either version 2 of the License, or\n"
    "(at your option) any later version.\n"
    "\n"
    "This program is distributed in the hope that it will be useful,\n"
    "but WITHOUT ANY WARRANTY; without even the implied warranty of\n"
    "MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n"
    "GNU General Public License for more details.\n"
    "\n"
    "You should have received a copy of the GNU General Public License\n"
    "along with this program; if not, write to the Free Software\n"
    "Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.\n");

  KAboutContainer *c=addContainerPage(i18n("&About"), AlignCenter, AlignLeft );
  if( c != 0 )
  {
    static const QString reasonToMailMe = i18n(""
      "This is a work in progress. If you\n\n"
      "1. have a suggestion for improvement\n"
      "2. have found a bug\n"
      "3. want to contribute with something\n"
      "4. just want to tell me how nice or useful khexedit is\n\n"
      "then feel free to send me a mail.");
    c->addWidget( new QLabel( reasonToMailMe, this ) );

    c->addPerson(QString::null,
      QString("espensa@online.no"),
      QString("http://home.sol.no/~espensa/khexedit/"),
      QString::null, true );
  }

  addTextPage( i18n("C&ontributors"), text1 );
  addTextPage( i18n("&Licence agreement"), text2 );
}

CAboutKHexEdit::~CAboutKHexEdit( void )
{
}



#include "aboutdialog.moc"
