#ifndef __BUTTONGROUP__H__
#define __BUTTONGROUP__H__

#include <qbuttongroup.h>

class ButtonGroup : public QButtonGroup 
{
  public:
    ButtonGroup(const QString & title, QWidget *parent);

  protected:
    virtual void showEvent( QShowEvent * );
    virtual void fontChange( const QFont & );

  private:
    bool changingFont;
};    

#endif
