// dialogs.h			emacs, this is a -*-c++-*- file
//
// This program is free software. See the file COPYING for details.
// Author: Mattias Engdeg�rd, 1997

// misc. handy dialogs for use everywhere

#ifndef DIALOGS_H
#define DIALOGS_H

class QLabel;
class QLineEdit;
class QPixmap;
class QPushButton;
class QSlider;

#include <kdialogbase.h>

// ValueDialog: modal dialog for input of a single value

class ValueDialog : public KDialogBase 
{
    Q_OBJECT
public:
    ValueDialog(const QString &caption, const QString &msg, 
		const QString &ed_txt );

protected slots:
    virtual void slotOk( void );

public:
    QString ed_result;

protected:
    QLineEdit *lined;
};

// SliderDialog: value dialog with additional slider

class SliderDialog : public KDialogBase 
{
    Q_OBJECT
public:
    SliderDialog(const QString &caption, const QString &msg,
		 int defaultval, int minval, int maxval, QWidget *parent=0 );

protected slots:
    virtual void slotOk( void );
    void slider_change(int val);

public:
    QString ed_result;

protected:
    QSlider *slider;
    QLineEdit *lined;
};



#endif	// DIALOGS_H
