// scheddlg.h
//
// This program is free software. See the file COPYING for details.
// Author: Mattias Engdeg�rd, 1997

#ifndef SCHEDDLG_H
#define SCHEDDLG_H

class QLabel;
class QLineEdit;
class QRadioButton;

#include <kdialogbase.h>

class SchedDialog : public KDialogBase 
{
  Q_OBJECT

  public:
    SchedDialog(int policy, int prio);

    int out_prio;
    int out_policy;

  protected slots:
    virtual void slotOK( void );
    void button_clicked(int id);

  private:
    QRadioButton *rb_other, *rb_fifo, *rb_rr;
    QLabel    *label;
    QLineEdit *lined;
};

#endif	// SCHEDDLG_H
