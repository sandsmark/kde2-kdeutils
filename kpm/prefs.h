// prefs.h				emacs, this is written in -*-c++-*-
//
// This program is free software. See the file COPYING for details.
// Author: Mattias Engdeg�rd, 1997, 1998

#ifndef PREFS_H
#define PREFS_H

#include <kdialogbase.h>

class Preferences : public KDialogBase 
{
  Q_OBJECT

  public:
    Preferences( QWidget *parent=0, const char *name=0, bool modal=true );

  public slots:
    void update_boxes();
    void update_reality(int);
    void closed();

  signals:
    void prefs_change();
};

#endif	// PREFS_H
