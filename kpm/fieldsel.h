// fieldsel.h				emacs, this is written in -*-c++-*-
//
// This program is free software. See the file COPYING for details.
// Author: Mattias Engdeg�rd, 1997

#ifndef FIELDSEL_H
#define FIELDSEL_H

#include <qbitarray.h>
#include <kdialogbase.h>

class QCheckBox;
class Procview;

class FieldSelect : public KDialogBase 
{
  Q_OBJECT

  public:
    FieldSelect(Procview *pv, Proc *proc, QWidget *parent=0, 
		const char *name=0, bool modal=true );
  ~FieldSelect();
    void update_boxes();

  public slots:
    void field_toggled(bool);

  protected:
    void set_disp_fields( void );

  signals:
    void added_field(int);
    void removed_field(int);

  protected:
    QCheckBox **buts;
    int nbuttons;
    QBitArray disp_fields;
    bool updating;
    Procview *procview;
};

#endif	// FIELDSEL_H

