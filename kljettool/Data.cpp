#include "Data.h"
#include "Data.moc"

#include <qfile.h>

#include <kapp.h>
#include <klocale.h>
#include <kconfig.h>
#include <kmessagebox.h>

char escapeseq[4] = "\x1b\x25\x2d";


Data::Data(){

  readSettings();
}

Data::~Data(){

}

void Data::operator=(Data data){

  Printer       =   data.Printer.copy();
  Job_name 	=   data.Job_name.copy();
  Density 	=   data.Density.copy();
  Fontnumber 	=   data.Fontnumber.copy();
  Format 	=   data.Format.copy();
  Termination 	=   data.Termination.copy();
  RET 		=   data.RET.copy();
  Copies 	=   data.Copies.copy();
  Powersave 	=   data.Powersave.copy();
  Resolution 	=   data.Resolution.copy();
  Language 	=   data.Language.copy();
  Pageprotect 	=   data.Pageprotect.copy();
  Ptsize 	=   data.Ptsize.copy();
  Pitch 	=   data.Pitch.copy();
  Economode 	=   data.Economode.copy();
  Manualfeed 	=   data.Manualfeed.copy();
  Orientation 	=   data.Orientation.copy();
  Symset 	=   data.Symset.copy();
  Autocont 	=   data.Autocont.copy();
  Formlines 	=   data.Formlines.copy();

 // BUG: return *this; missing. But QObject can't be copied anyway ! (Harri)
 // WABA: Let's make the function return void then.
}

bool Data::print(){


  FILE* printpipe;
  
  QString cmd;
  cmd = "lpr ";
  cmd += "-P" ;
  cmd += Printer;

  //  printf("%s\n",cmd.data());

  printpipe = popen(QFile::encodeName(cmd),"w");
  
  if(printpipe == 0L){
    KMessageBox::sorry(0L,
			  i18n("Unable to print with:\n %1").arg(cmd));
    return FALSE;
  }
  
  fwrite(escapeseq,3,1,printpipe);
  fprintf(printpipe,"12345X@PJL\n");
  fprintf(printpipe,"@PJL JOB NAME = \"%s\"\n",		Job_name.local8Bit().data());
  fprintf(printpipe,"@PJL DEFAULT Density = %s\n",	Density.local8Bit().data());
  fprintf(printpipe,"@PJL DEFAULT Fontnumber = %s\n",	Fontnumber.local8Bit().data());
  fprintf(printpipe,"@PJL DEFAULT Format = %s\n",	Format.local8Bit().data());
  fprintf(printpipe,"@PJL DEFAULT Termination = %s\n",	Termination.local8Bit().data());
  fprintf(printpipe,"@PJL DEFAULT RET = %s\n",		RET.local8Bit().data());
  fprintf(printpipe,"@PJL DEFAULT Copies = %s\n",	Copies.local8Bit().data());
  fprintf(printpipe,"@PJL DEFAULT Powersave = %s\n",	Powersave.local8Bit().data());
  fprintf(printpipe,"@PJL DEFAULT Resolution = %s\n", 	Resolution.local8Bit().data());
  fprintf(printpipe,"@PJL DEFAULT Language = %s\n",	Language.local8Bit().data());
  fprintf(printpipe,"@PJL DEFAULT Pageprotect = %s\n",	Pageprotect.local8Bit().data());
  fprintf(printpipe,"@PJL DEFAULT Ptsize = %s\n",	Ptsize.local8Bit().data());
  fprintf(printpipe,"@PJL DEFAULT Pitch = %s\n",	Pitch.local8Bit().data());
  fprintf(printpipe,"@PJL DEFAULT Economode = %s\n",	Economode.local8Bit().data());
  fprintf(printpipe,"@PJL DEFAULT Manualfeed = %s\n",	Manualfeed.local8Bit().data());
  fprintf(printpipe,"@PJL DEFAULT Orientation = %s\n",	Orientation.local8Bit().data());
  fprintf(printpipe,"@PJL DEFAULT Symset = %s\n",      	Symset.local8Bit().data());
  fprintf(printpipe,"@PJL DEFAULT Autocont = %s\n",	Autocont.local8Bit().data());
  fprintf(printpipe,"@PJL DEFAULT Formlines = %s\n", 	Formlines.local8Bit().data());
  fprintf(printpipe,"@PJL EOJ NAME= \"end profile\"\n");
  fwrite(escapeseq,3,1,printpipe);
  fprintf(printpipe,"12345X\n");

  pclose(printpipe);


  return TRUE;

}

void Data::setPrinter(const QString& printer){


  Printer = printer;
  

}


bool Data::initialize(){

  FILE* printpipe;
  
  QString cmd;
  cmd = "lpr ";
  cmd += "-P" ;
  cmd += Printer;

  //  printf("%s\n",cmd.data());

  printpipe = popen(QFile::encodeName(cmd),"w");

  if(printpipe == 0L){
    KMessageBox::sorry(0L,
			 i18n("Unable to print with:\n %1").arg(cmd));
    return FALSE;
  }
  

  fwrite(escapeseq,3,1,printpipe);
  fprintf(printpipe,"12345X@PJL\n");
  fprintf(printpipe,"@PJL INITIALIZE\n");
  fwrite(escapeseq,3,1,printpipe);
  fprintf(printpipe,"12345X\n");

  pclose(printpipe);


  return TRUE;

}

bool Data::eject(){

  FILE* printpipe;
  
  QString cmd;
  cmd = "lpr ";
  cmd += "-P" ;
  cmd += Printer;

  //  printf("%s\n",cmd.data());

  printpipe = popen(QFile::encodeName(cmd),"w");
  
  if(printpipe == 0L){
    KMessageBox::sorry(0L,
			 i18n("Unable to print with:\n %1").arg(cmd));
    return FALSE;
  }
  

  fwrite(escapeseq,3,1,printpipe);
  fprintf(printpipe,"12345X@PJL\n");
  fprintf(printpipe,"@PJL DEFAULT @PCL ENTER LANGUAGE = PCL\n");

  char ejectseq[4] = "\x1b\x45\x0a";
  fwrite(ejectseq,3,1,printpipe);

  fwrite(escapeseq,3,1,printpipe);
  fprintf(printpipe,"12345X\n");

  pclose(printpipe);

  return TRUE;
}

bool Data::reset(){

  FILE* printpipe;
  
  QString cmd;
  cmd = "lpr ";
  cmd += "-P" ;
  cmd += Printer;

  //  printf("%s\n",cmd.data());

  printpipe = popen(QFile::encodeName(cmd),"w");
  
  if(printpipe == 0L ){
    KMessageBox::sorry(0L,
			 i18n("Unable to print with:\n %1").arg(cmd));
    return FALSE;
  }
  
  fwrite(escapeseq,3,1,printpipe);
  fprintf(printpipe,"12345X@PJL\n");
  fprintf(printpipe,"@PJL DEFAULT RESET\n");
  fwrite(escapeseq,3,1,printpipe);
  fprintf(printpipe,"12345X\n");

  pclose(printpipe);

  return TRUE;
}

void Data::readSettings(){

  KConfig *config;
  config = kapp->config();
  config->setGroup( "Data" );

  /*  
  Printer = "lp";
  Job_name = "default job";
  Density = "3";
  Fontnumber = "0";
  Format = "Letter";
  Termination = "unix";
  RET = "medium";
  Copies = "1";
  Powersave = "15";
  Resolution = "600";
  Language = "pcl";
  Pageprotect = "auto";
  Ptsize = "12.0";
  Pitch = "10.0";
  Economode = "off";
  Manualfeed = "off";
  Orientation = "Portrait";
  Symset = "PC8";
  Autocont = "on";
  Formlines = "60"; // this should be 66 for DIN-A4
  */

  Printer 	= config->readEntry( "Printer","lp" );
  Job_name 	= config->readEntry( "Job_name","default job" );
  Density 	= config->readEntry( "Density","3" );
  Fontnumber	= config->readEntry( "Fontnumber", "0" );
  Format 	= config->readEntry( "Format", "Letter" );
  Termination	= config->readEntry( "Termination","unix" );
  RET 		= config->readEntry( "RET", "medium" );
  Copies	= config->readEntry( "Copies","1" );
  Powersave	= config->readEntry( "Powersave", "15" );
  Resolution	= config->readEntry( "Resolution","600" );
  Language 	= config->readEntry( "Language","pcl" );
  Pageprotect	= config->readEntry( "Pageprotect","auto" );
  Ptsize 	= config->readEntry( "Ptsize","12.0" );
  Pitch 	= config->readEntry( "Pitch","10.0" );
  Economode 	= config->readEntry( "Economode","off" );
  Manualfeed 	= config->readEntry( "Manualfeed","off" );
  Orientation 	= config->readEntry( "Orientation","Portrait" );
  Symset 	= config->readEntry( "Symset","PC8" );
  Autocont	= config->readEntry( "Autocont","on" );
  Formlines 	= config->readEntry( "Formlines","60" );

}

void Data::writeSettings(){

  KConfig *config;
  config = kapp->config();
  config->setGroup( "Data" );

  config->writeEntry( "Printer",Printer );
  config->writeEntry( "Job_name",Job_name );
  config->writeEntry( "Density",Density );
  config->writeEntry( "Fontnumber", Fontnumber );
  config->writeEntry( "Format", Format );
  config->writeEntry( "Termination",Termination );
  config->writeEntry( "RET", RET );
  config->writeEntry( "Copies",Copies );
  config->writeEntry( "Powersave", Powersave );
  config->writeEntry( "Resolution",Resolution );
  config->writeEntry( "Language",Language );
  config->writeEntry( "Pageprotect",Pageprotect );
  config->writeEntry( "Ptsize",Ptsize );
  config->writeEntry( "Pitch",Pitch );
  config->writeEntry( "Economode",Economode );
  config->writeEntry( "Manualfeed",Manualfeed );
  config->writeEntry( "Orientation",Orientation );
  config->writeEntry( "Symset",Symset );
  config->writeEntry( "Autocont",Autocont );
  config->writeEntry( "Formlines",Formlines );
  
  config->sync();

}
