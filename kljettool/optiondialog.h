/*
 *   kljettool - LaserJet Tool
 *   This file only: Copyright (C) 2000 Espen Sand, espen@kde.org
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

#ifndef _OPTION_DIALOG_H_
#define _OPTION_DIALOG_H_

#include "Data.h"

class QCheckBox;
class QComboBox;
class QLabel;
class QLineEdit;
class QRadioButton;

#include <kdialogbase.h>

class OptionDialog : public KDialogBase
{
  Q_OBJECT

  public:
    enum Page
    {
      page_paper = 0,
      page_printer,
      page_operation,
      page_font,
      page_about
    };

    OptionDialog( QWidget *parent=0, char *name=0, bool modal=true );
    ~OptionDialog( void );

  public slots:
    void setPrinter( const QString& printer );

  protected slots:
    virtual void slotDefault( void );
    virtual void slotOk( void );
    virtual void slotApply( void );

  private:
    struct SPaperWidgets
    {
      QComboBox    *formatCombo;
      QLineEdit    *copyEdit;
      QLineEdit    *linesEdit;
      QRadioButton *portraitRadio;
      QRadioButton *landscapeRadio;
      QCheckBox    *manualCheck;
      QCheckBox    *autoCheck;
    };

    struct SPrinterWidgets
    {
      QRadioButton *economyRadio;
      QRadioButton *presentationRadio;
      QRadioButton *dpi300Radio;
      QRadioButton *dpi600Radio;
      QComboBox    *printerCombo;
      QComboBox    *densityCombo;
      QComboBox    *resolutionCombo;
    };

    struct SOperationWidgets
    {
      QRadioButton *unixRadio;
      QRadioButton *dosRadio;
      QComboBox    *pageProtectCombo;
      QComboBox    *powersaveCombo;
    };

    struct SFontWidgets
    {
      QComboBox *languageCombo;
      QComboBox *symbolCombo;
      QLineEdit *fontEdit;
      QLineEdit *pitchEdit;
      QLineEdit *pointSizeEdit;
    };

  private:
    void setupPaperPage( void );
    void setupPrinterPage( void );
    void setupOperationPage( void );
    void setupFontPage( void );
    void setupAboutPage( void );

    void setState( void );
    void setPrinter( const Data &state );
    void setPaper( const Data &state );
    void setOperations( const Data &state );
    void setFont( const Data &state );
    void parsePrintcap( void );

  private slots:
    void printcapReadError( void );
    void printcapNoEntryError( void );
    void ejectButtonClicked( void );
    void resetButtonClicked( void );
    void initializeButtonClicked( void );

  private:
    Data              mState;
    SPaperWidgets     mPaper;
    SPrinterWidgets   mPrinter;
    SOperationWidgets mOperation;
    SFontWidgets      mFont;
};


#endif







