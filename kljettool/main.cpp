    /*

    $Id: main.cpp 39559 2000-01-30 02:40:10Z charles $

    Requires the Qt widget libraries, available at no cost at 
    http://www.troll.no
       
    Copyright (C) 1997 Bernd Johannes Wuebben   
                       wuebben@math.cornell.edu


    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

    */

//
// 2000-01-23 Espen Sand
// Heavily modified. Looks (almost) the same but the widget is now 
// QLayout based.
//

#include <kapp.h>
#include <kcmdlineargs.h>
#include <klocale.h>
#include <kaboutdata.h>

#include "optiondialog.h"
#include "version.h"

static const char *description = 
	I18N_NOOP("KDE tool for LaserJet(tm) printers");

int main ( int argc, char *argv[] )
{
  KAboutData aboutData( "kljettool", I18N_NOOP("KDE Laserjet Tool"),
    KLJETVERSION, description, KAboutData::License_GPL,
    "(c) 1997, Bernd Johannes Wuebben");
  aboutData.addAuthor("Bernd Johannes Wuebben",0, "wuebben@math.cornell.edu");
  KCmdLineArgs::init( argc, argv, &aboutData );

  KApplication a;
  
  OptionDialog *optionDialog = new OptionDialog();
  optionDialog->exec();
  return 0;
}
