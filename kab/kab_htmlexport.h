/* -*- C++ -*-
   This file declares widgets and other stuff related to HTML
   exporting in kab. 

   the KDE addressbook

   $ Author: Mirko Boehm $
   $ Copyright: (C) 1996-2000, Mirko Boehm $
   $ Contact: mirko@kde.org
         http://www.kde.org $
   $ License: GPL with the following explicit clarification:
         This code may be linked against any version of the Qt toolkit
         from Troll Tech, Norway. $

   $Revision: 90511 $	
*/

#ifndef KAB_HTMLEXPORT_H
#define KAB_HTMLEXPORT_H

#include "kab_htmlexportsetupbase.h" // created by uic
#include <map>

struct QStringLess
  : public std::binary_function<const QString&, const QString&, bool>
{
  /** The function operator, inline. */
  bool operator()(const QString& x, const QString& y) const
  {
    return x < y; // make one Qt operator fit exactly
  }
};

class HTMLExportSetup : public HTMLExportSetupBase
{
  Q_OBJECT
public:
  HTMLExportSetup(QWidget *parent=0);
  void slotTemplateFolder();
  void slotTargetFolder();
  const QString templateFolder();
  const QString targetFolder();
};

#endif // KAB_HTMLEXPORT_H
