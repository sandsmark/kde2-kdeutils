/* -*- C++ -*-
   This file implements the base class for kab�s looks.

   the KDE addressbook

   $ Author: Mirko Boehm $
   $ Copyright: (C) 1996-2000, Mirko Boehm $
   $ Contact: mirko@kde.org
         http://www.kde.org $
   $ License: GPL with the following explicit clarification:
         This code may be linked against any version of the Qt toolkit
         from Troll Tech, Norway. $

   $Revision: 66582 $	 
*/

#ifndef LOOK_KABBASIC_H
#define LOOK_KABBASIC_H

// this includes the AddressBook::Entry definition
class KabAPI;

#include <kabapi.h>

/** This is a pure virtual base class that defines the
 *  interface for how to display and change entries of
 *  the KDE addressbook.
 *
 *  This basic widget does not show anything in its client space.
 *  Derive it and implement its look and how the user may edit the
 *  entry.
 *
 *  The repaintEvent() has to paint the whole widget, since repaint()
 *  calls will not delete the widgets background. */
class KABBasicLook : public QWidget
{
  Q_OBJECT
public:
  /** The constructor. */
  KABBasicLook(KabAPI* database, QWidget* parent=0, const char* name=0);
  /** Set the entry. It will be displayed automatically. */
  virtual void setEntry(const AddressBook::Entry&);
  /** Get the current entry. */
  virtual void getEntry(AddressBook::Entry& entry);
signals:
  /** This signal is emitted when the user changed the entry. */
  void entryChanged();
  /** This signal indicates that the entry need sto be changed
      immidiately in the database. This might be due to changes in
      values that are available in menus. */
  void saveMe();
  /** The user acticated the email address displayed. This may happen
      by, for example, clicking on the displayed mailto-URL. */
  void sendEmail(const QString& address);
  /** The user activated one of the displayed HTTP URLs. For example
      by clicking on the displayed homepage address. */
  void browse(const QString& url);
protected:
  /** The displayed entry. */
  AddressBook::Entry current;
  /** Pointer to the database object. */
  KabAPI *db;
};

#endif // LOOK_KABBASIC_H
