/* -*- C++ -*-
   This file implements kab�s main widget.
   The main widget is inserted into the top level widget derived from
   KMainWindow. 
   
   the KDE addressbook

   $ Author: Mirko Boehm $
   $ Copyright: (C) 1996-2000, Mirko Boehm $
   $ Contact: mirko@kde.org
         http://www.kde.org $
   $ License: GPL with the following explicit clarification:
         This code may be linked against any version of the Qt toolkit
         from Troll Tech, Norway. $

   $Revision: 89703 $	
*/

#ifndef KABMAINWIDGET_H
#define KABMAINWIDGET_H

#include "kab_topwidget.h"
#include <qwidget.h>
#include <qsplitter.h>

class KABBasicLook;
class KabAPI;
class KabEntryList; // the list left to the entry view, showing all entries
class SearchResultsWidget; // displays search results if there are any
class QFrame;
class KabMainWidget : public QWidget
{
  Q_OBJECT
public:
  /** Constructor. */
  KabMainWidget(KabAPI* api, QWidget *parent=0, const char* name=0);
  /** Set the view. */
  void createView(TopLevelWidget::View=TopLevelWidget::NoView, bool recreate=false);
  /** Get a pointer to the view. */
  KABBasicLook *getView();
  /** Get a pointer to the search results window. */
  SearchResultsWidget *searchresults();
  /** Get a pointer to the entry list widget. */
  KabEntryList *entrylist();
  TopLevelWidget::View viewType();
protected:
  QSplitter *splVertical;
  QSplitter *splHorizontal;
  KabEntryList *list;
  SearchResultsWidget *search;
  QFrame *viewFrame; // container for the view
  KABBasicLook *view;
  TopLevelWidget::View currentView;
  KabAPI *api;
public slots:
  void slotShowSearchResults(bool);
  void slotHide(); // overloaded from ...Base
  void slotSetStatus(const QString&);
  void slotShowEntryList(bool);
signals:
  void showSearchResults(bool);
  void setStatus(const QString&);
};

#endif // defined KABMAINWIDGET_H
