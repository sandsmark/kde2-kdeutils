/* -*- C++ -*-
   This file implements the editing look.

   the KDE addressbook

   $ Author: Mirko Boehm $
   $ Copyright: (C) 1996-2000, Mirko Boehm $
   $ Contact: mirko@kde.org
         http://www.kde.org $
   $ License: GPL with the following explicit clarification:
         This code may be linked against any version of the Qt toolkit
         from Troll Tech, Norway. $

   $Revision: 66582 $	 
*/

#include "look_edit_basictab.h"
#include "look_edit.h"
#include "look_edit_tabperson.h"
#include "look_edit_tabaddresses.h"
#include "look_edit_tabtelephone.h"
#include "look_edit_tabuser.h"
#include <qtabwidget.h>
#include <kmessagebox.h>
#include <klocale.h>
#include <kdebug.h>
#include <kabapi.h>
#include <qconfigDB.h>
#include <qlist.h>

KABEditLook::KABEditLook(KabAPI* api, QWidget* parent, const char* name)
  : KABBasicLook(api, parent, name)
{
  Section *configsection;
  KeyValueMap *keys;
  QString headline=i18n("User Fields");
  // -----
  tabs=new QTabWidget(this);
  if(tabs==0)
    {
      KMessageBox::sorry
	(this, i18n("Out of memory."),
	 i18n("General failure."));
      ::exit(-1);
    }
  tabbirthday=new TabBirthday(api, tabs);
  tabperson=new TabPerson(api, tabs);
  tabcomment=new TabComment(api, tabs);
  tabuser=new TabUser(api, tabs);
  tabtelephone=new TabTelephone(api, tabs);
  tabaddresses=new TabAddresses(api, tabs);
  if(tabbirthday==0 || tabperson==0 || tabcomment==0 || tabuser==0
     || tabtelephone==0 || tabaddresses==0)
    {
      KMessageBox::sorry
	(this, i18n("Out of memory."),
	 i18n("General failure."));
      ::exit(-1);
    }
  // ----- find out the caption of the user fields tab:
  if(db!=0) // be careful to avoid segfaults in the ctor
    {
      configsection=db->addressbook()->configurationSection();
      if(configsection!=0)
	{
	  keys=configsection->getKeys();
	  keys->get("user_headline", headline);

         if( "(User fields)" == headline )
            headline = i18n("(User fields)");                                   
	}
      tabuser->configure(db);
    }
  tabs->addTab(tabperson, i18n("&Person"));
  tabs->addTab(tabtelephone, i18n("&Telephone"));
  tabs->addTab(tabaddresses, i18n("&Addresses"));
  tabs->addTab(tabbirthday, i18n("&Birthday"));
  tabs->addTab(tabcomment, i18n("Comment"));
  tabs->addTab(tabuser, headline);
  allTabs.append((TabBasic**)&tabperson);
  allTabs.append((TabBasic**)&tabaddresses);
  allTabs.append((TabBasic**)&tabtelephone);
  allTabs.append((TabBasic**)&tabbirthday);
  allTabs.append((TabBasic**)&tabcomment);
  allTabs.append((TabBasic**)&tabuser);
  // -----
  connect(tabbirthday, SIGNAL(changed()), SLOT(changed()));
  connect(tabaddresses, SIGNAL(changed()), SLOT(changed()));
  connect(tabtelephone, SIGNAL(changed()), SLOT(changed()));
  connect(tabperson, SIGNAL(changed()), SLOT(changed()));
  connect(tabperson, SIGNAL(saveMe()), SLOT(saveMeSlot()));
  connect(tabcomment, SIGNAL(changed()), SLOT(changed()));
  connect(tabuser, SIGNAL(changed()), SLOT(changed()));
}

void KABEditLook::resizeEvent(QResizeEvent*)
{
  tabs->setGeometry(0, 0, width(), height());
}

void KABEditLook::changed()
{
  register bool GUARD; GUARD=false;
  // kdDebug() << "KABEditLook::changed: entry changed." << endl;
  emit(entryChanged()); // signal from basic look
}

void KABEditLook::saveMeSlot()
{
  emit(saveMe());
}

void KABEditLook::setEntry(const AddressBook::Entry& entry)
{
  unsigned int count;
  // ----- set the contents in all tabs:
  for(count=0; count<allTabs.count(); ++count)
    {
      (*allTabs.at(count))->setContents(entry);
    }
  // ----- call the setEntry method of the base class:
  KABBasicLook::setEntry(entry);
}

void KABEditLook::getEntry(AddressBook::Entry& entry)
{
  unsigned int count;
  // ----- set the contents in all tabs:
  for(count=0; count<allTabs.count(); ++count)
    { // store the changes in our own entry object:
      (*allTabs.at(count))->storeContents(current);
    }
  // ----- call the setEntry method of the base class:
  KABBasicLook::getEntry(entry);
}

QSize KABEditLook::minimumSizeHint() const
{
  return tabs->minimumSizeHint();
}


#include "look_edit.moc"
