/* -*- C++ -*-
   This file implements the dialog to enter organisational attributes
   in the KDE address book.

   the KDE addressbook
   
   $ Author: Mirko Boehm $
   $ Copyright: (C) 1996-2000, Mirko Boehm $
   $ Contact: mirko@kde.org
         http://www.kde.org $
   $ License: GPL with the following explicit clarification:
         This code may be linked against any version of the Qt toolkit
         from Troll Tech, Norway. $

   $Revision: 95919 $	 
*/

#include "dialog_insinuation.h"
#include <qlineedit.h>
#include <qlabel.h>
#include <qlayout.h>
#include <klocale.h>

InsinuationDialog::InsinuationDialog(QWidget *parent, const char*
				     name)
  : QWidget(parent, name)
{
  QString texts[]= {
    i18n("Organization:"),
    i18n("Department:"),
    i18n("Sub-Department:")
  };
  int count;
  QGridLayout *layout=new QGridLayout(this, 3, 2, 2);
  layout->setAutoAdd(true);
  // -----
  for(count=0; count<NoOfFields; ++count)
    {
      new QLabel(texts[count], this);
      fields[count]=new QLineEdit(this);
    }
}

InsinuationDialog::~InsinuationDialog()
{
}

void InsinuationDialog::setFields(const AddressBook::Entry::Address& address)
{
  int count=0;
  // -----
  fields[count++]->setText(address.org);
  fields[count++]->setText(address.orgUnit);
  fields[count++]->setText(address.orgSubUnit);
}

void InsinuationDialog::getFields(AddressBook::Entry::Address& address)
{
  int count=0;
  // -----
  address.org=fields[count++]->text();
  address.orgUnit=fields[count++]->text();
  address.orgSubUnit=fields[count++]->text();
}
#include "dialog_insinuation.moc"
