/* -*- C++ -*-
   This file implements kabs main function.

   the KDE addressbook

   $ Author: Mirko Boehm $
   $ Copyright: (C) 1996-2000, Mirko Boehm $
   $ Contact: mirko@kde.org
         http://www.kde.org $
   $ License: GPL with the following explicit clarification:
         This code may be linked against any version of the Qt toolkit
         from Troll Tech, Norway. $

   $Revision: 89708 $	 
*/

#include <kapp.h>
#include <klocale.h>
#include <kcmdlineargs.h>
#include <kmessagebox.h>
#include <iostream.h>
#include <kabapi.h>
#include <kaboutdata.h>
#include <kdebug.h>
#include "kab_topwidget.h"
// #include "look_businesscard.h"
#include "kab_mainwidget.h"


static const char *description = 
	I18N_NOOP("KDE Addressbook");

static const char *version = "v2.0";

int main(int argc, char** argv)
{
  int rc;
  KAboutData aboutData( "kab", I18N_NOOP("KDE Addressbook"),
    version, description, KAboutData::License_GPL,
    "(c) Mirko Boehm");
  //  aboutData.addAuthor("",0, "");
  KCmdLineArgs::init( argc, argv, &aboutData );

  KApplication app;
  // -----
  kdDebug() << "kab main: starting application." << endl;
  // KAB toplevelwidgets are closing non-destructive!
  TopLevelWidget *widget=new TopLevelWidget;
  KMessageBox::information
    (0,
     i18n("<qt>Version 2 of the KDE address book is not compatible "
	  "with the previous one. "
	  "To protect your address database, a new file will be used. "
	  "Run <b>File->Import->KDE 1 addressbook</b> to use your old "
	  "database with the new version.</qt>"),
     i18n("new format"),
     "kab_new_version_incompatibility");
  app.setMainWidget(widget);
  widget->show();
  rc=app.exec();
  // widget->cleanup();
  kdDebug() << "kab main: exiting with exit code " << rc << "." << endl;
  return rc;
}

