/* -*- C++ -*-
   This file implements the dialog that is used to edit an entry.

   the KDE addressbook

   $ Author: Mirko Boehm $
   $ Copyright: (C) 1996-2000, Mirko Boehm $
   $ Contact: mirko@kde.org
         http://www.kde.org $
   $ License: GPL with the following explicit clarification:
         This code may be linked against any version of the Qt toolkit
         from Troll Tech, Norway. $

   $Revision: 71841 $
*/

#ifndef DIALOG_EDIT_ENTRY_H
#define DIALOG_EDIT_ENTRY_H

#include <kdialogbase.h>
#include <klocale.h>
#include "look_edit.h"
#include "kab_topwidget.h"
#include "kab_mainwidget.h"
#include <kdebug.h>

void TopLevelWidget::editEntrySlot()
{
  editCurrentEntry();
}

bool TopLevelWidget::editCurrentEntry()
{
  // it must be ensured that all other changes are committed to the database
  // before calling this method since it uses the entry as it is found into
  // the database
  AddressBook::Entry entry;
  // WORK_TO_DO: handle size
  // ----- get the entry from the view:
  mainwidget->getView()->getEntry(entry);
  if(editEntry(entry))
    {
      emit(setStatus(i18n("Accepted.")));
      mainwidget->getView()->setEntry(entry);
      if(api->addressbook()->change(*current, entry)!=AddressBook::NoError)
	{
	  emit(setStatus(i18n("Error changing the entry.")));
	} else {
	  save();
	}
      updateGUI();
      return true;
    } else {
      emit(setStatus(i18n("Rejected.")));
      return false;
    }
}

bool TopLevelWidget::editEntry(AddressBook::Entry& entry)
{
  // ----- create a KDialogBase dialog containing a KABEditLook widget:
  KDialogBase dialog(this);
  KABEditLook *look=new KABEditLook(api, &dialog);
  look->setMinimumSize(look->minimumSizeHint());
  dialog.showButtonApply(false);
  dialog.enableButtonSeparator(true);
  dialog.setMainWidget(look);
  dialog.setCaption(i18n("Edit entry"));
  // ----- display the editing dialog:
  look->setEntry(entry);
  if(dialog.exec())
    {
      emit(setStatus(i18n("Accepted.")));
      look->getEntry(entry);
      return true;
    } else {
      emit(setStatus(i18n("Rejected.")));
      return false;
    }
}

#endif // DIALOG_EDIT_ENTRY_H
