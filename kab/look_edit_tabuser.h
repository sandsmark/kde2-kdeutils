/* -*- C++ -*-
   This file implements the user fields tab for kab�s editing look.
 
   the KDE addressbook

   $ Author: Mirko Boehm $
   $ Copyright: (C) 1996-2000, Mirko Boehm $
   $ Contact: mirko@kde.org
         http://www.kde.org $
   $ License: GPL with the following explicit clarification:
         This code may be linked against any version of the Qt toolkit
         from Troll Tech, Norway. $

   $Revision: 66582 $
*/

#ifndef KAB_LOOK_EDIT_TABUSER_H
#define KAB_LOOK_EDIT_TABUSER_H "$ID$"

#include <look_edit_basictab.h>

class QMultiLineEdit;
class QLabel;
class QGridLayout;
class KabApi;

class TabUser : public TabBasic
{
  Q_OBJECT
public:
  TabUser(KabAPI *, QWidget *parent=0);
  ~TabUser();
  /** Derived from TabBasic. */
  void storeContents(AddressBook::Entry& entry);
  /** Dito. */
  void setContents(const AddressBook::Entry& entry);
  /** Configure this tab according to the file settings. */
  void configure(KabAPI*);
protected:
  enum Fields {
    /* Headline, */
    User1, User2, User3, User4, NoOfFields
  };
  QMultiLineEdit *mledits[NoOfFields];
  QLabel *labels[NoOfFields];
  QString *texts[NoOfFields];
  QCString *keys[NoOfFields];
  QGridLayout *layout;
protected slots:
  void textChangedSlot();
};

#endif // KAB_LOOK_EDIT_TABUSER_H
