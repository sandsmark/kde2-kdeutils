/* -*- C++ -*-
   This file implements the tab to edit the addresses.

   the KDE addressbook

   $ Author: Mirko Boehm $
   $ Copyright: (C) 1996-2000, Mirko Boehm $
   $ Contact: mirko@kde.org
         http://www.kde.org $
   $ License: GPL with the following explicit clarification:
         This code may be linked against any version of the Qt toolkit
         from Troll Tech, Norway. $

   $Revision: 73148 $	 
*/

#ifndef KAB_LOOK_EDIT_TABADDRESSES_H
#define KAB_LOOK_EDIT_TABADDRESSES_H "$ID$"

#include "look_edit_basictab.h"
#include "look_edit_tabaddressesbase.h"
#include <addressbook.h>
#include <list>

class QComboBox;
class QPushButton;
class QPopupMenu;
class QToolButton;
class QGridLayout;
class QLineEdit;
class QLabel;
class QString;
class KabAPI;

class TabAddresses : public TabBasic
{
  Q_OBJECT
public:
  TabAddresses(KabAPI*, QWidget *parent=0);
  /** Derived from TabBasic. */
  void storeContents(AddressBook::Entry& entry);
  /** Dito. */
  void setContents(const AddressBook::Entry& entry);
protected:
  /** The widget created with the designer. */
  TabAddressesBase *base;
  /** The index of the item currently displayed. Set by highlighted (exclusivly). */
  int currentItem;
  /** The list of addresses. */
  std::list<AddressBook::Entry::Address> addresses;
  /** A method to fill the selector with the address headlines. */
  void fillSelector();
  /** A method to fill all fields with the values of this address. */
  void fillFields();
  /** A method to write all changes back to the current address object. */
  void storeChanges();
  /** Remember user input. */
  bool modified;
  /** An iterator to the currently selected Address object. */
  std::list<AddressBook::Entry::Address>::iterator currentAddress();
  /** QPopupMenu to display "type of address" choices. */
  QPopupMenu *popupAddresses;
protected slots:
  /** Create a new address. */
  void addAddress();
  /** Delete an address. */
  void deleteAddress();
  /** Edit the organisational fields. */
  void insinuation();
  /** Edit the delivery label. */
  void deliveryLabel();
  /** An address has been highlighted by the user. */
  void highlighted(int);
  /** The currently selected address has been changed. */
  void addressChanged();
  /** The same, for line edits. */
  void addressChanged(const QString &);
  /** The headline has been changed. */
  void headlineChanged(const QString &);
};

#endif // KAB_LOOK_EDIT_TABADDRESSES_H
