/* -*- C++ -*-
   This file implements the user fields tab for kab�s editing look..

   the KDE addressbook

   $ Author: Mirko Boehm $
   $ Copyright: (C) 1996-2000, Mirko Boehm $
   $ Contact: mirko@kde.org
         http://www.kde.org $
   $ License: GPL with the following explicit clarification:
         This code may be linked against any version of the Qt toolkit
         from Troll Tech, Norway. $
 
   $Revision: 66582 $
*/

#include <klocale.h>
#include <qstring.h>
#include <qlayout.h>
#include <qlabel.h>
#include <qmultilineedit.h>
#include <kmessagebox.h>
#include <kdebug.h>
#include <kabapi.h>
#include <qconfigDB.h>
#include "look_edit_tabuser.h"

TabUser::TabUser(KabAPI* api, QWidget *parent)
  : TabBasic(api, parent)
{
  int row=0;
  const QString Texts[] = { // needed to get the translations:
    // i18n("Headline:"),
    i18n("User field 1:"),
    i18n("User field 2:"),
    i18n("User field 3:"),
    i18n("User field 4:")
  };
  const QCString Keys[] = { // the keys in the configuration section:
    // "user_headline",
    "user_1", "user_2", "user_3", "user_4"
  };
  int count;
  // -----
  layout=new QGridLayout(this, 9, 1, 3, 2);
  if(layout==0)
    {
      KMessageBox::sorry
	(this, i18n("Out of memory."),
	 i18n("General failure."));
      ::exit(-1);
    } 
  for(count=0; count<NoOfFields; ++count)
    {
      texts[count]=new QString(Texts[count]);
      keys[count]=new QCString(Keys[count]);
      labels[count]=new QLabel(*texts[count], this);
      layout->addWidget(labels[count], row++, 0);
      mledits[count]=new QMultiLineEdit(this);
      layout->addWidget(mledits[count], row++, 0);
      connect(mledits[count], SIGNAL(textChanged()),
	      SLOT(textChangedSlot()));
    }
  layout->setRowStretch(row, 1);
}

TabUser::~TabUser()
{
  int count;
  // -----
  for(count=0; count<NoOfFields; ++count)
    {
      delete texts[count];
      delete keys[count];
    }
}

void TabUser::storeContents(AddressBook::Entry& entry)
{
  QString *strings[]= {
    &entry.user1, &entry.user2, &entry.user3, &entry.user4
  };
  int count;
  // -----
  for(count=0; count<NoOfFields; ++count)
    {
      *(strings[count])=mledits[count]->text();
    }
}

void TabUser::setContents(const AddressBook::Entry& entry)
{
  // QString headline;
  const QString *strings[]= {
    // &headline,
    &entry.user1, &entry.user2, &entry.user3, &entry.user4
  };
  int count;
  // -----
  for(count=0; count<NoOfFields; ++count)
    {
      mledits[count]->setText(*strings[count]);
    }
}

void TabUser::textChangedSlot()
{
  //  kdDebug() << "TabUser::textChangedSlot: user field text changed."
  // << endl;
  emit(changed());
}

void TabUser::configure(KabAPI* api)
{
  /* All keys are set in the constructor that fills the arrays. */
  Section *configsection;
  KeyValueMap *configkeys;
  QString text;
  int count;
  // -----  
  if(api!=0)
    {
      configsection=api->addressbook()->configurationSection();
      if(configsection!=0)
	{
	  configkeys=configsection->getKeys();
	  for(count=0; count<NoOfFields; ++count)
	    {
	      text=*texts[count];
	      if(!configkeys->get(*keys[count], text))
		{
		  kdDebug() << "TabUser::configure: missing settings " <<
		    "in configuration section!" << endl;
		}

              if( (0 == count) && ("(User field 1)" == text) )
                text = i18n("(User field 1)");
 
              else if( (1 == count) && ("(User field 2)" == text) )
                text = i18n("(User field 2)");
 
              else if( (2 == count) && ("(User field 3)" == text) )
                text = i18n("(User field 3)");
 
              else if( (3 == count) && ("(User field 4)" == text) )
                text = i18n("(User field 4)");                                  

	      labels[count]->setText(text);
	    }
	}
    }
}

      

#include "look_edit_tabuser.moc"
