/* -*- C++ -*-
   This file implements the method to create new addressbook files..

   the KDE addressbook

   $ Author: Mirko Boehm $
   $ Copyright: (C) 1996-2000, Mirko Boehm $
   $ Contact: mirko@kde.org
         http://www.kde.org $
   $ License: GPL with the following explicit clarification:
         This code may be linked against any version of the Qt toolkit
         from Troll Tech, Norway. $

   $Revision: 71790 $	
*/

#include <kabapi.h>
#include <kfiledialog.h>
#include <kmessagebox.h>
#include <klocale.h>
#include <qfileinfo.h>
#include <qstring.h>
#include <qdir.h>
#include <kapp.h>
#include <kdebug.h>
#include <kurl.h>
#include <knotifyclient.h>
#include "kab_topwidget.h"

void TopLevelWidget::createNew()
{
  register bool GUARD; GUARD=true;
  // ###########################################################################
  kdDebug() << "KabMainWindow::newFile: called.";
  QString home, filename;
  KURL dummy;
  QFileInfo info;
  // ----- select the filename:
  home=QDir::homeDirPath();
  if(home.isEmpty())
    {
      KMessageBox::sorry
	(this, i18n("Could not find the users home directory."), i18n("Sorry"));
      setStatus(i18n("Intern error!"));
      KNotifyClient::beep();
      return;
    }
  for(;;) // do forever
    {
      // dummy=KFileDialog::getOpenFileName(home, "*kab", this);
      dummy=KFileDialog::getOpenURL(home, "*kab", this);
      kdDebug() << "KabMainWindow::newFile: URL is " << dummy.url() << endl;
      if(dummy.isEmpty()) // dialog has been cancelled
	{
	  setStatus(i18n("Cancelled."));
	  KNotifyClient::beep();
	  return;
	}
      // WORK_TO_DO: download the URL or find the local file name in filename
      if(!dummy.isLocalFile())
      {
        setStatus( i18n( "Only local files supported yet." ) );
	KNotifyClient::beep();
	return;
      } else {
	filename=dummy.path();
      }
      // ...
      // -----
      info.setFile(filename);
      if(info.isDir() || info.exists())
	{
	  KMessageBox::sorry
	    (this,
	     i18n("This file is a directory, or it already exists.\n"
		  "Please select another filename.\n"
		  "You must have permission to write "
		  "to create the new file."),
	     i18n("File error"));
	} else {
	  kdDebug() << "KabMainWindow::newFile: filename is " << filename << endl;
	  break;
	}
    }
  // ----- adjust name:
  if(filename.length()<4 || filename.mid(filename.length()-4, 4)!=".kab")
    {
      filename+=".kab";
      KMessageBox::information
	(this,
	 i18n("The filename did not end with \".kab\".\n"
	      "It has been changed to %1\n").arg(filename),
	 i18n("Note"));
    }
  // ----- create the new database file:
  if(api->addressbook()->createNew(filename)!=AddressBook::NoError)
    {
      KMessageBox::sorry
	(this,
	 i18n("The file could not be created, possibly permission denied."),
	 i18n("kab: File error"));
      KNotifyClient::beep();
      return;
    }
  // ----- load this file:
  if(api->addressbook()->load(filename)!=AddressBook::NoError)
    {
      KMessageBox::sorry
	(this, i18n("The file has been created, but could not be loaded."),
	 i18n("File error"));
      KNotifyClient::beep();
      return;
    }
  // -----
  modified=false;
  updateGUI();
  kdDebug() << "KabMainWindow::newFile: done.";
  // ###########################################################################
}


