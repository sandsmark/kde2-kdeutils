/* -*- C++ -*-
   This file implements the editing look.
 
   the KDE addressbook

   $ Author: Mirko Boehm $
   $ Copyright: (C) 1996-2000, Mirko Boehm $
   $ Contact: mirko@kde.org
         http://www.kde.org $
   $ License: GPL with the following explicit clarification:
         This code may be linked against any version of the Qt toolkit
         from Troll Tech, Norway. $

   $Revision: 66582 $	 
*/

#ifndef LOOK_EDIT_H
#define LOOK_EDIT_H

#include "look_basic.h"

class QTabWidget;
class TabPerson;
class TabAddresses;
class TabBirthday;
class TabBasic;
class TabComment;
class TabUser;
class TabContact;
class TabTelephone;

class KABEditLook : public KABBasicLook
{
  Q_OBJECT
public:
  /** The constructor. */
  KABEditLook(KabAPI*, QWidget* parent=0, const char* name=0);
  /** Overload basic set entry method. */
  void setEntry(const AddressBook::Entry&);
  /** Overload method to get the entry. */
  void getEntry(AddressBook::Entry& entry);
  QSize minimumSizeHint() const;
protected:
  /** The tabulator widget. */
  QTabWidget *tabs;
  // WORK_TO_DO: check if pointers are really needed
  /** The person tab. */
  TabPerson *tabperson;
  /** The telephone tab. */
  TabTelephone *tabtelephone;
  /** The birthday tab. */
  TabBirthday *tabbirthday;
  /** The comment tab. */
  TabComment *tabcomment;
  /** The user fields tab. */
  TabUser *tabuser;
  /** The tab to enter contact information. */
  TabContact *tabcontact;
  /** The tab to edit the addresses. */
  TabAddresses *tabaddresses;
  /** A list of all tabs. */
  QList<TabBasic*> allTabs;
  /** The resize event. */
  void resizeEvent(QResizeEvent*);
public slots:
  /** Emitted by the different tabs to notify changes. */
  void changed();
  /** Catch the save me messages emitted by the tabs. */
  void saveMeSlot();
};

#endif // LOOK_EDIT_H
