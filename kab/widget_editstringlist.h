/* -*- C++ -*-
   This file declares a widget to edit string lists.

   the KDE addressbook

   $ Author: Mirko Boehm $
   $ Copyright: (C) 1996-2000, Mirko Boehm $
   $ Contact: mirko@kde.org
         http://www.kde.org $
   $ License: GPL with the following explicit clarification:
         This code may be linked against any version of the Qt toolkit
         from Troll Tech, Norway. $

   $Revision: 66582 $
*/
  
#ifndef StringListEditWidget_included
#define StringListEditWidget_included

#include <qstringlist.h>
#include <qwidget.h>

class QPushButton;
class QLineEdit;
class QListBox;

class StringListEditWidget : public QWidget
{
  // ############################################################################
  Q_OBJECT
  // ----------------------------------------------------------------------------
public:
  StringListEditWidget(QWidget* parent=0, const char* name=0);
  virtual ~StringListEditWidget();
  QSize sizeHint() const;
  void setStrings(const QStringList& strings);
  void getStrings(QStringList& result);
  // ----------------------------------------------------------------------------
public slots:
  void initializeGeometry();
  // ----------------------------------------------------------------------------
protected slots:
  virtual void downPressed();
  virtual void itemSelected(int);
  virtual void itemChanged(const QString&);
  virtual void newItem();
  virtual void deletePressed();
  virtual void upPressed();
  // ----------------------------------------------------------------------------
protected:
  void resizeEvent(QResizeEvent*);
  void enableButtons();
  QListBox* lbStrings;
  QPushButton* buttonUp;
  QPushButton* buttonDown;
  QPushButton* buttonDelete;
  QLineEdit* leLine;
  QPushButton* buttonNew;
  // ############################################################################
};

#endif // StringListEditWidget_included




