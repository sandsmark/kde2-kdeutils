/* $Revision: 94391 $ */

#ifndef KAB_NETSCAPE_LDIF_H
#define KAB_NETSCAPE_LDIF_H

#include <kdialogbase.h>
#include <kabapi.h>

class Netscape_io : public KabAPI
{
	Q_OBJECT
public:
	Netscape_io(KabAPI *, QWidget *parent=0);
	int Netscape_ldif_Import();
	/* int Netscape_ldif_Export(); */
protected:
	KabAPI *api;
signals:
	void setStatus(const QString&);
};

#endif	/* KAB_NETSCAPE_LDIF_H */
