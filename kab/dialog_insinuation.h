/* -*- C++ -*-
   This file declares the dialog to enter organisational attributes
   in the KDE address book.

   the KDE addressbook
   $ Author: Mirko Boehm $
   $ Copyright: (C) 1996-2000, Mirko Boehm $
   $ Contact: mirko@kde.org
         http://www.kde.org $
   $ License: GPL with the following explicit clarification:
         This code may be linked against any version of the Qt toolkit
         from Troll Tech, Norway. $

   $Revision: 66582 $
*/

#ifndef DIALOG_INSINUATION_H
#define DIALOG_INSINUATION_H

#include <qwidget.h>
#include <addressbook.h>

class QLineEdit;

#define NoOfFields 3

class InsinuationDialog : public QWidget
{
  Q_OBJECT
public:
  InsinuationDialog(QWidget* parent, const char* name=0);
  ~InsinuationDialog();
  void setFields(const AddressBook::Entry::Address&);
  void getFields(AddressBook::Entry::Address&);
protected:
  QLineEdit *fields[NoOfFields];
};

#endif // DIALOG_INSINUATION_H

