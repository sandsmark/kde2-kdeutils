#ifndef SEARCHRESULTSWIDGET_H
#define SEARCHRESULTSWIDGET_H

#include <qwidget.h>
#include <qstringlist.h>
#include <qlist.h>
#include "kab_searchresultswidgetbase.h"

class BunchOfKeys;
class KabAPI;

class SearchResultsWidget : public SearchResultsWidgetBase
{
  Q_OBJECT
public:
  SearchResultsWidget(KabAPI *api, QWidget *parent=0, const char* name=0);
  /** Set the contents of the search results widget. Each element of
      headers needs to have an equivalent in indizes containing the
      index of the entry in the entry list(s). */
  void setContents(const QStringList& headers, const QList<int>
		   indizes);
  /** Clear the results of a previous search. Does not emit hideMe(). */
  void clear();
  /** Return if search results are available from a previous
      search. */
  bool hasResults();
  /** Get a BunchOfKeys object that represents the results. */
  bool bunchOfKeys(BunchOfKeys& keys);
protected:
  void slotHide();
  void slotClear();
  void slotPrint();
  void slotEntrySelected(int);
  // ----- 
  QStringList headlines;
  QList<int> indizes;
  KabAPI *api;
signals:
  void showMe(bool);
  void printResults();
  void setStatus(const QString&);
  void entrySelected(int);
};

class QVBoxLayout; 

#endif // SEARCHRESULTSWIDGET_H
