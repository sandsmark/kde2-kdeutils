/* -*- C++ -*-
   This file implements kab�s entry list widget..

   the KDE addressbook

   $ Author: Mirko Boehm $
   $ Copyright: (C) 1996-2000, Mirko Boehm $
   $ Contact: mirko@kde.org
         http://www.kde.org $
   $ License: GPL with the following explicit clarification:
         This code may be linked against any version of the Qt toolkit
         from Troll Tech, Norway. $

   $Revision: 111280 $
*/

#ifndef KAB_ENTRYLIST_H
#define KAB_ENTRYLIST_H

#include "kab_entrylistbase.h" // made with Qt Designer
#include <qtimer.h>
#include <kabapi.h>
#include <kdebug.h>

class QStringList;
class QTimer;
class KabAPI;

class Pattern
{
protected:
  QString mKey;
  QString mDesc;
  int mIndex;
public:
  Pattern(QString k=QString::null, QString d=QString::null, int i=-1) 
    : mKey(k), mDesc(d), mIndex(i) {}
  /** The pattern that is searched for. */
  QString key() { return mKey; }
  void setKey(const QString& k) { mKey=k; }
  /** The descriptive string displayed as a possible match. */
  QString desc() { return mDesc; }
  void setDesc(const QString& d) { mDesc=d; }
  /** The index of the hit in the database. */
  int index() { return mIndex; }
  void setIndex(int i) { mIndex=i; }
};

class PatternList : public QList<Pattern>
{
public:
  PatternList() : QList<Pattern> () {}
protected:
  int compareItems(QCollection::Item i1, QCollection::Item i2)
  {
    Pattern* item1=(Pattern*) i1;
    Pattern* item2=(Pattern*) i2;
    // ----- first compare the key:
    if(item1->key()>item2->key()) return 1;
    if(item1->key()<item2->key()) return -1;
    // ----- then the description:
    if(item1->desc()>item2->desc()) return 1;
    if(item1->desc()<item2->desc()) return -1;
    // ----- this items are equal
    return 0;
  }
};

class KabEntryList : public KabEntryListBase
{
  Q_OBJECT
public:
  KabEntryList(KabAPI *, QWidget *parent=0, const char* name=0);
  /** Update the displayed entries with the list given. Called from
      the top widget. */
  void update(QStringList*);
protected:
  /** Overloaded from base class. Find closest completion to the text the user
      entered. */
  void slotPatternChanged(const QString& pattern);
  /** Overloaded from base class. */
  void slotEntrySelected(int);
  /** Overloaded from base class. */
  void slotPatternEntered(const QString& pattern);
  /** Timer to start creating the map in the background. */
  QTimer timer;
  /** This array holds the pattern used for incremental
      search. It is always sorted alphabetically. */
  PatternList patterns;
  /** Access to the API. */
  KabAPI *api;
  /** The entries are stored temporarily when makeIndex is started in
      this list. The list is cleared each time makeIndex finished. */ 
  std::list<AddressBook::Entry> entries;
signals:
  void entrySelected(int);
  /** Emit status messages to the top level window. */
  void setStatus(const QString&);
public slots:
  /** This creates the index used for incremental search. */
  void makeIndex();
};

#endif // KAB_ENTRYLIST_H


