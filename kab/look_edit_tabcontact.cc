/* -*- C++ -*-
   This file implements the tab to edit contact information.
 
   the KDE addressbook
 
   $ Author: Mirko Boehm $
   $ Copyright: (C) 1996-2000, Mirko Boehm $
   $ Contact: mirko@kde.org
         http://www.kde.org $
   $ License: GPL with the following explicit clarification:
         This code may be linked against any version of the Qt toolkit
         from Troll Tech, Norway. $

   $Revision: 66582 $ 
*/

#include "look_edit_tabcontact.h"
#include <qpushbutton.h>
#include <qlayout.h>

#include <kmessagebox.h>
#include <klocale.h>
#include <kdebug.h>

TabContact::TabContact(KabAPI* api, QWidget *parent)
  : TabBasic(api, parent)
{
  const QString Texts[] = {
    i18n("Edit email addresses"),
    i18n("Edit talk addresses"),
    i18n("Edit URLs (internet addresses)"),
    i18n("Edit telephone numbers")
  };
  QPushButton ** buttons[] = {
    &buttonEmail, &buttonTalk, &buttonURLs, &buttonTelephone
  };
  const int Size=sizeof(Texts)/sizeof(Texts[0]);
  int count;
  // -----
  layout=new QGridLayout(this, 4, 1, 3, 2);
  if(layout==0)
    {
      KMessageBox::sorry
	(this, i18n("Out of memory."),
	 i18n("General failure."));
      ::exit(-1);
    } 
  layout->setAutoAdd(true);
  for(count=0; count<Size; ++count)
    {
      *buttons[count]=new QPushButton(Texts[count], this);
    }
  connect(buttonEmail, SIGNAL(clicked()), SLOT(editEmailAddresses()));
  connect(buttonTalk, SIGNAL(clicked()), SLOT(editTalkAddresses()));
  connect(buttonURLs, SIGNAL(clicked()), SLOT(editURLs()));
  connect(buttonTelephone, SIGNAL(clicked()), SLOT(editTelephoneNumbers()));
}

void TabContact::storeContents(AddressBook::Entry& e)
{
  e.emails=entry.emails;
  e.talk=entry.talk;
  e.URLs=entry.URLs;
  e.telephone=entry.telephone;
}

void TabContact::setContents(const AddressBook::Entry& e)
{
  entry=e;
}

void TabContact::editEmailAddresses()
{
  kdDebug() << "TabContact::editEmailAddresses" << endl;
}

void TabContact::editTalkAddresses()
{
  kdDebug() << "TabContact::editTalkAddresses" << endl;
}

void TabContact::editURLs()
{
  kdDebug() << "TabContact::editURLs" << endl;
}

void TabContact::editTelephoneNumbers()
{
  kdDebug() << "TabContact::editTelephoneNumbers" << endl;
}
#include "look_edit_tabcontact.moc"
