/* -*- C++ -*-
   This file implements the dialog that is used to set file specific
   information.
   
   the KDE addressbook

   $ Author: Mirko Boehm $
   $ Copyright: (C) 1996-2000, Mirko Boehm $
   $ Contact: mirko@kde.org
         http://www.kde.org $
   $ License: GPL with the following explicit clarification:
         This code may be linked against any version of the Qt toolkit
         from Troll Tech, Norway. $

   $Revision: 71841 $
*/

#include "dialog_configurefile.h"
#include <qtabwidget.h>
#include <qconfigDB.h>
#include <qlineedit.h>
#include <qlayout.h>
#include <qlabel.h>
#include <kdialogbase.h>
#include <kmessagebox.h>
#include <klocale.h>
#include <kabapi.h>
#include "kab_topwidget.h"
#include "widget_editstringlist.h"
#include "kab_mainwidget.h"
#include <kdebug.h>

#define KEY_ADDRESSTYPES "AddressTypes"

#ifdef KAB_KDEBUG_AREA
#undef KAB_KDEBUG_AREA
#endif

#define KAB_KDEBUG_AREA 800


void TopLevelWidget::configureFile()
{
  Section *config;
  // -----
  config=api->addressbook()->configurationSection();
  if(config==0)
    {
      KMessageBox::sorry
	(this, 
	 i18n("Cannot query configuration data."),
	 i18n("Internal failure"));
      return;
    }
  KDialogBase dialog(this);
  KabFileConfigWidget widget(config, &dialog);
  // widget.setMinimumSize(320, 200);
  dialog.showButtonApply(false);
  dialog.enableButtonSeparator(true);
  dialog.setMainWidget(&widget);
  if(dialog.exec())
    {
      emit(setStatus(i18n("Accepted.")));
      if(widget.saveSettings(config)!=AddressBook::NoError)
	{
	  emit(setStatus(i18n("Permission denied.")));
	} else {
	  emit(setStatus(i18n("Settings applied.")));
	}
      save();
      createView(mainwidget->viewType(), true);
    } else {
      emit(setStatus(i18n("Rejected.")));
    }
}

const int KabFileConfigWidget::Border=2;

KabFileConfigWidget::KabFileConfigWidget(Section* config, QWidget* parent,
					 const char* name)
  : QTabWidget(parent, name)
{
  // ###########################################################################
  kdDebug(KAB_KDEBUG_AREA) << "KabFileConfigWidget ctor: called." << endl;
  // ----- create some aggregats for the user fields tab:
  QLineEdit **ledits[]= {
    &userFieldsName, 
    &userField1, &userField2, &userField3, &userField4
  };
  QLabel **labels[]= {
    &userFieldsLabel,
    &userLabel1, &userLabel2, &userLabel3, &userLabel4
  };
  QString texts[]= {
    i18n("Tab name"),
    i18n("User field 1:"), i18n("User field 2:"),
    i18n("User field 3:"), i18n("User field 4:")
  };
  const QCString Keys[]= {
    "user_headline",
    "user_1", "user_2", "user_3", "user_4"
  };
  const int Size= sizeof(ledits)/sizeof(ledits[0]);
  int count;
  QGridLayout *layout;
  QString text;
  KeyValueMap *keys;
  QStringList strings;
  // -----
  keys=config->getKeys();
  userFields=new QWidget(this);
  layout=new QGridLayout(userFields, 4, 2, Border);
  layout->setAutoAdd(true);
  layout->setColStretch(0, 1);
  layout->setColStretch(1, 2);
  // ----- create local objects:
  if(userFields!=0)
    {
      for(count=0; count<Size; ++count)
	{
	  *labels[count]=new QLabel(texts[count], userFields);
	  text="";
	  if(!keys->get(Keys[count], text))
	    {
	      kdDebug() << "TabUser::configure: missing settings " <<
			 "in configuration section!";
	    }

          if( (0 == count) && ("(User fields)" == text) )
            text = i18n("(User fields)");
 
          else if( (1 == count) && ("(User field 1)" == text) )
            text = i18n("(User field 1)");
 
          else if( (2 == count) && ("(User field 2)" == text) )
            text = i18n("(User field 2)");
 
          else if( (3 == count) && ("(User field 3)" == text) )
            text = i18n("(User field 3)");
 
          else if( (4 == count) && ("(User field 4)" == text) )
            text = i18n("(User field 4)");                                      

	  *ledits[count]=new QLineEdit(text, userFields);
	  if(*labels[count]==0 || *ledits[count]==0)
	    {
	      KMessageBox::sorry
		(this, i18n("Out of memory."),
		 i18n("General failure."));
	      ::exit(-1);
	    }
	}
    }
  addTab(userFields, i18n("User fields"));
  // ----- add the address types tab:
  QWidget *helper=new QWidget(this);
  QHBoxLayout *hbl=new QHBoxLayout(helper, 6);
  hbl->setAutoAdd(true);
  editwidget=new StringListEditWidget(helper);
  if(!keys->get(KEY_ADDRESSTYPES, strings))
    {
      kdDebug() << "TabUser::configure: missing address types setting " <<
		 "in configuration section!";
    }
  editwidget->setStrings(strings);
  addTab(helper, i18n("Address types"));
  kdDebug(KAB_KDEBUG_AREA) << "KabFileConfigWidget ctor: done." << endl;
  // ###########################################################################
}

AddressBook::ErrorCode
KabFileConfigWidget::saveSettings(Section *config)
{
  // ###########################################################################
  KeyValueMap *keys;
  QStringList strings;
  // ----- get the keys:
  keys=config->getKeys();
  // ----- insert all values:
  editwidget->getStrings(strings);
  if(!keys->insert("user_headline", userFieldsName->text(), true) ||
     !keys->insert("user_1", userField1->text(), true) ||
     !keys->insert("user_2", userField2->text(), true) ||
     !keys->insert("user_3", userField3->text(), true) ||
     !keys->insert("user_4", userField4->text(), true) ||
     !keys->insert(KEY_ADDRESSTYPES, strings, true))
    {
      KMessageBox::information
	(this,
	 i18n("Cannot save your settings.\n"
	      "Possibly you do not have permissions to write to this file."),
	 i18n("File error"));
      return AddressBook::PermDenied;
    } else {
      return AddressBook::NoError;
    }
  // ###########################################################################
}

#include "dialog_configurefile.moc"
