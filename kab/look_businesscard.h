/* -*- C++ -*-
   This file implements the business card look.

   the KDE addressbook

   $ Author: Mirko Boehm $
   $ Copyright: (C) 1996-2000, Mirko Boehm $
   $ Contact: mirko@kde.org
         http://www.kde.org $
   $ License: GPL with the following explicit clarification:
         This code may be linked against any version of the Qt toolkit
         from Troll Tech, Norway. $

   $Revision: 66582 $	 
*/

#ifndef LOOK_BUSINESSCARD_H
#define LOOK_BUSINESSCARD_H

// the common interface for looks:
#include "look_basic.h"
#include <kurllabel.h>

class QComboBox;

/** This class implements kab�s classical business card look.
 *  Currently, there is no possibility to change the entry in this
 *  view. */
class KABBusinessCard : public KABBasicLook
{
  Q_OBJECT
public:
  /** The constructor. */
  KABBusinessCard(KabAPI*, QWidget* parent=0, const char* name=0);
  /** The virtual destructor. */
  virtual ~KABBusinessCard();
  /** Set the entry. The method is overloaded to fill the combobox
   *  displaying the headlines of the different addresses. */
  void setEntry(const AddressBook::Entry&);
protected:
  /** If tile is true, the card is displayed with a tile background,
    * if it is false, the background color is used. The background
    * color defaults to something.
    */
  bool tile;
  /** The filename of the background image. */
  QCString filename;
  /** The background color. */
  QColor bgColor;
  /** The background tile. */
  QPixmap* background;
  /** The combobox showing the different addresses. */
  QComboBox *addressCombo;
  /** The URL label for displaying the email link. */
  KURLLabel *urlEmail;
  /** The URL label for displaying the homepage link. */
  KURLLabel *urlHome;
  /** The paint event. */
  void paintEvent(QPaintEvent*);
  /** The resize event. */
  void resizeEvent(QResizeEvent*);
  /** The resize event. */
  // void resizeEvent(QResizeEvent*); // currently not handled
  /** The index of the address currently displayed. */
  int currentAddress;
public slots:
  /** Capture clicks on the mail address. */
  void mailURLClicked(const QString&);
  /** Capture clicks on the URL. */
  void homeURLClicked(const QString&);
  /** An address of this entry has been selected. */
  void addressSelected(int);
signals:
  /** The mail URL label has been clicked. */
  // void mailURLActivated(); // moved to look_basic
  /** The home page URL label has been clicked. */
  // void homeURLActivated(); // moved to look_basic
};

#endif // LOOK_BUSINESSCARD_H
