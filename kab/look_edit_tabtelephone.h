/* -*- C++ -*-
   This file implements the tab to edit the telephone numbers.

   the KDE addressbook

   $ Author: Mirko Boehm $
   $ Copyright: (C) 1996-2000, Mirko Boehm $
   $ Contact: mirko@kde.org
         http://www.kde.org $
   $ License: GPL with the following explicit clarification:
         This code may be linked against any version of the Qt toolkit
         from Troll Tech, Norway. $

   $Revision: 66582 $	
*/

#ifndef KAB_LOOK_EDIT_TABTELEPHONE_H
#define KAB_LOOK_EDIT_TABTELEPHONE_H "$ID$"

#include "look_edit_basictab.h"

class QComboBox;
class QLineEdit;
class KabAPI;

class TabTelephone : public TabBasic
{
  Q_OBJECT
public:
  TabTelephone(KabAPI*, QWidget *parent=0);
  // ~TabTelephone();
  /** Derived from TabBasic. */
  void storeContents(AddressBook::Entry& entry);
  /** Dito. */
  void setContents(const AddressBook::Entry& entry);
protected:
  QComboBox **boxes;
  QLineEdit **lineedits;
protected slots:
  /** To be connected to the combobox signals. */
  void changed(int);
  /** To be connected to the lineedit signals. */
  void changed(const QString&);
};

#endif // KAB_LOOK_EDIT_TABTELEPHONE_H
