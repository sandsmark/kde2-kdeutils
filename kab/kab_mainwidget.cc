/* -*- C++ -*-
   This file implements kab�s main widget.
   The main widget is inserted into the top level widget derived from
   KMainWindow. 
   
   the KDE addressbook

   $ Author: Mirko Boehm $
   $ Copyright: (C) 1996-2000, Mirko Boehm $
   $ Contact: mirko@kde.org
         http://www.kde.org $
   $ License: GPL with the following explicit clarification:
         This code may be linked against any version of the Qt toolkit
         from Troll Tech, Norway. $

   $Revision: 90511 $	
*/

#include "kab_mainwidget.h"
#include <kmessagebox.h>
#include <klocale.h>
#include <kdebug.h>
#include <qpushbutton.h>
#include <qlistbox.h>
#include <stdlib.h>
#include <qlayout.h>
#include <look_basic.h>
#include "look_businesscard.h"
#include "look_edit.h"
#include "kab_searchresultswidget.h"
#include "kab_entrylist.h"
#include <qwidget.h>

KabMainWidget::KabMainWidget(KabAPI * api_, QWidget *parent, const char* name)
  : QWidget(parent, name),
    view(0),
    currentView(TopLevelWidget::NoView),
    api(api_)
{
  QVBoxLayout *layout=new QVBoxLayout(this);
  layout->setAutoAdd(true);
  splHorizontal=new QSplitter(Qt::Horizontal, this);
  if(splHorizontal==0)
    {
      KMessageBox::sorry
	(this, i18n("Out of memory."),
	 i18n("General failure"));
      ::exit(-1);
    }
  // ----- the entry list:
  list=new KabEntryList(api, splHorizontal);
  splVertical=new QSplitter(Qt::Vertical, splHorizontal);
  if(splVertical==0)
    {
      KMessageBox::sorry
	(this, i18n("Out of memory."),
	 i18n("General failure"));
      ::exit(-1);
    }
  viewFrame=new QFrame(splVertical);
  QBoxLayout *viewLayout=new QBoxLayout
    (viewFrame, QBoxLayout::TopToBottom, 3);
  viewLayout->setAutoAdd(true);
  CHECK_PTR(viewFrame);
  search=new SearchResultsWidget(api, splVertical);
  // list->hide();
  search->hide();
  // ------
  connect(this, SIGNAL(showSearchResults(bool)),
	  parentWidget(), SLOT(slotShowSearchResults(bool)));
  connect(search, SIGNAL(setStatus(const QString&)), 
	  SLOT(slotSetStatus(const QString&)));
  connect(list, SIGNAL(setStatus(const QString&)), 
	  SLOT(slotSetStatus(const QString&)));
}

void KabMainWidget::createView(TopLevelWidget::View v, bool recreate)
{
  // ----- avoid deleting and creating:
  if(v==currentView && !recreate) return;
  AddressBook::Entry entry;
  // -----
  if(view!=0)
    {
      view->getEntry(entry);
      delete view;
      view=0;
    }
  // -----
  switch(v)
    {
    case TopLevelWidget::BusinessCard:
      view=new KABBusinessCard(api, viewFrame);
      break;
    case TopLevelWidget::Editing:
      view=new KABEditLook(api, viewFrame);
      connect(view, SIGNAL(entryChanged()), 
	      parentWidget(), SLOT(entryChangedSlot()));
      break;
    default:
      kdDebug() << "TopLevelWidget::createView: unknown kind of view." << endl;
      view=new KABBusinessCard(api, viewFrame);
    }
  connect(view, SIGNAL(sendEmail(const QString&)), 
	  parentWidget(), SLOT(mail(const QString&)));
  connect(view, SIGNAL(browse(const QString&)), 
	  parentWidget(), SLOT(browse(const QString&)));
  connect(view, SIGNAL(saveMe()), 
	  parentWidget(), SLOT(updateGUI()));
  view->setEntry(entry);
  view->show();
  currentView=v;
}

KABBasicLook *KabMainWidget::getView()
{
  return view;
}

TopLevelWidget::View KabMainWidget::viewType()
{
  return currentView;
}

void KabMainWidget::slotShowSearchResults(bool b)
{
  if(b)
    {
      search->show();
    } else {
      search->hide();
    }
}

void KabMainWidget::slotShowEntryList(bool b)
{
  if(b)
    {
      list->show();
    } else {
      list->hide();
    }
}

void KabMainWidget::slotHide()
{ // the actual hiding is done by the TopLevelWidget:
  kdDebug() << "KabMainWidget::slotHide: hiding search results." << endl;
  emit(showSearchResults(false));
}

SearchResultsWidget *KabMainWidget::searchresults()
{
  return search;
}

KabEntryList* KabMainWidget::entrylist()
{
  return list;
}

void KabMainWidget::slotSetStatus(const QString& t)
{
  emit(setStatus(t));
}

#include "kab_mainwidget.moc"
