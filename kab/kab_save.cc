/* -*- C++ -*-
   This file implements the method to save a file..

   the KDE addressbook

   $ Author: Mirko Boehm $
   $ Copyright: (C) 1996-2000, Mirko Boehm $
   $ Contact: mirko@kde.org
         http://www.kde.org $
   $ License: GPL with the following explicit clarification:
         This code may be linked against any version of the Qt toolkit
         from Troll Tech, Norway. $

   $Revision: 71841 $	
*/

#include <kmessagebox.h>
#include <klocale.h>
#include <kabapi.h>
#include "kab_topwidget.h"
#include "look_basic.h"
#include "kab_mainwidget.h"

void TopLevelWidget::save()
{
  // ###########################################################################
  AddressBook::Entry entry;
  // ----- save the database:
  AddressBook *book=api->addressbook();
  if(modified)
    {
      // ----- put the new entry into the database:
      //       preload it with the old contents (just in case)
      if(book->getEntry(*current, entry)!=AddressBook::NoError)
	{
	  emit(setStatus(i18n("Internal error.")));
	}
      //       get all values from the current view:
      mainwidget->getView()->getEntry(entry);
      //       and store it:
      switch(book->change(*current, entry))
	{
	case AddressBook::NoError:
	  emit(setStatus(i18n("Entry stored.")));
	  break;
	case AddressBook::PermDenied:
	  emit(setStatus(i18n("Permission denied.")));
	  return;
	default:
	  emit(setStatus(i18n("Internal error.")));
	  return;
	}
    }
  if(book->save("", true)!=AddressBook::NoError)
    {
      KMessageBox::sorry
	(this, i18n("The file could not be saved (permission denied)."),
	 i18n("kab: File error"));
    }
  // ----- finally say something and remember state:
  setStatus(i18n("File saved."));
  modified=false;
  updateGUI(); // just in case
  // ###########################################################################
}

