/* -*- C++ -*-
   This file implements kab�s main widget.
   The main widget is only a container for the entry (tree) list, the
   entry view and 

   the KDE addressbook

   $ Author: Mirko Boehm $
   $ Copyright: (C) 1996-2000, Mirko Boehm $
   $ Contact: mirko@kde.org
         http://www.kde.org $
   $ License: GPL with the following explicit clarification:
         This code may be linked against any version of the Qt toolkit
         from Troll Tech, Norway. $

   $Revision: 74665 $	
*/

#include <qlistbox.h>
#include <kdebug.h>
#include "kab_searchresultswidget.h"
#include <list>
#include <kabapi.h>
#include "kab_topwidget.h"

SearchResultsWidget::SearchResultsWidget(KabAPI * api_, QWidget *parent, 
					 const char* name)
  : SearchResultsWidgetBase(parent, name),
    api(api_)
{
}

void SearchResultsWidget::slotHide()
{
  emit(showMe(false));
}

void SearchResultsWidget::slotPrint()
{
  emit(printResults());
}

void SearchResultsWidget::slotClear()
{
  clear();
  emit(showMe(false));
}

void SearchResultsWidget::setContents(const QStringList& headlines_, 
				      const QList<int> indizes_)
{
  clear();
  headlines=headlines_;
  indizes=indizes_;
  lbResults->insertStringList(headlines);
}

void SearchResultsWidget::clear()
{
  lbResults->clear();
  headlines.clear();
  indizes.clear();
}

void SearchResultsWidget::slotEntrySelected(int item)
{
#if ! defined NDEBUG
  if((item<0 || (unsigned)item>=indizes.count())
     || 
     (indizes.count()!=headlines.count()))
    {
      kdDebug() << "SearchResultsWidget::slotEntrySelected: internal error! "
		<< "Number of elements in listbox does not match number of "
		<< "elements in indizes list." 
		<< endl;
    }
#endif
  emit(entrySelected(*indizes.at(item)));
}

bool SearchResultsWidget::hasResults()
{
  return !headlines.isEmpty();
}

bool SearchResultsWidget::bunchOfKeys(BunchOfKeys& keys)
{
  if(api==0) return false;
  KabKey key;
  // -----
  keys.clear();
  for(unsigned counter=0; counter<indizes.count(); ++counter)
    {
      if(api->addressbook()->getKey(*indizes.at(counter), key)==AddressBook::NoError)
	{
	  keys.push_back(key);
	} else {
	  kdDebug() << "SearchResultsWidget::bunchOfKeys: "
		    << "error querying entry " << counter 
		    << ", ignored." << endl;
	}
    }
  return true;
}

#include "kab_searchresultswidget.moc"

