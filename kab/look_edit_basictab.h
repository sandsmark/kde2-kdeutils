/* -*- C++ -*-
   This file implements the tab�s base class for the editing look.

   the KDE addressbook
   
   $ Author: Mirko Boehm $
   $ Copyright: (C) 1996-2000, Mirko Boehm $
   $ Contact: mirko@kde.org
         http://www.kde.org $
   $ License: GPL with the following explicit clarification:
         This code may be linked against any version of the Qt toolkit
         from Troll Tech, Norway. $

   $Revision: 66582 $	 
*/

#ifndef KAB_LOOK_EDIT_BASICTAB_H
#define KAB_LOOK_EDIT_BASICTAB_H "$ID$"

#include <qwidget.h>
#include <kabapi.h>

class KabAPI;

/* This class is used to create a common method in all tabs that
   stores all values (attributes of an entry) in an entry handed over
   by reference. This makes it much easier to add tabs or values
   handled by a tab.
*/

class TabBasic : public QWidget
{
  Q_OBJECT
public:
  TabBasic(KabAPI *api_, QWidget *parent=0, const char* name=0);
  /** Store the values in this tab into the entry.
   *  A pure virtual method that transfers the values in the tab into
   *  the entry given by reference.
   */
  virtual void storeContents(AddressBook::Entry& entry)=0;
  /** Set all needed values according to this entry. Called for all
      tabs when an entry is selected.
  */
  virtual void setContents(const AddressBook::Entry& entry)=0;
signals:
  /** Emitted when attributes in this tab are changed. */
  void changed();
protected:
  KabAPI *api;
};

#endif // defined KAB_LOOK_EDIT_BASICTAB_H
