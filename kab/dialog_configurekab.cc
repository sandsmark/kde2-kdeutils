/* -*- C++ -*-
   This file implements the dialog to configure the KDE address book.

   the KDE addressbook
   
   $ Author: Mirko Boehm $
   $ Copyright: (C) 1996-2000, Mirko Boehm $
   $ Contact: mirko@kde.org
         http://www.kde.org $
   $ License: GPL with the following explicit clarification:
         This code may be linked against any version of the Qt toolkit
         from Troll Tech, Norway. $

   $Revision: 95919 $	 
*/

#include "dialog_configurekab.h"
#include "kab_topwidget.h"
#include <qcheckbox.h>
#include <qconfigDB.h>
#include <qlabel.h>
#include <qlayout.h>
#include <kdialogbase.h>
#include <kmessagebox.h>
#include <kdebug.h>
#include <klocale.h>

void TopLevelWidget::configureKab()
{
  KDialogBase dialog(this);
  QConfigDB *db=api->addressbook()->getConfig();
  Section *config;
  if(!db->get("config", config))
    {
      KMessageBox::information
	(this,
	 i18n("Your configuration file is corrupt.\n"
	      "KAB cannot be configured."),
	     i18n("Fatal error"));
      emit(setStatus(i18n("Configuration file corrupt.")));
      return;
    }
  // ----- 
  CHECK_PTR(config);
  KabConfigWidget widget(&dialog);
  // -----
  dialog.setMainWidget(&widget);
  dialog.showButtonApply(false);
  dialog.enableButtonSeparator(true);
  widget.setSettings(config);
  if(dialog.exec())
    {
      emit(setStatus(i18n("Accepted.")));
      if(!widget.saveSettings(config))
	{
	  KMessageBox::information
	    (this,
	     i18n("There was an error saving your preferences.\n"
		  "Some of them may be lost."),
	     i18n("Error"));
	  emit(setStatus(i18n("Error saving preferences.")));
	} else {
	  if(db->save(i18n("KDE address book preferences.").local8Bit(), true))
	    {
	      emit(setStatus(i18n("Preferences saved.")));
	    } else {
	      KMessageBox::information
		(this,
		 i18n("Cannot save your preferences.\n"
		      "You may not have permission to write the file."),
		 i18n("Error"));
	      emit(setStatus(i18n("Error saving preferences.")));
	    }
	}
    } else {
      emit(setStatus(i18n("Rejected.")));
    }
  loadConfiguration();
}

KabConfigWidget::KabConfigWidget(QWidget* parent, const char* name)
  : QTabWidget(parent, name)
{
  QWidget *tabGeneral=new QWidget(this);
  QGridLayout *layout=new QGridLayout(tabGeneral, 4, 1, 2);
  layout->setAutoAdd(true);
  rememberLastView=new QCheckBox
    (i18n("Remember last selected view"), tabGeneral);
  rememberLastEntry=new QCheckBox
    (i18n("Remember last selected entry"), tabGeneral);
  showNameInTitle=new QCheckBox
    (i18n("Show assembled name in window title"), tabGeneral);
  createBackup=new QCheckBox
    (i18n("Create backup file on startup"), tabGeneral);
  addTab(tabGeneral, i18n("General settings"));
}

KabConfigWidget::~KabConfigWidget()
{
}

bool KabConfigWidget::setSettings(Section* section)
{
  bool lastView=true, lastEntry=true, nameInTitle=true, doCreateBackup=true;
  // -----
  KeyValueMap *keys=section->getKeys();
  if(!keys->get("RememberLastView", lastView))
    {
      kdDebug() << "KabConfigWidget::setSettings: "
		<< "no previous preference for RememberLastView"
		<< endl;
    }
  if(!keys->get("RememberLastEntry", lastEntry))
    {
      kdDebug() << "KabConfigWidget::setSettings: "
		<< "no previous preference for RememberLastEntry"
		<< endl;
    }
  if(!keys->get("ShowNameInWindowTitle", nameInTitle))
    {
      kdDebug() << "KabConfigWidget::setSettings: "
		<< "no previous preference for ShowNameInWindowTitle"
		<< endl;
    }
  if(!keys->get("CreateBackupOnStartup", doCreateBackup))
    {
      kdDebug() << "KabConfigWidget::setSettings: "
		<< "no previous preference for CreateBackupOnStartup"
		<< endl;
    }
  rememberLastView->setChecked(lastView);
  rememberLastEntry->setChecked(lastEntry);
  showNameInTitle->setChecked(nameInTitle);
  createBackup->setChecked(doCreateBackup);
  return true;
}

bool KabConfigWidget::saveSettings(Section* section)
{
  bool lastView=rememberLastView->isChecked(),
    lastEntry=rememberLastEntry->isChecked(),
    nameInTitle=showNameInTitle->isChecked(),
    doCreateBackup=createBackup->isChecked();
  bool rc=true;
  // -----
  KeyValueMap *keys=section->getKeys();
  if(!keys->insert("RememberLastView", lastView, true))
    {
      kdDebug() << "KabConfigWidget::saveSettings: "
		<< "cannot save preference for RememberLastView!"
		<< endl;
      rc=false;
    }
  if(!keys->insert("RememberLastEntry", lastEntry, true))
    {
      kdDebug() << "KabConfigWidget::saveSettings: "
		<< "cannot save preference for RememberLastEntry!"
		<< endl;
      rc=false;
    }
  if(!keys->insert("ShowNameInWindowTitle", nameInTitle, true))
    {
      kdDebug() << "KabConfigWidget::saveSettings: "
		<< "cannot save preference for ShowNameInWindowTitle!"
		<< endl;
      rc=false;
    }
  if(!keys->insert("CreateBackupOnStartup", doCreateBackup, true))
    {
      kdDebug() << "KabConfigWidget::saveSettings: "
		<< "cannot save preference for CreateBackupOnStartup!"
		<< endl;
      rc=false;
    }
  return rc;
}

#include "dialog_configurekab.moc"
