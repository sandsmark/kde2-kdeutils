/* -*- C++ -*-
   This file implements the base class for kab�s looks..

   the KDE addressbook

   $ Author: Mirko Boehm $
   $ Copyright: (C) 1996-2000, Mirko Boehm $
   $ Contact: mirko@kde.org
         http://www.kde.org $
   $ License: GPL with the following explicit clarification:
         This code may be linked against any version of the Qt toolkit
         from Troll Tech, Norway. $
   
   $Revision: 70632 $
*/

#include "look_basic.h"


KABBasicLook::KABBasicLook(KabAPI* api, QWidget* parent, const char* name)
  : QWidget(parent, name)
{
  db=api;
}

void
KABBasicLook::setEntry(const AddressBook::Entry& e)
{
  current=e;
  repaint(false);
}

void
KABBasicLook::getEntry(AddressBook::Entry& entry)
{
  entry=current;
}

#include "look_basic.moc"
