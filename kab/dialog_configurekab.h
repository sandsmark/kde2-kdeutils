/* -*- C++ -*-
   This file declares the dialog to configure the KDE address book.


   the KDE addressbook
   $ Author: Mirko Boehm $
   $ Copyright: (C) 1996-2000, Mirko Boehm $
   $ Contact: mirko@kde.org
         http://www.kde.org $
   $ License: GPL with the following explicit clarification:
         This code may be linked against any version of the Qt toolkit
         from Troll Tech, Norway. $

   $Revision: 66582 $	 
*/

#ifndef DIALOG_CONFIGUREKAB_H
#define DIALOG_CONFIGUREKAB_H

#include <qtabwidget.h>

class Section;
class QCheckBox;

class KabConfigWidget : public QTabWidget
{
  Q_OBJECT
public:
  /** The Qt standard constructor. Gets the open file. */
  KabConfigWidget(QWidget* parent, const char* name=0);
  /** The destructor. */
  ~KabConfigWidget();
  /** Initialize all settings from the given section. */
  bool setSettings(Section*);
  /** Store all settings to the given section. */
  bool saveSettings(Section*);
protected:
  /** Checkbox for remembering the last selected view. */
  QCheckBox *rememberLastView;
  /** Checkbox for remembering the last displayed entry. */
  QCheckBox *rememberLastEntry;
  /** Checkbox to enable or disable showing the name in the title
      bar. */
  QCheckBox *showNameInTitle;
  /** Checkbox to enable or disable backup creation on startup. */
  QCheckBox *createBackup;
};

#endif // DIALOG_CONFIGUREKAB_H

