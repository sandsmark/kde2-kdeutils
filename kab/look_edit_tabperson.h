/* -*- C++ -*-
   This file implements the tab showing personal attributes of persons.
   This file additionally implements  the comment and birthday tab,
   since they are totally simple. 
   
   the KDE addressbook

   $ Author: Mirko Boehm $
   $ Copyright: (C) 1996-2000, Mirko Boehm $
   $ Contact: mirko@kde.org
         http://www.kde.org $
   $ License: GPL with the following explicit clarification:
         This code may be linked against any version of the Qt toolkit
         from Troll Tech, Norway. $

   $Revision: 89629 $	 
*/

#ifndef KAB_LOOK_EDIT_TABPERSON_H
#define KAB_LOOK_EDIT_TABPERSON_H "$ID$"

#include "look_edit_basictab.h"
#include <qdatetime.h>

class KabAPI;
class KDatePicker;
class QLineEdit;
class QLabel;
class QGridLayout;
class QMultiLineEdit;
class QCheckBox;
class QStringList;

class TabPerson : public TabBasic
{
  Q_OBJECT
public:
  TabPerson(KabAPI*, QWidget *parent=0);
  ~TabPerson();
  /** Derived from TabBasic. */
  void storeContents(AddressBook::Entry& entry);
  /** Dito. */
  void setContents(const AddressBook::Entry& entry);
protected:
  // void resizeEvent(QResizeEvent*);
  QLineEdit *firstname;
  QLineEdit *middlename;
  QLineEdit *lastname;
  QLineEdit *fn; // the formatted name, replaces the combination of first/middle/last
  QLineEdit *nameprefix;
  QLineEdit *title;
  QLineEdit *rank;
  QLabel *l_firstname;
  QLabel *l_middlename;
  QLabel *l_lastname;
  QLabel *l_fn; // the formatted name, replaces the combination of first/middle/last
  QLabel *l_nameprefix;
  QLabel *l_title;
  QLabel *l_rank;
  QPushButton *buttonEmail;
  QPushButton *buttonTalk;
  QPushButton *buttonURLs;
  QGridLayout *layout;
  QStringList *emails;
  QStringList *talk;
  QStringList *urls;
protected slots:
  void textChangedSlot(const QString&);
  void editEmailAddresses();
  void editTalkAddresses();
  void editURLs();
signals:
  void saveMe();
};

class TabBirthday : public TabBasic
{
  Q_OBJECT
public:
  TabBirthday(KabAPI*, QWidget *parent=0);
  /** Derived from TabBasic. */
  void storeContents(AddressBook::Entry& entry);
  /** Dito. */
  void setContents(const AddressBook::Entry& entry);
protected:
  KDatePicker *datepicker;
  QCheckBox *enable;
protected slots:
  void dateChanged(QDate); 
  void stateChangedSlot(int);
};

class TabComment : public TabBasic
{
  Q_OBJECT
public:
  TabComment(KabAPI*, QWidget *parent=0);
  /** Derived from TabBasic. */
  void storeContents(AddressBook::Entry& entry);
  /** Dito. */
  void setContents(const AddressBook::Entry& entry);
protected:
  void resizeEvent(QResizeEvent*);
  QMultiLineEdit *mle;
protected slots:
  void textChangedSlot();
};

#endif // KAB_LOOK_EDIT_TABPERSON_H
