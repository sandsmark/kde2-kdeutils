/* -*- C++ -*-
   This file implements the tab�s base class for the editing look.
   
   the KDE addressbook
   
   $ Author: Mirko Boehm $
   $ Copyright: (C) 1996-2000, Mirko Boehm $
   $ Contact: mirko@kde.org
         http://www.kde.org $
   $ License: GPL with the following explicit clarification:
         This code may be linked against any version of the Qt toolkit
         from Troll Tech, Norway. $

   $Revision: 66582 $
*/

#include <kabapi.h>
#include "look_edit_basictab.h"

TabBasic::TabBasic(KabAPI *api_, QWidget *parent, const char* name)
  : QWidget(parent, name),
    api(api_)
{
}
#include "look_edit_basictab.moc"
