/* -*- C++ -*-
   This file implements the test for the kabapi. You have to "make check"
   to build it.

   the KDE addressbook

   $ Author: Mirko Boehm $
   $ Copyright: (C) 1996-2000, Mirko Boehm $
   $ Contact: mirko@kde.org
         http://www.kde.org $
   $ License: GPL with the following explicit clarification:
         This code may be linked against any version of the Qt toolkit
         from Troll Tech, Norway. $

   $Revision: 89843 $
*/

#include <qimage.h>
#include <kabapi.h>
#include <iostream.h>
#include <kapp.h>
#include <klocale.h>
#include <kmessagebox.h>
#include <kcmdlineargs.h>
#include <kdebug.h>
#include <qvariant.h>
#include <list>

static const char *description = 
	I18N_NOOP("Test program");

static const char *version = "v0.0.1";

int main(int argc, char** argv)
{
  // ############################################################################
  KCmdLineArgs::init(argc, argv, "kabapi_test", description, version);
  
  KApplication app;

  KabAPI api;
  
  list<AddressBook::Entry> entries;

  AddressBook::ErrorCode ec;

  QVariant var;
  // ----- <test>:
  if(api.init()!=AddressBook::NoError)
    {
      KMessageBox::information
	(0, i18n("Error initializing addressbook API."),
	 i18n("Error"));
      return -1;
    }
  if(api.exec())
    {
      QString name;
      QString text;
      AddressBook::Entry entry;
      KabKey key;
      // -----
      kdDebug() << "kabapi_test: test accepted." << endl;
      switch(api.getEntry(entry, key))
	{
	case AddressBook::NoError:
	  api.addressbook()->literalName(entry, name, true, false);
	  text=QString("KabAPI test: entry is ") + name;
	  // ----- check key-fieldname lookup:
	  kdDebug() << "kabapi_test: listing class Entry field names "
		    << "and translations." << endl;
	  for(int counter=0; 
	      counter<AddressBook::Entry::NoOfFields;
	      ++counter)
	    {
	      if(AddressBook::Entry::nameOfField
		 (AddressBook::Entry::Fields[counter], name))
		{
		  kdDebug() << "kabapi_test: name of field "
			    << AddressBook::Entry::Fields[counter]
			    << " is " << name << endl;
		} else {
		  kdDebug() << "key-fieldname lookup is broken" << endl;
		}
	    }
	  kdDebug() << "kabapi_test: checking key-based field lookup for Entry."
		    << endl;
	  for(int counter=0; 
	      counter<AddressBook::Entry::NoOfFields;
	      ++counter)
	    {
	      if(entry.get(AddressBook::Entry::Fields[counter], var)
		 ==AddressBook::NoError)
		{
		  kdDebug() << "kabapi_test: " 
			    << AddressBook::Entry::Fields[counter]
			    << ": " << var.asString() << endl;
		} else {
		  kdDebug() << "kabapi_test: " 
			    << AddressBook::Entry::Fields[counter]
			    << " is no defined field name." << endl;
		}
	    } 
	  kdDebug() << "kabapi_test: listing class Entry::Address field names "
		    << "and translations." << endl;
	  for(int counter=0; 
	      counter<AddressBook::Entry::Address::NoOfFields;
	      ++counter)
	    {
	      if(AddressBook::Entry::Address::nameOfField
		 (AddressBook::Entry::Address::Fields[counter], name))
		{
		  kdDebug() << "kabapi_test: name of field "
			    << AddressBook::Entry::Address::Fields[counter]
			    << " is " << name << endl;
		} else {
		  kdDebug() << "key-fieldname lookup is broken" << endl;
		}
	    }
	  kdDebug() << "kabapi_test: checking key-based field lookup for Entry::Address."
		    << endl;
	  for(int add=0; add<entry.noOfAddresses(); ++add)
	    {
	      AddressBook::Entry::Address address;
	      // -----
	      if(entry.getAddress(add, address)==AddressBook::NoError)
		{
		  for(int counter=0; 
		      counter<AddressBook::Entry::Address::NoOfFields;
		      ++counter)
		    {
		      if(address.get(AddressBook::Entry::Address::Fields[counter], var)
			 ==AddressBook::NoError)
			{
			  kdDebug() << "kabapi_test: " 
				    << AddressBook::Entry::Address::Fields[counter]
				    << ": " << var.asString() << endl;
			} else {
			  kdDebug() << "kabapi_test: " 
				    << AddressBook::Entry::Address::Fields[counter]
				    << " is no defined field name." << endl;
			}
		    }
		}
	    }
	  break;
	case AddressBook::NoEntry:
	  text="KabAPI test: no entries.";
	  break;
	default:
	  text="Internal error.";
	}
      KMessageBox::information(0, text, "Selection");
    } else {
      kdDebug() << "kabapi_test: test rejected." << endl;
    }
  // ----- test getEntries:
  ec=api.getEntries(entries);
  switch(ec)
    {
    case AddressBook::NoError:
      kdDebug() << "kabapi_test: KabAPI::getEntries returned "
		<< entries.size() << " entries." << endl;
      break;
    case AddressBook::NoEntry:
      kdDebug() << "kabapi_test: KabAPI::getEntries returned NoEntries."
		<< endl;
      break;
    case AddressBook::InternError:
    default:
      kdDebug() << "kabapi_test: KabAPI::getEntries returned "
		<< "an internal error." << endl;
    }
  // ----- </test>
  return 0;
  // ############################################################################
}


