/* -*- C++ -*-
   This file declares the dialog that is used to set file specific
   information.
 
   the KDE addressbook

   $ Author: Mirko Boehm $
   $ Copyright: (C) 1996-2000, Mirko Boehm $
   $ Contact: mirko@kde.org
         http://www.kde.org $
   $ License: GPL with the following explicit clarification:
         This code may be linked against any version of the Qt toolkit
         from Troll Tech, Norway. $

   $Revision: 70632 $	 
*/
  
#ifndef DIALOG_CONFIGUREFILE_H
#define DIALOG_CONFIGUREFILE_H

#include <addressbook.h> // we use the ErrorCodes here
#include <qtabwidget.h>

class Section;
class QLineEdit;
class QLabel;
class StringListEditWidget;

/** This class is used in the dialog where the user may configure the file
 *  specific settings for the loaded kab address database.
 *  It is used as a \e "main widget"  for a DialogBase object. */

class KabFileConfigWidget : public QTabWidget
{
  // ############################################################################
  Q_OBJECT
  // ----------------------------------------------------------------------------
public:
  /** The Qt standard constructor. Gets the configuration section of
      the currently open file. */
  KabFileConfigWidget(Section* config, QWidget* parent, const char* name=0);
  /** Save the settings to the file. */
  AddressBook::ErrorCode saveSettings(Section*); 
  // ----------------------------------------------------------------------------
protected:
  /** Tab for configuring the user fields. */
  QWidget *userFields; 
  /** The name of the first user field. */
  QLineEdit *userFieldsName; 
  /** The label for the first user field. */
  QLabel *userFieldsLabel;
  /** The name of the first user field. */
  QLineEdit *userField1; 
  /** The label for the first user field. */
  QLabel *userLabel1;
  /** The name of the second user field. */
  QLineEdit *userField2;
  /** The label for the second user field. */
  QLabel *userLabel2; 
  /** The name of the third user field. */
  QLineEdit *userField3; 
  /** The label for the third user field. */
  QLabel *userLabel3; 
  /** The name of the fourth user field. */
  QLineEdit *userField4; 
  /** The label for the fourth user field. */
  QLabel *userLabel4; 
  /** The border width inside the dialog. */
  static const int Border;
  /** A StringListEditWidget for editing the kinds of addresses. It is used as
   *  a tab. */
  StringListEditWidget *editwidget;
  // ############################################################################
};



#endif // DIALOG_CONFIGUREFILE_H
