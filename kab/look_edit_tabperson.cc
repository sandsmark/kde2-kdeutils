/* -*- C++ -*-
   This file implements the tab showing personal attributes of persons.
 
   the KDE addressbook
 
   $ Author: Mirko Boehm $
   $ Copyright: (C) 1996-2000, Mirko Boehm $
   $ Contact: mirko@kde.org
         http://www.kde.org $
   $ License: GPL with the following explicit clarification:
         This code may be linked against any version of the Qt toolkit
         from Troll Tech, Norway. $
   $Revision: 156296 $	  
*/

#include "look_edit_basictab.h"
#include "look_edit_tabperson.h"
#include "widget_editstringlist.h"
#include <kdatepik.h>
#include <kmessagebox.h>
#include <klocale.h>
#include <qlineedit.h>
#include <qmultilineedit.h>
#include <qlabel.h>
#include <qlayout.h>
#include <qstringlist.h>
#include <qcheckbox.h>
#include <qtooltip.h>
#include <kdebug.h>

TabPerson::TabPerson(KabAPI *api, QWidget *parent)
  : TabBasic(api, parent)    
{
  QWidget *helper;
  QHBoxLayout *hlay;
  const int NoOfCols=3;
  int row=0;
  QHBoxLayout *hboxbuttons;
  const QString Texts[] = {
    i18n("Email addresses..."),
    i18n("Talk addresses..."),
    i18n("URLs...")
  };
  QPushButton ** buttons[] = {
    &buttonEmail, &buttonTalk, &buttonURLs
  };
  const int Size=sizeof(Texts)/sizeof(Texts[0]);
  int count;
  // -----
  emails=new QStringList;
  talk=new QStringList;
  urls=new QStringList;
  // -----
  layout=new QGridLayout(this, 8, NoOfCols, 3, 2);
  if(layout==0)
    {
      KMessageBox::sorry
	(this, i18n("Out of memory."),
	 i18n("General failure."));
      ::exit(-1);
    }
  layout->setRowStretch(7, 1);
  // ----- first add formatted name field:
  row=0;
  l_fn=new QLabel(i18n("Formatted name:"), this);
  helper=new QWidget(this);
  fn=new QLineEdit(helper);
  QToolTip::add(fn, i18n("<qt>Enter the <i>full name</i> of the person or organization here"
			 " if <b>kab</b> assembles it in the wrong way.</qt>")); 
  layout->addWidget(l_fn, row, 0);
  connect(fn, SIGNAL(textChanged(const QString&)),
	  SLOT(textChangedSlot(const QString&)));
  hlay = new QHBoxLayout( helper, 0, 0);
  hlay->addWidget(fn);
  layout->addMultiCellWidget(helper, row, row, 1, 2);
  ++row;
  // ----- add first, middle and last name:
  l_firstname=new QLabel(i18n("First name:"), this);
  layout->addWidget(l_firstname, row, 0);
  l_middlename=new QLabel(i18n("Middle name:"), this);
  layout->addWidget(l_middlename, row, 1);
  l_lastname=new QLabel(i18n("Last name:"), this);
  layout->addWidget(l_lastname, row, 2);
  ++row;
  firstname=new QLineEdit(this);
  layout->addWidget(firstname, row, 0);
  connect(firstname, SIGNAL(textChanged(const QString&)),
	  SLOT(textChangedSlot(const QString&)));
  middlename=new QLineEdit(this);
  layout->addWidget(middlename, row, 1);
  connect(middlename, SIGNAL(textChanged(const QString&)),
	  SLOT(textChangedSlot(const QString&)));
  lastname=new QLineEdit(this);
  layout->addWidget(lastname, row, 2);
  connect(lastname, SIGNAL(textChanged(const QString&)),
	  SLOT(textChangedSlot(const QString&)));
  ++row;
  // ----- name prefix, title, rank:
  l_nameprefix=new QLabel(i18n("Name prefix:"), this);
  layout->addWidget(l_nameprefix, row, 0);
  l_title=new QLabel(i18n("Title:"), this);
  layout->addWidget(l_title, row, 1);
  l_rank=new QLabel(i18n("Rank:"), this);
  layout->addWidget(l_rank, row, 2);
  ++row;
  nameprefix=new QLineEdit(this);
  connect(nameprefix, SIGNAL(textChanged(const QString&)),
	  SLOT(textChangedSlot(const QString&)));
  layout->addWidget(nameprefix, row, 0);
  title=new QLineEdit(this);
  connect(title, SIGNAL(textChanged(const QString&)),
	  SLOT(textChangedSlot(const QString&)));
  layout->addWidget(title, row, 1);
  rank=new QLineEdit(this);
  connect(rank, SIGNAL(textChanged(const QString&)),
	  SLOT(textChangedSlot(const QString&)));
  layout->addWidget(rank, row, 2);
  ++row;
  // ----- button row on bottom:
  layout->addRowSpacing(row, 12);
  ++row; // one empty row
  helper=new QWidget(this);
  hboxbuttons=new QHBoxLayout(helper, 0, 2);
  for(count=0; count<Size; ++count)
    {
      *buttons[count]=new QPushButton(Texts[count], helper);
      hboxbuttons->addWidget(*buttons[count]);
    }
  // hboxbuttons->addStretch();
  // hboxbuttons->setDirection(QBoxLayout::LeftToRight);
  layout->addMultiCellWidget(helper, row, row, 0, 2, AlignCenter);
  connect(buttonEmail, SIGNAL(clicked()), SLOT(editEmailAddresses()));
  connect(buttonTalk, SIGNAL(clicked()), SLOT(editTalkAddresses()));
  connect(buttonURLs, SIGNAL(clicked()), SLOT(editURLs()));
}

TabPerson::~TabPerson()
{
  delete emails;
  delete talk;
  delete urls;
}

void TabPerson::textChangedSlot(const QString&)
{
  emit(changed());
}

void TabPerson::storeContents(AddressBook::Entry& entry)
{
  QLineEdit **lineedits[]= {
    &firstname, &middlename, &lastname,
    &fn, &nameprefix, &title, &rank 
  };
  QString *texts[]= {
    &entry.firstname, &entry.middlename, &entry.lastname,
    &entry.fn, &entry.nameprefix, &entry.title, &entry.rank
  };
  const int Size=sizeof(lineedits)/sizeof(lineedits[0]);
  int count;
  // -----
  for(count=0; count<Size; ++count)
    {
      *texts[count]=(*lineedits[count])->text();
    }
  entry.emails=*emails;
  entry.talk=*talk;
  entry.URLs=*urls;
}

void TabPerson::setContents(const AddressBook::Entry& entry)
{
  QLineEdit **lineedits[]= {
    &firstname, &middlename, &lastname,
    &fn, &nameprefix, &title, &rank 
  };
  QString const *texts[]= {
    &entry.firstname, &entry.middlename, &entry.lastname,
    &entry.fn, &entry.nameprefix, &entry.title, &entry.rank
  };
  const int Size=sizeof(lineedits)/sizeof(lineedits[0]);
  int count;
  // -----
  for(count=0; count<Size; ++count)
    {
      (*lineedits[count])->setText(*texts[count]);
    }
  *emails=entry.emails;
  *talk=entry.talk;
  *urls=entry.URLs;
}

void TabPerson::editEmailAddresses()
{ // a nice example of the use of KDialogBase with user defined main widgets:
  // make a dialog with ok and cancel button:
  KDialogBase dialog(this, "", true, i18n("Edit email addresses"),
		     KDialogBase::Ok|KDialogBase::Cancel,
		     KDialogBase::Ok, true); 
  StringListEditWidget w(&dialog); // make it�s main widget
  w.setStrings(*emails); // add values to it
  dialog.setMainWidget(&w); // make it known
  if(dialog.exec()) // and execute it
    {
      w.getStrings(*emails);
      emit(changed());
      emit(saveMe());
    }
}

void TabPerson::editTalkAddresses()
{
  KDialogBase dialog(this, "", true, i18n("Edit talk addresses"),
		     KDialogBase::Ok|KDialogBase::Cancel,
		     KDialogBase::Ok, true); 
  StringListEditWidget w(&dialog);
  w.setStrings(*talk);
  dialog.setMainWidget(&w);
  if(dialog.exec())
    {
      w.getStrings(*talk);
      emit(changed());
      emit(saveMe());
    }
}

void TabPerson::editURLs()
{
  KDialogBase dialog(this, "", true, i18n("Edit URLs"),
		     KDialogBase::Ok|KDialogBase::Cancel,
		     KDialogBase::Ok, true); 
  StringListEditWidget w(&dialog);
  w.setStrings(*urls);
  dialog.setMainWidget(&w);
  if(dialog.exec())
    {
      emit(changed());
      w.getStrings(*urls );
    }
}

TabBirthday::TabBirthday(KabAPI* api, QWidget *parent)
  : TabBasic(api, parent)
{
  QVBoxLayout *layout=new QVBoxLayout(this, 3, 3);
  layout->setAutoAdd(true);
  enable=new QCheckBox(i18n("I know this person's birthday:"), this);
  datepicker=new KDatePicker(this);
  datepicker->setEnabled(false);
  connect(enable, SIGNAL(toggled(bool)), datepicker, SLOT(setEnabled(bool)));
  connect(enable, SIGNAL(stateChanged(int)), SLOT(stateChangedSlot(int)));
  if(datepicker==0)
    {
      KMessageBox::sorry
	(this, i18n("Out of memory."),
	 i18n("General failure."));
      ::exit(-1);
    }
  connect(datepicker, SIGNAL(dateSelected(QDate)), SLOT(dateChanged(QDate)));
  connect(datepicker, SIGNAL(dateEntered(QDate)), SLOT(dateChanged(QDate)));
}

void TabBirthday::storeContents(AddressBook::Entry& entry)
{
  QDate date;
  // -----
  if(enable->isChecked())
    {
      entry.birthday=datepicker->getDate();
    } else {
      entry.birthday=date;
    }
}

void TabBirthday::setContents(const AddressBook::Entry& entry)
{
  if(entry.birthday.isValid())
    {
      enable->setChecked(true);
      datepicker->setDate(entry.birthday);
    } else {
      enable->setChecked(false);
    }
}

void TabBirthday::dateChanged(QDate d)
{
  kdDebug() << "TabBirthday::dateChanged: birthday changed." << endl;
  datepicker->setDate(d);  
  emit(changed());
}

void TabBirthday::stateChangedSlot(int)
{
  emit(changed());
}

TabComment::TabComment(KabAPI* api, QWidget *parent)
  : TabBasic(api, parent)
{
  mle=new QMultiLineEdit(this);
  if(mle==0)
    {
      KMessageBox::sorry
	(this, i18n("Out of memory."),
	 i18n("General failure."));
      ::exit(-1);
    }
  connect(mle, SIGNAL(textChanged()), SLOT(textChangedSlot()));
}

void TabComment::textChangedSlot()
{
  kdDebug() << "TabComment::textChangedSlot: comment changed." << endl;
  emit(changed());
}

void TabComment::resizeEvent(QResizeEvent*)
{
  const int Grid=3;
  // -----
  mle->setGeometry(Grid, Grid, width()-2*Grid, height()-2*Grid);
}

void TabComment::storeContents(AddressBook::Entry& entry)
{
  entry.comment=mle->text();
}

void TabComment::setContents(const AddressBook::Entry& entry)
{
  mle->setText(entry.comment);
}


#include "look_edit_tabperson.moc"
