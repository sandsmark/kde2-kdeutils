/* -*- C++ -*-
   This file implements kab�s toplevel widget..

   the KDE addressbook

   $ Author: Mirko Boehm $
   $ Copyright: (C) 1996-2000, Mirko Boehm $
   $ Contact: mirko@kde.org
         http://www.kde.org $
   $ License: GPL with the following explicit clarification:
         This code may be linked against any version of the Qt toolkit
         from Troll Tech, Norway. $

   $Revision: 94391 $
*/

#ifndef KAB_TOPLEVEL_WIDGET_H
#define KAB_TOPLEVEL_WIDGET_H

#include <kmainwindow.h>
#include <kabapi.h>

class KAction;
class KToggleAction;
class KABBasicLook;
class KDataNavigator;
class QPopupMenu;
class QTimer;
class QStringList;
class Section;
class KabMainWidget;
class QPainter;
class KPrinter;

// a list of KabKeys (list<KabKey>):
class BunchOfKeys : public std::list<KabKey> {};

class TopLevelWidget : public KMainWindow
{
  Q_OBJECT
public:
  /** The indices of the statusbar parts. */
  enum StatusbarIndex { Text=1, /**< The status text. */
			Number /**< The number of entries. */
  };
  /** This descriptors are used to create the view. */
  enum View { NoView=0,
	      BusinessCard,
	      Editing,
	      NoOfViews // not a real view
  };
  enum ActionIDs { NoAction=0,
		   Entries
  };
  /** The constructor. */
  TopLevelWidget();
  /** The destrcutor. */
  ~TopLevelWidget();
  /** Clean up the program before main ends. This was called
      destroyMe() before, but somebody removed this method :-)
      @return false if something goes wrong. */
  bool cleanup();
protected:
  /** Prepare the actions of this application. */
  void makeActions();
  /** Construct the menubar. */
  void makeMenu();
  /** Construct the toolbar. */
  void makeToolbar();
  /** Construct the statusbar. */
  void makeStatusbar();
  /** Create the interface to the address database. */
  bool initializeInterface();
  /** Handle window closing correctly. */
  bool queryClose();
  /** Create the view. This method reads the latest view
      chosen by the user and restores it if no view is given.
  */
  void createView(View=NoView, bool recreate=false);
  /** The interface to the AddressBook object. */
  KabAPI *api;
  /** A pointer to a key storing the entry currently displayed.
   *  This property is set by the entrySelected method.
   *  @see entrySelected
   */
  KabKey *current;
  /** The main widget. It manages the different kab main controls. */
  KabMainWidget *mainwidget;
  /** The index of the current entry in the combobox. */
  int currentIndex;
  /** Some flags to adapt the GUI to. */
  // bool hasEmailAddresses, hasURLs;
  /** The pointer to the file menu. */
  QPopupMenu *file;
  /** The pointer to the print menu. */
  QPopupMenu *print;
  /** The pointer to the import menu. */
  QPopupMenu *import;
  /** The pointer to the edit menu. */
  QPopupMenu *edit;
  /** The pointer to the view menu. */
  QPopupMenu *menuview;
  /** The action that activates exporting to HTML from the
      menu/toolbar (not from within the search results window). */
  KAction *actionExportHTML;
  /** Action "first entry. */
  KAction *actionFirstEntry;
  /** Action "previous entry". */
  KAction *actionPreviousEntry;
  /** Action "next entry". */
  KAction *actionNextEntry;
  /** Action "last entry". */
  KAction *actionLastEntry;
  /** Action "new entry". */
  KAction *actionNewEntry;
  /** Action "edit entry". */
  KAction *actionEditEntry;
  /** Action "delete entry". */
  KAction *actionDeleteEntry;
  /** Action "send email". */
  KAction *actionSendEmail;
  /** Action "browse url". */
  KAction *actionBrowse;
  /** Action "search entries" */
  KAction *actionSearchEntries;
  /** Toggleaction "show search results". */
  KToggleAction *actionShowSearchResults;
  /** Toggleaction "show main toolbar". */
  KToggleAction *actionShowMainToolbar;
  /** Toggleaction "show entry navigation toolbar". */
  KToggleAction *actionShowEntryNavToolbar;
  /** Toggleaction "show entry list" */
  KToggleAction *actionShowEntryList;
  /** Action "print this entry" */
  KAction *actionPrintEntry;
  /** Action "print all entries". */
  KAction *actionPrintEntries;
  /** Action "select editing view". */
  KToggleAction *actionSelectViewEdit;
  /** Action "select business card view". */
  KToggleAction *actionSelectViewBC;
  /** Action "save database to file" */
  KAction *actionSave;
  /** Used to reset the status bar. */
  QTimer *timer;
  /** The list containing the status messages to display. */
  QStringList *messages;
  /** If true, the entry needs to be saved before showing another
      one, closing the application etc. */
  bool modified;
  /** Signals that the "Quit" button has been selected. */
  bool closingdown;
  /** Edit the current entry in a dialog. */
  bool editCurrentEntry();
  /** Open the edit dialog to modify the given entry. */
  bool editEntry(AddressBook::Entry& entry);
  /** Remove this entry. If quiet is true, the entry is removed
      silently even if query-on-delete is set. Use it for deleting
      temporaries. Careful!
      @see modified */
  bool removeCurrentEntry(bool quiet=false);
  /** Test: */
  void writeConfiguration();
  //       the strange notation may be obsolete when using actions
  /** Make the entry with the given index the active one. */
  bool activateEntry(int index);
  /** Handle the key events that need to be. */
  void keyPressEvent(QKeyEvent *e);
  /** Catch show events. */
  void showEvent(QShowEvent* e);
  /** The pointer to the main widget. Earlier, there has been a view frame. */
  KabMainWidget *main;
  /** Paint one entry using the given painter. May not only be used on
      printer objects...
      If fake is true, the method does not actually paint something,
      but just calculates the space needed to draw this entry onto the
      given window.
      top is the starting pixel in vertical direction (coordinate
      system origin id the upper left corner), it is zero by default,
      but may be larger than this to place another entry below an
      already printed one.
      @return false if something failed, else true */
  bool printEntry(const AddressBook::Entry&,
		  const QRect& window,
		  QPainter *, int top=0, bool fake=false, QRect
		  *rect=0);
  /** This paints all the entries in keys, in the order they are
      given. Uses the whole page, may be more than one :-). Gets a
      painter and a window in it to work on. */
  bool printEntries(const BunchOfKeys keys,
		    KPrinter *,
		    QPainter *,
		    const QRect& window);
  /** This paints all the entries in keys, in the order they are
      given. Uses the whole page, may be more than one :-).
      This version creates a painter it prints on. */
  bool printEntries(const BunchOfKeys keys);
public slots:
  /** An entry has been selected. */
  void entrySelected(int);
  /** The entries map has changed. Possibly the database, too...
      Update all internal data structures that contain data from the
      interface. */
  void entriesChanged();
  // The file menu:
  /** Create a new address database. Triggered from the menu. */
  void createNew();
  /** Load the standard database. This is the predefined user database
   *  created on the first start of the program. Note that the user might
   *  configure this filename!. Triggered from the menu. */
  void loadDefaultDatabase();
  /** Load a database file. With kab2, the filename is no more fixed.
   *  Triggered from the menu. */
  void loadDatabaseFile();
  /** Save the database. Clears modified.
      @see modified */
  void save();
  /** Import kab 1 addressbook. This is triggered from the menu. */
  void importKab1Addressbook();
  /** Import netscape ldif-file. This is triggered from the menu. */
  void importNetscape_ldif();
  /** Set the status bar text. The status bar is implemented in a way
      that it cycles through messages that follow quickly on each
      other in a delayed manner. */
  void setStatus(const QString&);
  /** Erase it after some time. */
  void statusbarTimeOut();
  /** Add an entry. */
  void add();
  /** Edit an entry. */
  void editEntrySlot();
  /** Remove an entry. */
  void remove();
  /** Send an email. Calls mail(const QString& url). Just for convenience. */
  void mail();
  /** Send an email. */
  void mail(const QString& url);
  /** Browse the homepage. Calls browse(const QString& url).
      Just for convenience. */
  void browse();
  /** Browse the homepage. */
  void browse(const QString& url);
  /** Enable all messages disabled by the "do not show again" button. */
  void enableAllMessages();
  /** Configure settings local to the file. User field names, for example. */
  void configureFile();
  /** Configure settings for the application. */
  void configureKab();
  /** The currently displayed entry has been changed by the user. We
      will store this in a boolean value, and (according to a
      configuration setting) ask the user to save her changes before
      displaying another entry. */
  void entryChangedSlot();
  /** Update the user interface, actions etc. This slot is called when
      changes on the current entry are accepted or the database is
      changed another way. */
  void updateGUI();
  /** Quit the application. */
  void quit();
  /** About-kab-slot. Overloaded from KTMainWindow. */
  void showAboutApplication(void);
  /** Enable or disable menu items etc. */
  // void enableButtons(); has been unified with updateGUI
  /** Load configuration. On startup, or if it changed. */
  void loadConfiguration();
  /** Export the whole database to a HTML file. */
  void exportHTML();
  /** Export the given list of AddressBook::Entry objects to a HTML file. */
  void exportHTML(const BunchOfKeys&);
  /** Slot for action "First entry". */
  void slotFirstEntry();
  /** Slot for action "Previous entry". */
  void slotPreviousEntry();
  /** Slot for action "Next entry". */
  void slotNextEntry();
  /** Slot for action "Last entry". */
  void slotLastEntry();
  /** Slot to start searches on entries. Creates a BunchOfKeys object.  */
  void slotSearchEntries();
  /** Slot to hide the search results. Reacts on clicks on the button in the
      SearchResultsWindow. */
  void slotShowSearchResults(bool);
  /** Activate the entry at index. */
  void slotActivateEntry(int index);
  /** Show or hide the main tool bar. */
  void slotShowMainToolbar(bool);
  /** Show or hide the entry navigation tool bar. */
  void slotShowEntryNavToolbar(bool);
  /** Show or hide the entry navigation list. */
  // void slotShowEntryList(bool);
  /** Slot to print one entry. */
  void slotPrintEntry();
  /** Slot to print all entries. */
  void slotPrintEntries();
  /** Slot to print the search results. Connected to
      MainWidget->SearchResults->print. */
  void slotPrintSearchResults();
  /** The business card view has been selected. */
  void slotSelectViewBC(bool);
  /** The editing view has been selected. */
  void slotSelectViewEdit(bool);
signals:
  /** Called after changes in the database that the AddressBook object
   *  does not recognize automatically. The database might be changed
   by code in the addressbook without activating the signals otherwise
   emitted by the interface.
   */
  void databaseChanged();
protected:
  // Everything following here is read from the global configuration
  // and updated if it changes.
  bool showAssembledNameInWindowTitle;
  bool rememberLastView;
  bool rememberLastEntry;
  bool createBackupOnStartup;
  bool showEntryList;
};

#endif // KAB_TOPLEVEL_WIDGET_H
