/* -*- C++ -*-
   This file implements the importing tool for kab 1 databases.

   the KDE addressbook

   $ Author: Mirko Boehm $
   $ Copyright: (C) 1996-2000, Mirko Boehm $
   $ Contact: mirko@kde.org
         http://www.kde.org $
   $ License: GPL with the following explicit clarification:
         This code may be linked against any version of the Qt toolkit
         from Troll Tech, Norway. $

   $Revision: 66582 $	 
*/

#ifndef KAB_KAB1IMPORTER_H
#define KAB_KAB1IMPORTER_H

#include <kdialogbase.h>
// #include <kabapi.h>
class KabAPI;

class QMultiLineEdit;
class KabAPI;

class Kab1Importer : public KDialogBase
{
  Q_OBJECT
public:
  Kab1Importer(KabAPI*, QWidget* parent=0);
  int exec();
protected:
  bool importKab1Addressbook();
  QMultiLineEdit *log;
  KabAPI *api;
  signals:
  void setStatus(const QString&);
};

#endif // KAB_TOPLEVEL_WIDGET_H
