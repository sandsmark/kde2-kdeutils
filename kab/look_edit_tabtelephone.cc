
/* -*- C++ -*-
   This file implements the tab for editing the telephone numbers of a person.

   the KDE addressbook
   $ Author: Mirko Boehm $
   $ Copyright: (C) 1996-2000, Mirko Boehm $
   $ Contact: mirko@kde.org
         http://www.kde.org $
   $ License: GPL with the following explicit clarification:
         This code may be linked against any version of the Qt toolkit
         from Troll Tech, Norway. $

   $Revision: 66582 $	 
*/

#include "look_edit_tabtelephone.h"
#include <addressbook.h>
#include <klocale.h>
#include <kdebug.h>
#include <qlabel.h>
#include <qframe.h>
#include <qcombobox.h>
#include <qlineedit.h>
#include <qlayout.h>
#include <qstringlist.h>

#ifdef NoOfFields
#undef NoOfFields
#endif

#define NoOfFields AddressBook::NoOfTelephoneTypes-1
#define KAB_KDEBUG_AREA 800

TabTelephone::TabTelephone(KabAPI* api, QWidget *parent)
  : TabBasic(api, parent)    
{
  // const int NoOfFields=AddressBook::NoOfTelephoneTypes-1;
  const int NoOfColumns=2;
  const int NoOfRows=NoOfFields+3;
  const int Border=3;
  const int Space=2;
  const int Col1=0;
  const int Col2=1;
  QString texts[] = {
    i18n("Fixed:"),
    i18n("Mobile:"),
    i18n("Fax:"),
    i18n("Modem:"),
    i18n("User defined 1:"),
    i18n("User defined 2:"),
    i18n("User defined 3:")
  };
  QStringList strings;
  int row=0;
  unsigned int count;
  // -----
  for(count=0; count<sizeof(texts)/sizeof(texts[0]); ++count)
    {
      strings.append(texts[count]);
    }
  QGridLayout* layout=new QGridLayout(this, NoOfRows, NoOfColumns, Border, Space);
  layout->setRowStretch(NoOfRows-1, 1);
  QLabel *header1=new QLabel(i18n("Type"), this);
  layout->addWidget(header1, row, Col1);
  QLabel *header2=new QLabel(i18n("Number"), this);
  layout->addWidget(header2, row, Col2);
  ++row;
  QFrame *f=new QFrame(this);
  f->setFrameStyle(QFrame::HLine|QFrame::Sunken);
  layout->addMultiCellWidget(f, row, row, 0, 2);
  ++row;
  boxes=new QComboBox*[NoOfFields];
  lineedits=new QLineEdit*[NoOfFields];
  for(count=0; count<NoOfFields; ++count)
    {
      boxes[count]=new QComboBox(false, this);
      boxes[count]->insertStringList(strings);
      layout->addWidget(boxes[count], row+count, Col1);
      connect(boxes[count], SIGNAL(activated(int)), SLOT(changed(int)));
      lineedits[count]=new QLineEdit(this);
      layout->addWidget(lineedits[count], row+count, Col2);
      connect(lineedits[count], SIGNAL(textChanged(const QString&)),
	      SLOT(changed(const QString&)));
    }
}

void TabTelephone::storeContents(AddressBook::Entry& entry)
{ // maybe remove "values" and store in entry.telephone directly?
  QStringList values;
  int type;
  int count;
  QString number, temp;
  // -----
  for(count=0; count<NoOfFields; ++count)
    {
      // ----- only non-empty values get stored: 
      if(lineedits[count]->text().isEmpty()) continue;
      type=boxes[count]->currentItem(); // cannot be unselected
      if(type<0 || type>=NoOfFields)
	{
	  kdDebug(KAB_KDEBUG_AREA)
	    << "TabTelephone::storeContents: invalid type, "
	    << "this is a bug." << endl;
	  type=0;
	}
      number=lineedits[count]->text();
      temp.setNum(type);
      values.append(temp);
      values.append(number);
    }
  entry.telephone=values;
}

void TabTelephone::setContents(const AddressBook::Entry& entry)
{
  QStringList::ConstIterator it;
  int count;
  int type;
  bool ok;
  QString first, second;
  // ----- clear boxes and set combos to current item zero:
  for(count=0; count<NoOfFields; ++count)
    {
      boxes[count]->setCurrentItem(0);
      lineedits[count]->setText(QString::null);
    }
  count=0;
  kdDebug(entry.telephone.count()%2!=0, KAB_KDEBUG_AREA)
    << "TabTelephone::setContents: number of elements in telephone "
    << "list must be even. This is a bug." << endl;
  for(it=entry.telephone.begin(); it!=entry.telephone.end(); /* nothing */)
    {
      // ----- make sure loop breaks even with broken lists (odd count):
      if(count==NoOfFields) break;
      // ----- get values:
      first=(*it); ++it;
      second=(*it); ++it;
      // ----- break on empty values:
      if(first.isEmpty() || second.isEmpty())
	{
	  ++count;
	  continue;
	}
      // ----- find type:
      type=first.toInt(&ok);
      if(!ok)
	{
	  kdDebug(KAB_KDEBUG_AREA)
	    << "TabTelephone::setContents: warning: invalid type!" << endl;
	  type=0;
	}
      boxes[count]->setCurrentItem(type);
      // ----- find number:
      lineedits[count]->setText(second);
      // ----- done, increment count:
      ++count;
    }
}

void TabTelephone::changed(int)
{
  emit(TabBasic::changed());
}

void TabTelephone::changed(const QString&)
{
  emit(TabBasic::changed());
}
#include "look_edit_tabtelephone.moc"
