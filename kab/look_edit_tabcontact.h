/* -*- C++ -*-
   This file implements the tab to edit contact information.
 
   the KDE addressbook

   $ Author: Mirko Boehm $
   $ Copyright: (C) 1996-2000, Mirko Boehm $
   $ Contact: mirko@kde.org
         http://www.kde.org $
   $ License: GPL with the following explicit clarification:
         This code may be linked against any version of the Qt toolkit
         from Troll Tech, Norway. $

   $Revision: 66582 $
*/

#ifndef KAB_LOOK_EDIT_TABCONTACT_H
#define KAB_LOOK_EDIT_TABCONTACT_H "$ID$"

#include "look_edit_basictab.h"

class QPushButton;
class QGridLayout;

class TabContact : public TabBasic
{
  Q_OBJECT
public:
  TabContact(KabAPI*, QWidget *parent=0);
  /** Derived from TabBasic. */
  void storeContents(AddressBook::Entry& entry);
  /** Dito. */
  void setContents(const AddressBook::Entry& entry);
protected:
  QPushButton *buttonEmail;
  QPushButton *buttonTalk;
  QPushButton *buttonTelephone;
  QPushButton *buttonURLs;
  QGridLayout *layout;
  AddressBook::Entry entry;
protected slots:
  void editTelephoneNumbers();
  void editEmailAddresses();
  void editTalkAddresses();
  void editURLs();
};

#endif // KAB_LOOK_EDIT_TABCONTACT_H

